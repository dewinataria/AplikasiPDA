<?php

namespace app\controllers;

use Yii;
use app\models\Institusihasalamat;
use app\models\InstitusihasalamatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InstitusihasalamatController implements the CRUD actions for Institusihasalamat model.
 */
class InstitusihasalamatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Institusihasalamat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InstitusihasalamatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Institusihasalamat model.
     * @param integer $institusi_ID
     * @param integer $alamat_id
     * @return mixed
     */
    public function actionView($institusi_ID, $alamat_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($institusi_ID, $alamat_id),
        ]);
    }

    /**
     * Creates a new Institusihasalamat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Institusihasalamat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'institusi_ID' => $model->institusi_ID, 'alamat_id' => $model->alamat_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Institusihasalamat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $institusi_ID
     * @param integer $alamat_id
     * @return mixed
     */
    public function actionUpdate($institusi_ID, $alamat_id)
    {
        $model = $this->findModel($institusi_ID, $alamat_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'institusi_ID' => $model->institusi_ID, 'alamat_id' => $model->alamat_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Institusihasalamat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $institusi_ID
     * @param integer $alamat_id
     * @return mixed
     */
    public function actionDelete($institusi_ID, $alamat_id)
    {
        $this->findModel($institusi_ID, $alamat_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Institusihasalamat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $institusi_ID
     * @param integer $alamat_id
     * @return Institusihasalamat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($institusi_ID, $alamat_id)
    {
        if (($model = Institusihasalamat::findOne(['institusi_ID' => $institusi_ID, 'alamat_id' => $alamat_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
