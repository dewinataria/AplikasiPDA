<?php

namespace app\controllers;

use Yii;
use app\models\Pekerjaanhasinstansi;
use app\models\PekerjaanhasinstansiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PekerjaanhasinstansiController implements the CRUD actions for Pekerjaanhasinstansi model.
 */
class PekerjaanhasinstansiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pekerjaanhasinstansi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PekerjaanhasinstansiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pekerjaanhasinstansi model.
     * @param integer $pekerjaan_id
     * @param integer $instansi_id
     * @return mixed
     */
    public function actionView($pekerjaan_id, $instansi_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($pekerjaan_id, $instansi_id),
        ]);
    }

    /**
     * Creates a new Pekerjaanhasinstansi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pekerjaanhasinstansi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'pekerjaan_id' => $model->pekerjaan_id, 'instansi_id' => $model->instansi_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pekerjaanhasinstansi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $pekerjaan_id
     * @param integer $instansi_id
     * @return mixed
     */
    public function actionUpdate($pekerjaan_id, $instansi_id)
    {
        $model = $this->findModel($pekerjaan_id, $instansi_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'pekerjaan_id' => $model->pekerjaan_id, 'instansi_id' => $model->instansi_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pekerjaanhasinstansi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $pekerjaan_id
     * @param integer $instansi_id
     * @return mixed
     */
    public function actionDelete($pekerjaan_id, $instansi_id)
    {
        $this->findModel($pekerjaan_id, $instansi_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pekerjaanhasinstansi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $pekerjaan_id
     * @param integer $instansi_id
     * @return Pekerjaanhasinstansi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($pekerjaan_id, $instansi_id)
    {
        if (($model = Pekerjaanhasinstansi::findOne(['pekerjaan_id' => $pekerjaan_id, 'instansi_id' => $instansi_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
