<?php
    namespace app\controllers;
    
    use yii\web\Controller;
    use yii\helpers\Json;

    
    
    class DepartemenrinciformagisterController extends Controller
    {
         public function actionIndex()
    {


//data usia departemen 2011
        $deptusia = (new \yii\db\Query())
            ->select(['Mayor'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();


        $lessthan25 = (new \yii\db\Query())
            ->select(['lessthan25'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();
       
        $btween25to29 = (new \yii\db\Query())
            ->select(['btween25to29'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        
        $btween30to34 = (new \yii\db\Query())
            ->select(['btween30to34'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        
        $btween35to39 = (new \yii\db\Query())
            ->select(['btween35to39'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        
        $btween40to44 = (new \yii\db\Query())
            ->select(['btween40to44'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        
        $btween45to49 = (new \yii\db\Query())
            ->select(['btween45to49'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        
        $morethaneqs50 = (new \yii\db\Query())
            ->select(['morethaneqs50'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        
        $NotComplete = (new \yii\db\Query())
            ->select(['NotComplete'])
            ->from('temp_usia2011dept')
            ->limit(100)
            ->column();              
        

        $lessthan25 = array_map('floatval', $lessthan25);
        $btween25to29 = array_map('floatval', $btween25to29);
        $btween30to34 = array_map('floatval', $btween30to34);
        $btween35to39 = array_map('floatval', $btween35to39);
        $btween40to44 = array_map('floatval', $btween40to44);
        $btween45to49 = array_map('floatval', $btween45to49);
        $morethaneqs50 = array_map('floatval', $morethaneqs50);
        $NotComplete = array_map('floatval', $NotComplete);

         
        $data['deptusia'] = json_encode($deptusia);
        $data['lessthan25'] = json_encode($lessthan25);
        $data['btween25to29'] = json_encode($btween25to29);
        $data['btween30to34'] = json_encode($btween30to34);
        $data['btween35to39'] = json_encode($btween35to39);
        $data['btween40to44'] = json_encode($btween40to44);
        $data['btween45to49'] = json_encode($btween45to49);
        $data['morethaneqs50'] = json_encode($morethaneqs50);
        $data['NotComplete'] = json_encode($NotComplete);



//data ipk departemen 2011

        $deptipk = (new \yii\db\Query())
            ->select(['Mayor'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();


        $lessthanipk = (new \yii\db\Query())
            ->select(['lessthan2,5'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();
       
        $btweenipk1 = (new \yii\db\Query())
            ->select(['btween2,50upto2,74'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();              
        
        $btweenipk2 = (new \yii\db\Query())
            ->select(['between2,75upto2,99'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();              
        
        $btweenipk3 = (new \yii\db\Query())
            ->select(['btween3,00upto3,24'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();              
        
        $btweenipk4 = (new \yii\db\Query())
            ->select(['btween3,25upto3,49'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();              
        
        $btweenipk5 = (new \yii\db\Query())
            ->select(['btween3,50upto4,00'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();              
        
        $tidakbisadikonversi = (new \yii\db\Query())
            ->select(['TidakBisaDikonversi'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();              
        
        $TidakLengkap = (new \yii\db\Query())
            ->select(['TidakLengkap'])
            ->from('temp_ipk2011dept')
            ->limit(100)
            ->column();        
        

        $lessthanipk = array_map('floatval', $lessthanipk);
        $btweenipk1 = array_map('floatval', $btweenipk1);
        $btweenipk2 = array_map('floatval', $btweenipk2);
        $btweenipk3 = array_map('floatval', $btweenipk3);
        $btweenipk4 = array_map('floatval', $btweenipk4);
        $btweenipk5 = array_map('floatval', $btweenipk5);
        $tidakbisadikonversi = array_map('floatval', $tidakbisadikonversi);
        $TidakLengkap = array_map('floatval', $TidakLengkap);

         
        $data['deptipk'] = json_encode($deptipk);
        $data['lessthanipk'] = json_encode($lessthanipk);
        $data['btweenipk1'] = json_encode($btweenipk1);
        $data['btweenipk2'] = json_encode($btweenipk2);
        $data['btweenipk3'] = json_encode($btweenipk3);
        $data['btweenipk4'] = json_encode($btweenipk4);
        $data['btweenipk5'] = json_encode($btweenipk5);
        $data['tidakbisadikonversi'] = json_encode($tidakbisadikonversi);
        $data['TidakLengkap'] = json_encode($TidakLengkap);

//data ipk fakultas 2010

        $deptipk2010 = (new \yii\db\Query())
            ->select(['Mayor'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();


        $lessthanipk2010 = (new \yii\db\Query())
            ->select(['lessthan2,5'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();
       
        $btweenipk1a = (new \yii\db\Query())
            ->select(['btween2,50upto2,74'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();              
        
        $btweenipk2a = (new \yii\db\Query())
            ->select(['between2,75upto2,99'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();              
        
        $btweenipk3a = (new \yii\db\Query())
            ->select(['btween3,00upto3,24'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();              
        
        $btweenipk4a = (new \yii\db\Query())
            ->select(['btween3,25upto3,49'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();              
        
        $btweenipk5a = (new \yii\db\Query())
            ->select(['btween3,50upto4,00'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();              
        
        $tidakbisadikonversi2010 = (new \yii\db\Query())
            ->select(['TidakBisaDikonversi'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();              
        
        $TidakLengkap2010 = (new \yii\db\Query())
            ->select(['TidakLengkap'])
            ->from('temp_ipk2010dept')
            ->limit(100)
            ->column();        
        

        $lessthanipk2010 = array_map('floatval', $lessthanipk2010);
        $btweenipk1a = array_map('floatval', $btweenipk1a);
        $btweenipk2a = array_map('floatval', $btweenipk2a);
        $btweenipk3a = array_map('floatval', $btweenipk3a);
        $btweenipk4a = array_map('floatval', $btweenipk4a);
        $btweenipk5a = array_map('floatval', $btweenipk5a);
        $tidakbisadikonversi2010 = array_map('floatval', $tidakbisadikonversi2010);
        $TidakLengkap2010 = array_map('floatval', $TidakLengkap2010);

         
        $data['deptipk2010'] = json_encode($deptipk2010);
        $data['lessthanipk2010'] = json_encode($lessthanipk2010);
        $data['btweenipk1a'] = json_encode($btweenipk1a);
        $data['btweenipk2a'] = json_encode($btweenipk2a);
        $data['btweenipk3a'] = json_encode($btweenipk3a);
        $data['btweenipk4a'] = json_encode($btweenipk4a);
        $data['btweenipk5a'] = json_encode($btweenipk5a);
        $data['tidakbisadikonversi2010'] = json_encode($tidakbisadikonversi2010);
        $data['TidakLengkap2010'] = json_encode($TidakLengkap2010);



       

        return $this->render('index',$data);

        }
    

        
    }


