<?php

namespace app\controllers;

use Yii;
use app\models\Ipbmstoranghasalamat;
use app\models\IpbmstoranghasalamatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IpbmstoranghasalamatController implements the CRUD actions for Ipbmstoranghasalamat model.
 */
class IpbmstoranghasalamatController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Ipbmstoranghasalamat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IpbmstoranghasalamatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Ipbmstoranghasalamat model.
     * @param integer $ipbmst_orang_ID
     * @param integer $alamat_id
     * @return mixed
     */
    public function actionView($ipbmst_orang_ID, $alamat_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($ipbmst_orang_ID, $alamat_id),
        ]);
    }

    /**
     * Creates a new Ipbmstoranghasalamat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ipbmstoranghasalamat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ipbmst_orang_ID' => $model->ipbmst_orang_ID, 'alamat_id' => $model->alamat_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Ipbmstoranghasalamat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $ipbmst_orang_ID
     * @param integer $alamat_id
     * @return mixed
     */
    public function actionUpdate($ipbmst_orang_ID, $alamat_id)
    {
        $model = $this->findModel($ipbmst_orang_ID, $alamat_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'ipbmst_orang_ID' => $model->ipbmst_orang_ID, 'alamat_id' => $model->alamat_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Ipbmstoranghasalamat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $ipbmst_orang_ID
     * @param integer $alamat_id
     * @return mixed
     */
    public function actionDelete($ipbmst_orang_ID, $alamat_id)
    {
        $this->findModel($ipbmst_orang_ID, $alamat_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Ipbmstoranghasalamat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $ipbmst_orang_ID
     * @param integer $alamat_id
     * @return Ipbmstoranghasalamat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($ipbmst_orang_ID, $alamat_id)
    {
        if (($model = Ipbmstoranghasalamat::findOne(['ipbmst_orang_ID' => $ipbmst_orang_ID, 'alamat_id' => $alamat_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
