<?php
    namespace app\controllers;
    
    use yii\web\Controller;
    use yii\helpers\Json;
    use app\models\Faculty;
    use app\models\Department;
    use app\models\Major;

    
    
    class MagisterrinciController extends Controller
    {

        public function lessfaculty($faculty,$condition){

            $data = Faculty::find()->where(['Faculty'=>$faculty])->one();

            $value = 0;
            if(!empty($data)){
                if($condition == 'lessthan25'){
                    $value = $data->lessthan25;
                }else{
                    $value = $data->btween25to29;
                }
            }
            return $value;
    }

    public function lessdepartment($faculty,$condition){

            $data = Department::find()->where(['Department'=>$department])->one();

            $value = 0;
            if(!empty($data)){
                if($condition == 'lessthan25'){
                    $value = $data->lessthan25;
                }else{
                    $value = $data->btween25to29;
                }
            }
            return $value;
    }

        public function actionIndex()
    {

            $faculty = Faculty::find()->all();
            // $faculty = array('A','B');
            // print_r($faculty);die();
// //data usia fakultas
//         $faculty = (new \yii\db\Query())
//             ->select(['Faculty'])
//             ->from('facultyin2011')
//             ->limit(10)
//             ->column();

//         $lessfaculty = (new \yii\db\Query())
//             ->select(['lessthan25'])
//             ->from('facultyin2011')
//             ->limit(10)
//             ->column();

//         $lessfaculty = array_map('floatval', $lessfaculty);


        


//         $betweenfaculty = (new \yii\db\Query())
//             ->select(['btween25to29'])
//             ->from('facultyin2011')
//             ->limit(10)
//             ->column();              
        
//         $betweenfaculty = array_map('floatval', $betweenfaculty);

         
       




// // next
//         $department = (new \yii\db\Query())
//             ->select(['Department'])
//             ->from('department2011')
//             ->limit(10)
//             ->column();

//         $lessdepartment = (new \yii\db\Query())
//             ->select(['lessthan25'])
//             ->from('department2011')
//             ->limit(10)
//             ->column();
//          $lessdepartment = array_map('floatval', $lessdepartment);

        

//         $betweendepartment = (new \yii\db\Query())
//             ->select(['btween25to29'])
//             ->from('department2011')
           
//             ->limit(10)
//             ->column();          
//         $betweendepartment = array_map('floatval', $betweendepartment);
        
   
        
//     //next
        
//         $major = (new \yii\db\Query())
//             ->select(['major'])
//             ->from('major2011')
//             ->limit(10)
//             ->column();

//         $lessmajor = (new \yii\db\Query())
//             ->select(['lessthan25'])
//             ->from('department2011')
//             ->limit(10)
//             ->column();

//         $betweenmajor = (new \yii\db\Query())
//             ->select(['btween25to29'])
//             ->from('department2011')
//             ->limit(10)
//             ->column();
       

//         $lessmajor = array_map('floatval', $lessmajor);
//         $betweenmajor = array_map('floatval', $betweenmajor);


        
        $faculty_lessarr=array();
        $drilldownArray=array();
        $faculty_betweenarr=array();
        $lessthan25=array();
        $between25to29=array();
        // $faculty=array();

        
        foreach($faculty as $data){
                array_push($faculty_lessarr,
                    array('name'=>$data['Faculty'],
                        'y'=>intval($this->lessfaculty($data['Faculty'],'lessthan25')),
                        'drilldown'=>$lessdepartment['lessthan25']));

                // array_push($drilldownArray,
                //     array('name'=>$department['Department'],
                //         'id'=>$lessdepartment['lessthan25'],
                //         'data'=>array(['name'=>$department['Department'],
                //             'y'=>intval($lessdepartment['lessthan25']),
                //             'drilldown'=>$lessmajor['lessthan25']])));

                array_push($faculty_betweenarr,
                    array('name'=>$faculty['Faculty'],
                        'y'=>intval($betweenfaculty['btween25to29']),
                        'drilldown'=>$betweendepartment['btween25to29']));

                // array_push($drilldownArray,
                //     array('name'=>$department['Department'],
                //         'id'=>$betweendepartment['btween25to29'],
                //         'data'=>array(['name'=>$department['Department'],
                //             'y'=>intval($betweendepartment['btween25to29']),
                //             'drilldown'=>$betweenmajor['btween25to29']])));
                        }

        // foreach($department as $data){ //this is for third drilldown still trying
        //                 array_push($drilldownArray,
        //                     array('name'=>$department['Department'],
        //                         'id'=>$lessmajor['lessthan25'],
        //                         'data'=>array(['name'=>$major['major'],
        //                             'y'=>intval($lessmajor['lessthan25'])])));
                        /*array_push($department_arr,array('name'=>$department[$key]['Department'],'y'=>intval($lessdepartment[$key]['lessthan25']),'drilldown'=>$lessmajor[$key]['lessthan25']));
                        array_push($department_arr,array('name'=>$department[$key]['Department'],'y'=>intval($betweendepartment[$key]['btween25to29']),'drildown'=>$betweenmajor[$key]['btween25to29']));*/
                        

        $data['faculty'] = json_encode($faculty);
        // $data['lessfaculty'] = json_encode($lessfaculty);
        // $data['betweenfaculty'] = json_encode($betweenfaculty);

        // $data['department'] = json_encode($department);
        // $data['lessdepartment'] = json_encode($lessdepartment);
        // $data['betweendepartment'] = json_encode($betweendepartment);
        
        // $data['major'] = json_encode($major);
        // $data['lessmajor'] = json_encode($lessmajor);
        // $data['betweenmajor'] = json_encode($betweenmajor);



        return $this->render('index',$data);

        }


    public function actionIndexa()
    {
        return $this->render('indexa');
    }
    

        
    }


