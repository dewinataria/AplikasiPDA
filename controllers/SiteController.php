<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMagister()
    {
        return $this->render('magister');
    }

    public function actionDoktor()
    {
        return $this->render('doktor');
    }
    public function actionSubmagister()
    {
        return $this->render('submagister');
    }
    public function actionSubdoktor()
    {
        return $this->render('subdoktor');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

   /* public function actionDynamicChart()
    {
        $db = \Yii::$app->db;
        $years = $db->createCommand('
              SELECT DISTINCT(year) FROM survey_framework
              ORDER BY year ASC')
            ->queryColumn();
        $frameworks = $db->createCommand('
            SELECT * from framework
            ORDER BY id ASC')
            ->queryAll();
        $series = [];
        foreach($frameworks as $framework){
            $results = $db->createCommand('
                SELECT total FROM survey_framework
                WHERE framework_id='.$framework['id'].'
                ORDER BY year ASC')
                ->queryColumn();
            $data = array_map('intval', $results);
            $series[] = [
                'name' => $framework['name'],
                'data' => $data,
            ];
        }

        return $this->render('dynamic-chart',[
            'years' => $years,
            'series' => $series,
            ]);*/
}
