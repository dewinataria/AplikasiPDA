<?php
namespace app\controllers;

use yii\web\Controller;
use app\models\Jeniskelaminreal;
use app\models\Jeniskelaminrealdoktor;
use app\models\Studi;
use yii\helpers\Json;

class HighchartsController extends Controller
{
	public function actionIndex()
	{

		$masuk= Jeniskelaminreal::find();
		$awal = $masuk->orderBy('TahunMasuk ASC')->one()->TahunMasuk;
		$akhir = $masuk->orderBy('TahunMasuk DESC')->one()->TahunMasuk;
		// $data = $masuk->all();
		$arr_l = [];
		$arr_p = [];
		$tahun = [];

		
		for($i=$awal;$i<=$akhir;$i++){
			
				if($awal == $i){
					$jum_l = count($masuk->where(['TahunMasuk'=>$awal,'JenisKelamin'=>'Perempuan'])->all());
					$jum_p = count($masuk->where(['TahunMasuk'=>$awal,'JenisKelamin'=>'Laki-laki'])->all());
					
				}elseif($i > $awal && $i <= $akhir){
					$jum_l = count($masuk->where(['TahunMasuk'=>$i,'JenisKelamin'=>'Perempuan'])->all());
					$jum_p = count($masuk->where(['TahunMasuk'=>$i,'JenisKelamin'=>'Laki-laki'])->all());
				}
				array_push($arr_l,$jum_l);
				array_push($arr_p,$jum_p);
				array_push($tahun,$i);
				}				
			
		
		$data['tahun'] = json_encode($tahun);
		$data['data_p'] = json_encode($arr_p);
		$data['data_l'] = json_encode($arr_l);


		//data selanjutnya
		$masuk1= Jeniskelaminrealdoktor::find();
		$awal1= $masuk1->orderBy('TahunMasuk ASC')->one()->TahunMasuk;
		$akhir1= $masuk1->orderBy('TahunMasuk DESC')->one()->TahunMasuk;

		$arr_l1 = [];
		$arr_p1 = [];
		$tahun1 = [];

		for($j=$awal1;$j<=$akhir1;$j++){
			
				if($awal1 == $j){
					$jum_l1 = count($masuk1->where(['TahunMasuk'=>$awal1,'JenisKelamin'=>'Perempuan'])->all());
					$jum_p1 = count($masuk1->where(['TahunMasuk'=>$awal1,'JenisKelamin'=>'Laki-laki'])->all());
					
				}elseif($j > $awal1 && $j <= $akhir1){
					$jum_l1 = count($masuk1->where(['TahunMasuk'=>$j,'JenisKelamin'=>'Perempuan'])->all());
					$jum_p1 = count($masuk1->where(['TahunMasuk'=>$j,'JenisKelamin'=>'Laki-laki'])->all());
				}
				array_push($arr_l1,$jum_l1);
				array_push($arr_p1,$jum_p1);
				array_push($tahun1,$j);
				}

		$data['tahun1'] = json_encode($tahun1);
		$data['data_p1'] = json_encode($arr_p1);
		$data['data_l1'] = json_encode($arr_l1);

		//data selanjutnya
		$masuk2= Jeniskelaminreal::find()->where(['<', 'Usia', 25]);
		$awal2 = $masuk2->orderBy('TahunMasuk ASC')->one()->TahunMasuk;
		$akhir2 = $masuk2->orderBy('TahunMasuk DESC')->one()->TahunMasuk;
		// $data = $masuk->all();
		$arr_l2 = [];
		$tahun2 = [];

		
		for($k=$awal2;$k<=$akhir2;$k++){
			
				if($awal2 == $k){
					$jum_l2 = count($masuk2->where(['TahunMasuk'=>$awal2,['<', 'Usia', 25]])->all());
					
					
				}elseif($k > $awal2 && $k <= $akhir2){
					$jum_l2 = count($masuk2->where(['TahunMasuk'=>$k,['<', 'Usia', 25]])->all());
					
				}
				array_push($arr_l2,$jum_l2);
				
				array_push($tahun2,$k);
				}				
			
		
		$data['tahun2'] = json_encode($tahun);
		$data['data_l2'] = json_encode($arr_l2);

		
		

		return $this->render('index',$data);
	}

	/*public function actionData()
	{
		return $this->render('data');
	}*/
}