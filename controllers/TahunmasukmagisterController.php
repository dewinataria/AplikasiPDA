<?php

namespace app\controllers;

use Yii;
use app\models\Tahunmasukmagister;
use app\models\TahunmasukmagisterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TahunmasukmagisterController implements the CRUD actions for Tahunmasukmagister model.
 */
class TahunmasukmagisterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tahunmasukmagister models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TahunmasukmagisterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tahunmasukmagister model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tahunmasukmagister model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		 if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->username != 'admin') {
			throw new \yii\web\ForbiddenHttpException('Insufficient privileges to access this area.');
		} 
        $model = new Tahunmasukmagister();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NIM]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tahunmasukmagister model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		
        if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->username != 'admin') {
			throw new \yii\web\ForbiddenHttpException('Insufficient privileges to access this area.');
		} 
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NIM]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tahunmasukmagister model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		 if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->username != 'admin') {
			throw new \yii\web\ForbiddenHttpException('Insufficient privileges to access this area.');
		} 
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tahunmasukmagister model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Tahunmasukmagister the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tahunmasukmagister::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	 public function beforeAction($event)
    {   
        return parent::beforeAction($event);
    }
}
