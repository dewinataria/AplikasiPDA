<?php

namespace app\controllers;

use Yii;
use app\models\PutusstudiDoktor;
use app\models\PutusstudiDoktorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PutusstudiDoktorController implements the CRUD actions for PutusstudiDoktor model.
 */
class PutusstudiDoktorController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PutusstudiDoktor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PutusstudiDoktorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PutusstudiDoktor model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PutusstudiDoktor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->username != 'admin') {
			throw new \yii\web\ForbiddenHttpException('Insufficient privileges to access this area.');
		} 
        $model = new PutusstudiDoktor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NIM]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing PutusstudiDoktor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
		if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->username != 'admin') {
			throw new \yii\web\ForbiddenHttpException('Insufficient privileges to access this area.');
		} 
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->NIM]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PutusstudiDoktor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
		if (\Yii::$app->user->isGuest || \Yii::$app->user->identity->username != 'admin') {
			throw new \yii\web\ForbiddenHttpException('Insufficient privileges to access this area.');
		} 
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PutusstudiDoktor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return PutusstudiDoktor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PutusstudiDoktor::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
