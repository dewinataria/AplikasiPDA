<?php

namespace app\controllers;

use Yii;
use app\models\Pekerjaanhasinstitusi;
use app\models\PekerjaanhasinstitusiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PekerjaanhasinstitusiController implements the CRUD actions for Pekerjaanhasinstitusi model.
 */
class PekerjaanhasinstitusiController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pekerjaanhasinstitusi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PekerjaanhasinstitusiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Pekerjaanhasinstitusi model.
     * @param integer $pekerjaan_id
     * @param integer $institusi_ID
     * @return mixed
     */
    public function actionView($pekerjaan_id, $institusi_ID)
    {
        return $this->render('view', [
            'model' => $this->findModel($pekerjaan_id, $institusi_ID),
        ]);
    }

    /**
     * Creates a new Pekerjaanhasinstitusi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pekerjaanhasinstitusi();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'pekerjaan_id' => $model->pekerjaan_id, 'institusi_ID' => $model->institusi_ID]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pekerjaanhasinstitusi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $pekerjaan_id
     * @param integer $institusi_ID
     * @return mixed
     */
    public function actionUpdate($pekerjaan_id, $institusi_ID)
    {
        $model = $this->findModel($pekerjaan_id, $institusi_ID);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'pekerjaan_id' => $model->pekerjaan_id, 'institusi_ID' => $model->institusi_ID]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pekerjaanhasinstitusi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $pekerjaan_id
     * @param integer $institusi_ID
     * @return mixed
     */
    public function actionDelete($pekerjaan_id, $institusi_ID)
    {
        $this->findModel($pekerjaan_id, $institusi_ID)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pekerjaanhasinstitusi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $pekerjaan_id
     * @param integer $institusi_ID
     * @return Pekerjaanhasinstitusi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($pekerjaan_id, $institusi_ID)
    {
        if (($model = Pekerjaanhasinstitusi::findOne(['pekerjaan_id' => $pekerjaan_id, 'institusi_ID' => $institusi_ID])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
