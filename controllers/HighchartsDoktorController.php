<?php
    namespace app\controllers;
    
    use yii\web\Controller;
    use yii\helpers\Json;

    
    
    class HighchartsController extends Controller
    {
         public function actionIndexDoktor()
    {

//data jk doktor

        $jkdoktor = (new \yii\db\Query())
            ->select(['TahunMasuk'])
            ->from('temp_jkmhsdoktor')
            ->limit(10)
            ->column();

        $jkdoktor1 = (new \yii\db\Query())
            ->select(['Lakilaki'])
            ->from('temp_jkmhsdoktor')
            ->limit(10)
            ->column();

        $jkdoktor2 = (new \yii\db\Query())
            ->select(['Perempuan'])
            ->from('temp_jkmhsdoktor')
            ->limit(10)
            ->column();
        $jkdoktor3 = (new \yii\db\Query())
            ->select(['Lainlain'])
            ->from('temp_jkmhsdoktor')
            ->limit(10)
            ->column();

        $jkdoktor1 = array_map('floatval', $jkdoktor1);
        $jkdoktor2 = array_map('floatval', $jkdoktor2);
        $jkdoktor3 = array_map('floatval', $jkdoktor3);

        $data['yearjkdoktor'] = json_encode($jkdoktor);
        $data['maledoktor'] = json_encode($jkdoktor1);
        $data['femaledoktor'] = json_encode($jkdoktor2);
        $data['otherdoktor'] = json_encode($jkdoktor3);

//data usia doktor
        $usiadoktor = (new \yii\db\Query())
            ->select(['TahunMasuk'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();

        $usiadoktor1 = (new \yii\db\Query())
            ->select(['lessthan25'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor2 = (new \yii\db\Query())
            ->select(['btween25to29'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor3 = (new \yii\db\Query())
            ->select(['btween30to34'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor4 = (new \yii\db\Query())
            ->select(['btween35to39'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor5 = (new \yii\db\Query())
            ->select(['btween40to44'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor6 = (new \yii\db\Query())
            ->select(['btween45to49'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor7 = (new \yii\db\Query())
            ->select(['morethaneqs50'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();
        $usiadoktor8 = (new \yii\db\Query())
            ->select(['NotComplete'])
            ->from('temp_usiadoktor')
            ->limit(10)
            ->column();

                 

        $usiadoktor1 = array_map('floatval', $usiadoktor1);
        $usiadoktor2 = array_map('floatval', $usiadoktor2);
        $usiadoktor3 = array_map('floatval', $usiadoktor3);
        $usiadoktor4 = array_map('floatval', $usiadoktor4);
        $usiadoktor5 = array_map('floatval', $usiadoktor5);
        $usiadoktor6 = array_map('floatval', $usiadoktor6);
        $usiadoktor7 = array_map('floatval', $usiadoktor7);
        $usiadoktor8 = array_map('floatval', $usiadoktor8);


        $data['yearusiadoktor'] = json_encode($usiadoktor);
        $data['usiadoktor1'] = json_encode($usiadoktor1);
        $data['usiadoktor2'] = json_encode($usiadoktor2);
        $data['usiadoktor3'] = json_encode($usiadoktor3);
        $data['usiadoktor4'] = json_encode($usiadoktor4);
        $data['usiadoktor5'] = json_encode($usiadoktor5);
        $data['usiadoktor6'] = json_encode($usiadoktor6);
        $data['usiadoktor7'] = json_encode($usiadoktor7);
        $data['usiadoktor8'] = json_encode($usiadoktor8);

//ipk doktor
        $ipkdoktor = (new \yii\db\Query())
            ->select(['tahunawal'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();

        $ipkdoktor1 = (new \yii\db\Query())
            ->select(['lessthan2,50'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();

        $ipkdoktor2 = (new \yii\db\Query())
            ->select(['btween2,50to2,74'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();
        $ipkdoktor3 = (new \yii\db\Query())
            ->select(['btween2,75to2,99'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();
        $ipkdoktor4 = (new \yii\db\Query())
            ->select(['btween3,00to3,24'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();
        $ipkdoktor5 = (new \yii\db\Query())
            ->select(['btween3,25to3,49'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();
        $ipkdoktor6 = (new \yii\db\Query())
            ->select(['btween3,50to4,00'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();
        $ipkdoktor7 = (new \yii\db\Query())
            ->select(['tidakbisadiconvert'])
            ->from('temp_ipkmhsdoktor')
            ->limit(10)
            ->column();
                 

        $ipkdoktor1 = array_map('floatval', $ipkdoktor1);
        $ipkdoktor2 = array_map('floatval', $ipkdoktor2);
        $ipkdoktor3 = array_map('floatval', $ipkdoktor3);
        $ipkdoktor4 = array_map('floatval', $ipkdoktor4);
        $ipkdoktor5 = array_map('floatval', $ipkdoktor5);
        $ipkdoktor6 = array_map('floatval', $ipkdoktor6);
        $ipkdoktor7 = array_map('floatval', $ipkdoktor7);

        $data['yearipkdoktor'] = json_encode($ipkdoktor);
        $data['ipkdoktor1'] = json_encode($ipkdoktor1);
        $data['ipkdoktor2'] = json_encode($ipkdoktor2);
        $data['ipkdoktor3'] = json_encode($ipkdoktor3);
        $data['ipkdoktor4'] = json_encode($ipkdoktor4);
        $data['ipkdoktor5'] = json_encode($ipkdoktor5);
        $data['ipkdoktor6'] = json_encode($ipkdoktor6);
        $data['ipkdoktor7'] = json_encode($ipkdoktor7);

//durasi studi doktor
        $durasistudidoktor = (new \yii\db\Query())
            ->select(['TahunMasuk'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();

        $durasistudidoktor1 = (new \yii\db\Query())
            ->select(['lessthan2'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();

        $durasistudidoktor2 = (new \yii\db\Query())
            ->select(['btween2,0to2,4'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();
        $durasistudidoktor3 = (new \yii\db\Query())
            ->select(['btween2,5to2,9'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();
        $durasistudidoktor4 = (new \yii\db\Query())
            ->select(['btween3,0to3,4'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();
        $durasistudidoktor5 = (new \yii\db\Query())
            ->select(['btween3,5to3,9'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();
        $durasistudidoktor6 = (new \yii\db\Query())
            ->select(['morethaneqs4,0'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();
        $durasistudidoktor7 = (new \yii\db\Query())
            ->select(['TidakDiketahui'])
            ->from('temp_jmlhdurasisstudidoktor')
            ->limit(10)
            ->column();    
                 

        $durasistudidoktor1 = array_map('floatval', $durasistudidoktor1);
        $durasistudidoktor2 = array_map('floatval', $durasistudidoktor2);
        $durasistudidoktor3 = array_map('floatval', $durasistudidoktor3);
        $durasistudidoktor4 = array_map('floatval', $durasistudidoktor4);
        $durasistudidoktor5 = array_map('floatval', $durasistudidoktor5);
        $durasistudidoktor6 = array_map('floatval', $durasistudidoktor6);
        $durasistudidoktor7 = array_map('floatval', $durasistudidoktor7);


        $data['yeardurasistudidoktor'] = json_encode($durasistudidoktor);
        $data['durasistudidoktor1'] = json_encode($durasistudidoktor1);
        $data['durasistudidoktor2'] = json_encode($durasistudidoktor2);
        $data['durasistudidoktor3'] = json_encode($durasistudidoktor3);
        $data['durasistudidoktor4'] = json_encode($durasistudidoktor4);
        $data['durasistudidoktor5'] = json_encode($durasistudidoktor5);
        $data['durasistudidoktor6'] = json_encode($durasistudidoktor6);
        $data['durasistudidoktor7'] = json_encode($durasistudidoktor7);

//status studi doktor
        $statusstudidoktor = (new \yii\db\Query())
            ->select(['TahunAwal'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();

        $statusstudidoktor1 = (new \yii\db\Query())
            ->select(['Aktif'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();

        $statusstudidoktor2= (new \yii\db\Query())
            ->select(['Cuti'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();
        $statusstudidoktor3 = (new \yii\db\Query())
            ->select(['Lulus'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();
        $statusstudidoktor4 = (new \yii\db\Query())
            ->select(['MengundurkanDiri'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();
        $statusstudidoktor5 = (new \yii\db\Query())
            ->select(['DropOut'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();
        $statusstudidoktor6 = (new \yii\db\Query())
            ->select(['TanpaKeterangan'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();
        $statusstudidoktor7 = (new \yii\db\Query())
            ->select(['MeninggalDunia'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();   
        $statusstudidoktor8 = (new \yii\db\Query())
            ->select(['NonAktif'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();
        $statusstudidoktor9 = (new \yii\db\Query())
            ->select(['PindahMayor'])
            ->from('temp_statusstudimhsdoktor')
            ->limit(10)
            ->column();    
                 

        $statusstudidoktor1 = array_map('floatval', $statusstudidoktor1);
        $statusstudidoktor2 = array_map('floatval', $statusstudidoktor2);
        $statusstudidoktor3 = array_map('floatval', $statusstudidoktor3);
        $statusstudidoktor4 = array_map('floatval', $statusstudidoktor4);
        $statusstudidoktor5 = array_map('floatval', $statusstudidoktor5);
        $statusstudidoktor6 = array_map('floatval', $statusstudidoktor6);
        $statusstudidoktor7 = array_map('floatval', $statusstudidoktor7);
        $statusstudidoktor8 = array_map('floatval', $statusstudidoktor8);
        $statusstudidoktor9 = array_map('floatval', $statusstudidoktor9);


        $data['yearstatusstudidoktor'] = json_encode($statusstudidoktor);
        $data['statusstudidoktor1'] = json_encode($statusstudidoktor1);
        $data['statusstudidoktor2'] = json_encode($statusstudidoktor2);
        $data['statusstudidoktor3'] = json_encode($statusstudidoktor3);
        $data['statusstudidoktor4'] = json_encode($statusstudidoktor4);
        $data['statusstudidoktor5'] = json_encode($statusstudidoktor5);
        $data['statusstudidoktor6'] = json_encode($statusstudidoktor6);
        $data['statusstudidoktor7'] = json_encode($statusstudidoktor7);
        $data['statusstudidoktor8'] = json_encode($statusstudidoktor8);
        $data['statusstudidoktor9'] = json_encode($statusstudidoktor9);


        return $this->render('indexdoktor',$data);
    	}
    
    	
    }
