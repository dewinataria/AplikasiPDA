<?php
    namespace app\controllers;
    
    use yii\web\Controller;
    use app\models\Hapus;
    use yii\helpers\Json;
    use app\models\Cobbaa;
    
    
    class HighchartsController extends Controller
    {
    	 public function actionIndex()
    {

//data jk magister

        $rows = (new \yii\db\Query())
            ->select(['TahunMasuk'])
            ->from('jkmahasiswamagister')
            ->limit(10)
            ->column();

        $rowsa = (new \yii\db\Query())
            ->select(['Lakilaki'])
            ->from('jkmahasiswamagister')
            ->limit(10)
            ->column();

        $rowsaa = (new \yii\db\Query())
            ->select(['Perempuan'])
            ->from('jkmahasiswamagister')
            ->limit(10)
            ->column();
        $rowsaaa = (new \yii\db\Query())
            ->select(['Lainlain'])
            ->from('jkmahasiswamagister')
            ->limit(10)
            ->column();

        $rowsa = array_map('floatval', $rowsa);
        $rowsaa = array_map('floatval', $rowsaa);
         $rowsaaa = array_map('floatval', $rowsaaa);

        $data['year'] = json_encode($rows);
        $data['male'] = json_encode($rowsa);
        $data['female'] = json_encode($rowsaa);
        $data['other'] = json_encode($rowsaaa);

//data jk doktor
        $doktors = (new \yii\db\Query())
            ->select(['TahunMasuk'])
            ->from('jkmahasiswadoktor')
            ->limit(10)
            ->column();

        $doktorsa = (new \yii\db\Query())
            ->select(['Lakilaki'])
            ->from('jkmahasiswadoktor')
            ->limit(10)
            ->column();

        $doktorsaa = (new \yii\db\Query())
            ->select(['Perempuan'])
            ->from('jkmahasiswadoktor')
            ->limit(10)
            ->column();
        $doktorsaaa = (new \yii\db\Query())
            ->select(['Lainlain'])
            ->from('jkmahasiswadoktor')
            ->limit(10)
            ->column();

        $doktorsa = array_map('floatval', $doktorsa);
        $doktorsaa = array_map('floatval', $doktorsaa);
         $doktorsaaa = array_map('floatval', $doktorsaaa);

        $data['yeardoktor'] = json_encode($doktors);
        $data['maledoktor'] = json_encode($doktorsa);
        $data['femaledoktor'] = json_encode($doktorsaa);
        $data['otherdoktor'] = json_encode($doktorsaaa);

//data usia
        $usia1 = (new \yii\db\Query())
            ->select(['TahunMasuk'])
            ->from('usia')
            ->limit(10)
            ->column();

        $usia2 = (new \yii\db\Query())
            ->select(['lessthan25'])
            ->from('usia')
            ->limit(10)
            ->column();

        $usia3 = (new \yii\db\Query())
            ->select(['btween25upto29'])
            ->from('usia')
            ->limit(10)
            ->column();
        $usia4 = (new \yii\db\Query())
            ->select(['btween30upto34'])
            ->from('usia')
            ->limit(10)
            ->column();
        $usia5 = (new \yii\db\Query())
            ->select(['btween35upto39'])
            ->from('usia')
            ->limit(10)
            ->column();
        $usia6 = (new \yii\db\Query())
            ->select([' btween40upto44'])
            ->from('usia')
            ->limit(10)
            ->column();
        $usia7 = (new \yii\db\Query())
            ->select(['btween45upto49'])
            ->from('usia')
            ->limit(10)
            ->column();
        $usia8 = (new \yii\db\Query())
            ->select(['morethanequals50'])
            ->from('usia')
            ->limit(10)
            ->column();
        $usia9 = (new \yii\db\Query())
            ->select(['TidakLengkap'])
            ->from('usia')
            ->limit(10)
            ->column();

          
          


        $usia2 = array_map('floatval', $usia2);
        $usia3 = array_map('floatval', $usia3);
        $usia4 = array_map('floatval', $usia4);
        $usia5 = array_map('floatval', $usia5);
        $usia6 = array_map('floatval', $usia6);
        $usia7 = array_map('floatval', $usia7);
        $usia8 = array_map('floatval', $usia8);
        $usia9 = array_map('floatval', $usia9);


        $data['yearusia'] = json_encode($usia1);
        $data['usia2'] = json_encode($usia2);
        $data['usia3'] = json_encode($usia3);
        $data['usia4'] = json_encode($usia4);
        $data['usia5'] = json_encode($usia5);
        $data['usia6'] = json_encode($usia6);
        $data['usia7'] = json_encode($usia7);
        $data['usia8'] = json_encode($usia8);
        $data['usia9'] = json_encode($usia9);




        return $this->render('index',$data);
    	}
    
    	
    }
