<?php
    namespace app\controllers;
    
    use yii\web\Controller;
    use yii\helpers\Json;

    
    
    class MagisterrinciiController extends Controller
    {
        
         public function actionIndex()
    {


//data usia fakultas
        $categoryArray = [];

        $faculty = (new \yii\db\Query())
            ->select(['Faculty'])
            ->from('faculty')
            ->limit(10)
            ->column();

        array_push($categoryArray,
                    array('id'=>0,
                        'categories'=>$faculty));

        $faculty2 = array_map('floatval', $faculty);


        $lessfaculty = (new \yii\db\Query())
            ->select(['lessthan25'])
            ->from('faculty')
            ->limit(10)
            ->column();
            // print_r($lessfaculty);die();
        $fakultas = $faculty;

        

        $lessfaculty = array_map('floatval', $lessfaculty);

        foreach ($lessfaculty as $key => $lessfaculty_value) {
            $lessfaculty[$key] = [
                'name' => 'Fakultas',
                'y' => $lessfaculty_value,
                'drilldown' => 'department'.$fakultas[$key].'less'
            ];
        };


        $betweenfaculty = (new \yii\db\Query())
            ->select(['btween25to29'])
            ->from('faculty')
            ->limit(10)
            ->column();             
        

        $betweenfaculty = array_map('floatval', $betweenfaculty);

         foreach ($betweenfaculty as $key => $betweenfaculty_value) {
            $betweenfaculty[$key] = [
                'name' => 'Fakultas',
                'y' => $betweenfaculty_value,
                'drilldown' => 'department'.$fakultas[$key].'betw'
            ];
        };
       


        $data['faculty'] = json_encode($faculty);
        $data['lessfaculty'] = json_encode($lessfaculty);
        $data['betweenfaculty'] = json_encode($betweenfaculty);


// next
        $i = 1;
        $drilldownArray = [];
        foreach($faculty2 as $keyf=>$faculty_value){

        $department = (new \yii\db\Query())
            ->select(['Department'])
            ->from('department')
            ->where('Department like "'.$fakultas[$keyf].'%"')
            ->limit(10)
            ->column();

        array_push($categoryArray,
                    array('id'=>$i,
                        'categories'=>$department));



        $lessdepartment = (new \yii\db\Query())
            ->select(['lessthan25'])
            ->from('department')
            ->where('Department like "'.$fakultas[$keyf].'%"')
            ->limit(10)
            ->column();
         $lessdepartment = array_map('floatval', $lessdepartment);

        foreach ($lessdepartment as $key => $lessdepartment_value) {
                $lessdepartment[$key] = [
                'name' => $fakultas[$keyf].($key+1),
                'y' => $lessdepartment_value,
                'drilldown' => 'major1'
            ];
            };

        array_push($drilldownArray,
                    array('name'=>'lessthan25',
                        'xAxis'=> $keyf+1,
                        'id'=>'department'.$fakultas[$keyf].'less',
                        'data'=>$lessdepartment));


        $betweendepartment = (new \yii\db\Query())
            ->select(['btween25to29'])
            ->from('department')
            ->where('Department like "'.$fakultas[$keyf].'%"')
            ->limit(10)
            ->column();

            
       

       
        $betweendepartment = array_map('floatval', $betweendepartment);
        foreach ($betweendepartment as $key => $betweendepartment_value) {
                $betweendepartment[$key] = [
                'name' => $fakultas[$keyf].($key+1),
                'y' => $betweendepartment_value,
                'drilldown' => 'major2'
            ];
            };

            array_push($drilldownArray,
                    array('name'=>'btween25to29',
                        'xAxis'=> $keyf+1,
                        'id'=>'department'.$fakultas[$keyf].'betw',
                        'data'=>$betweendepartment));

            

            $i++;
        }

        // $data['department'] = json_encode($department);
      
        // $data['lessdepartment'] = json_encode($lessdepartment);
        // $data['betweendepartment'] = json_encode($betweendepartment);

        $data['categoryArray'] = json_encode($categoryArray);
      
        $data['drilldownArray'] = json_encode($drilldownArray);
        
        

//next
        
        $major = (new \yii\db\Query())
            ->select(['major'])
            ->from('major')
            ->limit(10)
            ->column();

        $lessmajor = (new \yii\db\Query())
            ->select(['lessthan25'])
            ->from('department')
            ->limit(10)
            ->column();

        $betweenmajor = (new \yii\db\Query())
            ->select(['btween25to29'])
            ->from('department')
            ->limit(10)
            ->column();
       

        $lessmajor = array_map('floatval', $lessmajor);
        $betweenmajor = array_map('floatval', $betweenmajor);


        $data['major'] = json_encode($major);
        $data['lessmajor'] = json_encode($lessmajor);
        $data['betweenmajor'] = json_encode($betweenmajor);



        
    

        return $this->render('index',$data);

        }
    

        
    }


