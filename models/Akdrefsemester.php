<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_semester".
 *
 * @property integer $ID
 * @property integer $SemesterAngka
 * @property string $SemesterNama
 */
class Akdrefsemester extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_semester';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'SemesterAngka', 'SemesterNama'], 'required'],
            [['ID', 'SemesterAngka'], 'integer'],
            [['SemesterNama'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'SemesterAngka' => Yii::t('app', 'Semester Angka'),
            'SemesterNama' => Yii::t('app', 'Semester Nama'),
        ];
    }
}
