<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pendaftaranhasprogramstudi;

/**
 * PendaftaranhasprogramstudiSearch represents the model behind the search form about `app\models\Pendaftaranhasprogramstudi`.
 */
class PendaftaranhasprogramstudiSearch extends Pendaftaranhasprogramstudi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ProgramStudi_id', 'urutan'], 'integer'],
            [['Pendaftaran_noPendaftaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pendaftaranhasprogramstudi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ProgramStudi_id' => $this->ProgramStudi_id,
            'urutan' => $this->urutan,
        ]);

        $query->andFilterWhere(['like', 'Pendaftaran_noPendaftaran', $this->Pendaftaran_noPendaftaran]);

        return $dataProvider;
    }
}
