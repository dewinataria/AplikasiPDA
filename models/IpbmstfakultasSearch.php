<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ipbmstfakultas;

/**
 * IpbmstfakultasSearch represents the model behind the search form about `app\models\Ipbmstfakultas`.
 */
class IpbmstfakultasSearch extends Ipbmstfakultas
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Dekan'], 'integer'],
            [['Kode', 'TanggalDibentuk', 'FakultasBKey'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ipbmstfakultas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'Dekan' => $this->Dekan,
            'TanggalDibentuk' => $this->TanggalDibentuk,
        ]);

        $query->andFilterWhere(['like', 'Kode', $this->Kode])
            ->andFilterWhere(['like', 'FakultasBKey', $this->FakultasBKey]);

        return $dataProvider;
    }
}
