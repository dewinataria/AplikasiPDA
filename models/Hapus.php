<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hapus".
 *
 * @property integer $TahunMasuk
 * @property integer $Perempuan
 * @property integer $Lakilaki
 */
class Hapus extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hapus';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TahunMasuk', 'Perempuan', 'Lakilaki'], 'required'],
            [['TahunMasuk', 'Perempuan', 'Lakilaki'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'Perempuan' => Yii::t('app', 'Perempuan'),
            'Lakilaki' => Yii::t('app', 'Lakilaki'),
        ];
    }
}
