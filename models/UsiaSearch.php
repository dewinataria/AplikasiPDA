<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usia;

/**
 * UsiaSearch represents the model behind the search form about `app\models\Usia`.
 */
class UsiaSearch extends Usia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TahunMasuk', 'lessthan25', 'btween25upto29', 'btween30upto34', 'btween35upto39', 'btween40upto44', 'btween45upto49', 'morethanequals50', 'TidakLengkap', 'Total'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usia::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TahunMasuk' => $this->TahunMasuk,
            'lessthan25' => $this->lessthan25,
            'btween25upto29' => $this->btween25upto29,
            'btween30upto34' => $this->btween30upto34,
            'btween35upto39' => $this->btween35upto39,
            'btween40upto44' => $this->btween40upto44,
            'btween45upto49' => $this->btween45upto49,
            'morethanequals50' => $this->morethanequals50,
            'TidakLengkap' => $this->TidakLengkap,
            'Total' => $this->Total,
        ]);

        return $dataProvider;
    }
}
