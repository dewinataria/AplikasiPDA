<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdhis_akreditasips".
 *
 * @property integer $ID
 * @property integer $MayorID
 * @property integer $NilaiAkreditasi
 * @property string $PredikatAkreditasi
 * @property string $TMTAkreditasi
 * @property string $TSTAkreditasi
 * @property integer $LembagaPengakreditasiID
 * @property integer $LingkupAkreditasiID
 *
 * @property AkdmstMayor $mayor
 */
class Akdhisakreditasips extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdhis_akreditasips';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'MayorID', 'NilaiAkreditasi', 'LembagaPengakreditasiID', 'LingkupAkreditasiID'], 'integer'],
            [['TMTAkreditasi', 'TSTAkreditasi'], 'safe'],
            [['PredikatAkreditasi'], 'string', 'max' => 50],
            [['MayorID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdmstMayor::className(), 'targetAttribute' => ['MayorID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MayorID' => Yii::t('app', 'Mayor ID'),
            'NilaiAkreditasi' => Yii::t('app', 'Nilai Akreditasi'),
            'PredikatAkreditasi' => Yii::t('app', 'Predikat Akreditasi'),
            'TMTAkreditasi' => Yii::t('app', 'Tmtakreditasi'),
            'TSTAkreditasi' => Yii::t('app', 'Tstakreditasi'),
            'LembagaPengakreditasiID' => Yii::t('app', 'Lembaga Pengakreditasi ID'),
            'LingkupAkreditasiID' => Yii::t('app', 'Lingkup Akreditasi ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMayor()
    {
        return $this->hasOne(AkdmstMayor::className(), ['ID' => 'MayorID']);
    }
}
