<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pendaftaran".
 *
 * @property string $noPendaftaran
 * @property integer $OrangID
 * @property integer $RencanaPembiayaanID
 * @property integer $ManajemenJalurMasukID
 * @property string $PaketPendaftaranID
 * @property integer $setujuSyarat
 * @property integer $verifikasiPMB
 * @property string $waktuBuat
 * @property string $waktuubah
 *
 * @property Rencanapembiayaan $rencanaPembiayaan
 * @property IpbmstOrang $orang
 * @property PendaftaranHasProgramstudi[] $pendaftaranHasProgramstudis
 */
class Pendaftaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pendaftaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['noPendaftaran'], 'required'],
            [['OrangID', 'RencanaPembiayaanID', 'ManajemenJalurMasukID', 'setujuSyarat', 'verifikasiPMB'], 'integer'],
            [['waktuBuat', 'waktuubah'], 'safe'],
            [['noPendaftaran'], 'string', 'max' => 100],
            [['PaketPendaftaranID'], 'string', 'max' => 45],
            [['RencanaPembiayaanID'], 'exist', 'skipOnError' => true, 'targetClass' => Rencanapembiayaan::className(), 'targetAttribute' => ['RencanaPembiayaanID' => 'ID']],
            [['OrangID'], 'exist', 'skipOnError' => true, 'targetClass' => IpbmstOrang::className(), 'targetAttribute' => ['OrangID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'noPendaftaran' => Yii::t('app', 'No Pendaftaran'),
            'OrangID' => Yii::t('app', 'Orang ID'),
            'RencanaPembiayaanID' => Yii::t('app', 'Rencana Pembiayaan ID'),
            'ManajemenJalurMasukID' => Yii::t('app', 'Manajemen Jalur Masuk ID'),
            'PaketPendaftaranID' => Yii::t('app', 'Paket Pendaftaran ID'),
            'setujuSyarat' => Yii::t('app', 'Setuju Syarat'),
            'verifikasiPMB' => Yii::t('app', 'Verifikasi Pmb'),
            'waktuBuat' => Yii::t('app', 'Waktu Buat'),
            'waktuubah' => Yii::t('app', 'Waktuubah'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRencanaPembiayaan()
    {
        return $this->hasOne(Rencanapembiayaan::className(), ['ID' => 'RencanaPembiayaanID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrang()
    {
        return $this->hasOne(IpbmstOrang::className(), ['ID' => 'OrangID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendaftaranHasProgramstudis()
    {
        return $this->hasMany(PendaftaranHasProgramstudi::className(), ['Pendaftaran_noPendaftaran' => 'noPendaftaran']);
    }
}
