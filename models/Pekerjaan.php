<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pekerjaan".
 *
 * @property integer $id
 * @property string $tanggalMasuk
 * @property string $tanggalKeluar
 * @property string $jabatan
 * @property string $noIdentitas
 * @property integer $jenisInstansi_id
 * @property integer $orang_id
 * @property string $pekerjaanLainnya
 * @property string $waktuBuat
 * @property string $waktuUbah
 *
 * @property IpbmstOrang $orang
 * @property Jenisinstansi $jenisInstansi
 * @property PekerjaanHasInstansi[] $pekerjaanHasInstansis
 * @property Instansi[] $instansis
 * @property PekerjaanHasInstitusi[] $pekerjaanHasInstitusis
 * @property Institusi[] $institusis
 */
class Pekerjaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pekerjaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'jenisInstansi_id', 'orang_id'], 'integer'],
            [['tanggalMasuk', 'tanggalKeluar', 'waktuBuat', 'waktuUbah'], 'safe'],
            [['jabatan', 'pekerjaanLainnya'], 'string', 'max' => 100],
            [['noIdentitas'], 'string', 'max' => 50],
            [['orang_id'], 'exist', 'skipOnError' => true, 'targetClass' => IpbmstOrang::className(), 'targetAttribute' => ['orang_id' => 'ID']],
            [['jenisInstansi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenisinstansi::className(), 'targetAttribute' => ['jenisInstansi_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'tanggalMasuk' => Yii::t('app', 'Tanggal Masuk'),
            'tanggalKeluar' => Yii::t('app', 'Tanggal Keluar'),
            'jabatan' => Yii::t('app', 'Jabatan'),
            'noIdentitas' => Yii::t('app', 'No Identitas'),
            'jenisInstansi_id' => Yii::t('app', 'Jenis Instansi ID'),
            'orang_id' => Yii::t('app', 'Orang ID'),
            'pekerjaanLainnya' => Yii::t('app', 'Pekerjaan Lainnya'),
            'waktuBuat' => Yii::t('app', 'Waktu Buat'),
            'waktuUbah' => Yii::t('app', 'Waktu Ubah'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrang()
    {
        return $this->hasOne(IpbmstOrang::className(), ['ID' => 'orang_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisInstansi()
    {
        return $this->hasOne(Jenisinstansi::className(), ['id' => 'jenisInstansi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanHasInstansis()
    {
        return $this->hasMany(PekerjaanHasInstansi::className(), ['pekerjaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstansis()
    {
        return $this->hasMany(Instansi::className(), ['id' => 'instansi_id'])->viaTable('pekerjaan_has_instansi', ['pekerjaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanHasInstitusis()
    {
        return $this->hasMany(PekerjaanHasInstitusi::className(), ['pekerjaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitusis()
    {
        return $this->hasMany(Institusi::className(), ['ID' => 'institusi_ID'])->viaTable('pekerjaan_has_institusi', ['pekerjaan_id' => 'id']);
    }
}
