<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "department2011".
 *
 * @property string $Department
 * @property integer $lessthan25
 * @property integer $btween25to29
 */
class Department2011 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'department2011';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Department', 'lessthan25', 'btween25to29'], 'required'],
            [['lessthan25', 'btween25to29'], 'integer'],
            [['Department'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Department' => Yii::t('app', 'Department'),
            'lessthan25' => Yii::t('app', 'Lessthan25'),
            'btween25to29' => Yii::t('app', 'Btween25to29'),
        ];
    }
}
