<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tahunmasukmagister".
 *
 * @property string $NIM
 * @property integer $TahunMasuk
 * @property integer $JenisKelaminID
 * @property string $Usia
 */
class Tahunmasukmagister extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tahunmasukmagister';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NIM', 'JenisKelaminID'], 'required'],
            [['TahunMasuk', 'JenisKelaminID', 'Usia'], 'integer'],
            [['NIM'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NIM' => Yii::t('app', 'NIM'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'JenisKelaminID' => Yii::t('app', 'Jenis Kelamin ID'),
            'Usia' => Yii::t('app', 'Usia'),
        ];
    }
     public static function primaryKey()
    {
        return array('NIM');
    }
}
