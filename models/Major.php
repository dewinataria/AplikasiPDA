<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "major".
 *
 * @property string $major
 * @property integer $lessthan25
 * @property integer $btween25to29
 */
class Major extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'major';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['major', 'lessthan25', 'btween25to29'], 'required'],
            [['lessthan25', 'btween25to29'], 'integer'],
            [['major'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'major' => Yii::t('app', 'Major'),
            'lessthan25' => Yii::t('app', 'Lessthan25'),
            'btween25to29' => Yii::t('app', 'Btween25to29'),
        ];
    }
}
