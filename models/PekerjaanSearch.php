<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pekerjaan;

/**
 * PekerjaanSearch represents the model behind the search form about `app\models\Pekerjaan`.
 */
class PekerjaanSearch extends Pekerjaan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'jenisInstansi_id', 'orang_id'], 'integer'],
            [['tanggalMasuk', 'tanggalKeluar', 'jabatan', 'noIdentitas', 'pekerjaanLainnya', 'waktuBuat', 'waktuUbah'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pekerjaan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tanggalMasuk' => $this->tanggalMasuk,
            'tanggalKeluar' => $this->tanggalKeluar,
            'jenisInstansi_id' => $this->jenisInstansi_id,
            'orang_id' => $this->orang_id,
            'waktuBuat' => $this->waktuBuat,
            'waktuUbah' => $this->waktuUbah,
        ]);

        $query->andFilterWhere(['like', 'jabatan', $this->jabatan])
            ->andFilterWhere(['like', 'noIdentitas', $this->noIdentitas])
            ->andFilterWhere(['like', 'pekerjaanLainnya', $this->pekerjaanLainnya]);

        return $dataProvider;
    }
}
