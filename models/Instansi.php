<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instansi".
 *
 * @property integer $id
 * @property string $nama
 * @property string $tlp
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property integer $negara_id
 *
 * @property PekerjaanHasInstansi[] $pekerjaanHasInstansis
 * @property Pekerjaan[] $pekerjaans
 */
class Instansi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instansi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'negara_id'], 'integer'],
            [['nama'], 'string', 'max' => 250],
            [['tlp'], 'string', 'max' => 45],
            [['fax', 'website'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'tlp' => Yii::t('app', 'Tlp'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'website' => Yii::t('app', 'Website'),
            'negara_id' => Yii::t('app', 'Negara ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanHasInstansis()
    {
        return $this->hasMany(PekerjaanHasInstansi::className(), ['instansi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaans()
    {
        return $this->hasMany(Pekerjaan::className(), ['id' => 'pekerjaan_id'])->viaTable('pekerjaan_has_instansi', ['instansi_id' => 'id']);
    }
}
