<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manajemenjalurmasuk".
 *
 * @property integer $id
 * @property integer $ProgramStudi_id
 * @property integer $jalurmasuk_id
 * @property integer $program_id
 * @property integer $aktif
 * @property integer $strata
 * @property integer $biayapendidikan_id
 *
 * @property Programstudi $programStudi
 * @property AkdrefStrata $strata0
 * @property Jalurmasuk $jalurmasuk
 */
class Manajemenjalurmasuk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manajemenjalurmasuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'ProgramStudi_id', 'jalurmasuk_id', 'program_id', 'aktif', 'strata', 'biayapendidikan_id'], 'integer'],
            [['ProgramStudi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Programstudi::className(), 'targetAttribute' => ['ProgramStudi_id' => 'id']],
            [['strata'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefStrata::className(), 'targetAttribute' => ['strata' => 'ID']],
            [['jalurmasuk_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jalurmasuk::className(), 'targetAttribute' => ['jalurmasuk_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ProgramStudi_id' => Yii::t('app', 'Program Studi ID'),
            'jalurmasuk_id' => Yii::t('app', 'Jalurmasuk ID'),
            'program_id' => Yii::t('app', 'Program ID'),
            'aktif' => Yii::t('app', 'Aktif'),
            'strata' => Yii::t('app', 'Strata'),
            'biayapendidikan_id' => Yii::t('app', 'Biayapendidikan ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramStudi()
    {
        return $this->hasOne(Programstudi::className(), ['id' => 'ProgramStudi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrata0()
    {
        return $this->hasOne(AkdrefStrata::className(), ['ID' => 'strata']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJalurmasuk()
    {
        return $this->hasOne(Jalurmasuk::className(), ['id' => 'jalurmasuk_id']);
    }
}
