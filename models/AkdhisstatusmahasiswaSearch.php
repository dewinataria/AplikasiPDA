<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdhisstatusmahasiswa;

/**
 * AkdhisstatusmahasiswaSearch represents the model behind the search form about `app\models\Akdhisstatusmahasiswa`.
 */
class AkdhisstatusmahasiswaSearch extends Akdhisstatusmahasiswa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MahasiswaID', 'TahunSemesterID', 'SemesterMahasiswa', 'StatusAkademikID'], 'integer'],
            [['NIM', 'TanggalStatus', 'NoSurat', 'Keterangan', 'StatusMahasiswaBKey'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdhisstatusmahasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'MahasiswaID' => $this->MahasiswaID,
            'TahunSemesterID' => $this->TahunSemesterID,
            'SemesterMahasiswa' => $this->SemesterMahasiswa,
            'StatusAkademikID' => $this->StatusAkademikID,
            'TanggalStatus' => $this->TanggalStatus,
        ]);

        $query->andFilterWhere(['like', 'NIM', $this->NIM])
            ->andFilterWhere(['like', 'NoSurat', $this->NoSurat])
            ->andFilterWhere(['like', 'Keterangan', $this->Keterangan])
            ->andFilterWhere(['like', 'StatusMahasiswaBKey', $this->StatusMahasiswaBKey]);

        return $dataProvider;
    }
}
