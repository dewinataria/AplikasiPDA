<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ipbmstdepartemen;

/**
 * IpbmstdepartemenSearch represents the model behind the search form about `app\models\Ipbmstdepartemen`.
 */
class IpbmstdepartemenSearch extends Ipbmstdepartemen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'FakultasID', 'Kadep'], 'integer'],
            [['Kode', 'TanggalDibentuk', 'DepartemenBKey'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ipbmstdepartemen::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'FakultasID' => $this->FakultasID,
            'Kadep' => $this->Kadep,
            'TanggalDibentuk' => $this->TanggalDibentuk,
        ]);

        $query->andFilterWhere(['like', 'Kode', $this->Kode])
            ->andFilterWhere(['like', 'DepartemenBKey', $this->DepartemenBKey]);

        return $dataProvider;
    }
}
