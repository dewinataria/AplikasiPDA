<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "studi".
 *
 * @property string $Fakultas
 * @property string $Departemen
 * @property string $Mayor
 * @property string $NIM
 * @property string $LamaStudi
 * @property integer $TahunMasuk
 */
class Studi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'studi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NIM'], 'required'],
            [['LamaStudi', 'TahunMasuk'], 'integer'],
            [['Fakultas'], 'string', 'max' => 200],
            [['Departemen'], 'string', 'max' => 5],
            [['Mayor'], 'string', 'max' => 255],
            [['NIM'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Fakultas' => Yii::t('app', 'Fakultas'),
            'Departemen' => Yii::t('app', 'Departemen'),
            'Mayor' => Yii::t('app', 'Mayor'),
            'NIM' => Yii::t('app', 'Nim'),
            'LamaStudi' => Yii::t('app', 'Lama Studi'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
        ];
    }

     public static function primaryKey()
    {
        return array('NIM');
    }
}
