<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\S2view;

/**
 * S2viewSearch represents the model behind the search form about `app\models\S2view`.
 */
class S2viewSearch extends S2view
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fakultas', 'Departemen', 'KodeMayor', 'NIM'], 'safe'],
            [['TahunMasuk'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = S2view::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TahunMasuk' => $this->TahunMasuk,
        ]);

        $query->andFilterWhere(['like', 'Fakultas', $this->Fakultas])
            ->andFilterWhere(['like', 'Departemen', $this->Departemen])
            ->andFilterWhere(['like', 'KodeMayor', $this->KodeMayor])
            ->andFilterWhere(['like', 'NIM', $this->NIM]);

        return $dataProvider;
    }
}
