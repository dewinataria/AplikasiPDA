<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdhis_statusmahasiswa".
 *
 * @property integer $ID
 * @property integer $MahasiswaID
 * @property string $NIM
 * @property integer $TahunSemesterID
 * @property integer $SemesterMahasiswa
 * @property integer $StatusAkademikID
 * @property string $TanggalStatus
 * @property string $NoSurat
 * @property string $Keterangan
 * @property string $StatusMahasiswaBKey
 *
 * @property AkdmstMahasiswa $mahasiswa
 * @property AkdrefStatusakademik $statusAkademik
 * @property AkdrefTahunsemester $tahunSemester
 */
class AkdhisStatusmahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdhis_statusmahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TahunSemesterID', 'StatusAkademikID'], 'required'],
            [['ID', 'MahasiswaID', 'TahunSemesterID', 'SemesterMahasiswa', 'StatusAkademikID'], 'integer'],
            [['TanggalStatus'], 'safe'],
            [['NIM'], 'string', 'max' => 11],
            [['NoSurat'], 'string', 'max' => 100],
            [['Keterangan'], 'string', 'max' => 255],
            [['StatusMahasiswaBKey'], 'string', 'max' => 50],
            [['MahasiswaID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdmstMahasiswa::className(), 'targetAttribute' => ['MahasiswaID' => 'ID']],
            [['StatusAkademikID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefStatusakademik::className(), 'targetAttribute' => ['StatusAkademikID' => 'ID']],
            [['TahunSemesterID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefTahunsemester::className(), 'targetAttribute' => ['TahunSemesterID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MahasiswaID' => Yii::t('app', 'Mahasiswa ID'),
            'NIM' => Yii::t('app', 'Nim'),
            'TahunSemesterID' => Yii::t('app', 'Tahun Semester ID'),
            'SemesterMahasiswa' => Yii::t('app', 'Semester Mahasiswa'),
            'StatusAkademikID' => Yii::t('app', 'Status Akademik ID'),
            'TanggalStatus' => Yii::t('app', 'Tanggal Status'),
            'NoSurat' => Yii::t('app', 'No Surat'),
            'Keterangan' => Yii::t('app', 'Keterangan'),
            'StatusMahasiswaBKey' => Yii::t('app', 'Status Mahasiswa Bkey'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(AkdmstMahasiswa::className(), ['ID' => 'MahasiswaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusAkademik()
    {
        return $this->hasOne(AkdrefStatusakademik::className(), ['ID' => 'StatusAkademikID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTahunSemester()
    {
        return $this->hasOne(AkdrefTahunsemester::className(), ['ID' => 'TahunSemesterID']);
    }
}
