<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdmst_mayor".
 *
 * @property integer $ID
 * @property integer $StrukturOrganisasiID
 * @property integer $DepartemenID
 * @property integer $StrataID
 * @property string $Kode
 * @property string $KodePDPT
 * @property string $Nama
 * @property string $NamaEn
 * @property string $Inisial
 * @property integer $Kaprodi
 * @property string $TanggalDibentuk
 * @property string $NomorSKDibentuk
 * @property string $MayorSarjanaBKey
 * @property string $MayorPascasarjanaBKey
 * @property string $TanggalDitutup
 * @property string $NomorSKDitutup
 *
 * @property AkdhisAkreditasips[] $akdhisAkreditasips
 * @property AkdmstMahasiswadoktor[] $akdmstMahasiswadoktors
 * @property AkdmstMahasiswamagister1[] $akdmstMahasiswamagister1s
 * @property AkdrefStrata $strata
 * @property IpbmstDepartemen $departemen
 */
class Akdmstmayor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdmst_mayor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'DepartemenID', 'StrataID', 'Nama'], 'required'],
            [['ID', 'StrukturOrganisasiID', 'DepartemenID', 'StrataID', 'Kaprodi'], 'integer'],
            [['TanggalDibentuk', 'TanggalDitutup'], 'safe'],
            [['Kode'], 'string', 'max' => 10],
            [['KodePDPT', 'MayorSarjanaBKey', 'MayorPascasarjanaBKey'], 'string', 'max' => 5],
            [['Nama', 'NamaEn'], 'string', 'max' => 255],
            [['Inisial'], 'string', 'max' => 100],
            [['NomorSKDibentuk'], 'string', 'max' => 15],
            [['NomorSKDitutup'], 'string', 'max' => 50],
            [['StrataID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefStrata::className(), 'targetAttribute' => ['StrataID' => 'ID']],
            [['DepartemenID'], 'exist', 'skipOnError' => true, 'targetClass' => IpbmstDepartemen::className(), 'targetAttribute' => ['DepartemenID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'StrukturOrganisasiID' => Yii::t('app', 'Struktur Organisasi ID'),
            'DepartemenID' => Yii::t('app', 'Departemen ID'),
            'StrataID' => Yii::t('app', 'Strata ID'),
            'Kode' => Yii::t('app', 'Kode'),
            'KodePDPT' => Yii::t('app', 'Kode Pdpt'),
            'Nama' => Yii::t('app', 'Nama'),
            'NamaEn' => Yii::t('app', 'Nama En'),
            'Inisial' => Yii::t('app', 'Inisial'),
            'Kaprodi' => Yii::t('app', 'Kaprodi'),
            'TanggalDibentuk' => Yii::t('app', 'Tanggal Dibentuk'),
            'NomorSKDibentuk' => Yii::t('app', 'Nomor Skdibentuk'),
            'MayorSarjanaBKey' => Yii::t('app', 'Mayor Sarjana Bkey'),
            'MayorPascasarjanaBKey' => Yii::t('app', 'Mayor Pascasarjana Bkey'),
            'TanggalDitutup' => Yii::t('app', 'Tanggal Ditutup'),
            'NomorSKDitutup' => Yii::t('app', 'Nomor Skditutup'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisAkreditasips()
    {
        return $this->hasMany(AkdhisAkreditasips::className(), ['MayorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswadoktors()
    {
        return $this->hasMany(AkdmstMahasiswadoktor::className(), ['MayorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswamagister1s()
    {
        return $this->hasMany(AkdmstMahasiswamagister1::className(), ['MayorID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrata()
    {
        return $this->hasOne(AkdrefStrata::className(), ['ID' => 'StrataID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartemen()
    {
        return $this->hasOne(IpbmstDepartemen::className(), ['ID' => 'DepartemenID']);
    }
}
