<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipbmst_fakultas".
 *
 * @property integer $ID
 * @property string $Kode
 * @property integer $Dekan
 * @property string $TanggalDibentuk
 * @property string $FakultasBKey
 *
 * @property IpbmstDepartemen[] $ipbmstDepartemens
 */
class Ipbmstfakultas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ipbmst_fakultas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Kode'], 'required'],
            [['ID', 'Dekan'], 'integer'],
            [['TanggalDibentuk'], 'safe'],
            [['Kode'], 'string', 'max' => 5],
            [['FakultasBKey'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Kode' => Yii::t('app', 'Kode'),
            'Dekan' => Yii::t('app', 'Dekan'),
            'TanggalDibentuk' => Yii::t('app', 'Tanggal Dibentuk'),
            'FakultasBKey' => Yii::t('app', 'Fakultas Bkey'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpbmstDepartemens()
    {
        return $this->hasMany(IpbmstDepartemen::className(), ['FakultasID' => 'ID']);
    }
}
