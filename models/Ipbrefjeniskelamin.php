<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipbref_jeniskelamin".
 *
 * @property integer $id
 * @property string $nama
 *
 * @property IpbmstOrang[] $ipbmstOrangs
 */
class Ipbrefjeniskelamin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ipbref_jeniskelamin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['nama'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpbmstOrangs()
    {
        return $this->hasMany(IpbmstOrang::className(), ['JenisKelaminID' => 'id']);
    }
}
