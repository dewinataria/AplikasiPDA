<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdtrx_wisuda".
 *
 * @property integer $ID
 * @property integer $MahasiswaID
 * @property string $NIM
 * @property integer $NomorIjazah
 * @property string $TanggalLulus
 * @property string $TanggalIjazah
 * @property string $TanggalDaftar
 * @property integer $GelarID
 * @property integer $SyaratBuktiLaporan
 * @property string $SyaratBuktiLaporanPath
 * @property integer $SyaratBebasPustaka
 * @property string $SyaratBebasPustakaPath
 * @property integer $SyaratLembarPengesahan
 * @property string $SyaratLembarPengesahanPath
 * @property integer $SyaratPasFoto
 * @property string $SyaratPasFotoPath
 * @property integer $SyaratBuktiBayar
 * @property string $SyaratBuktiBayarPath
 * @property integer $SyaratIjazahSMA
 * @property string $SyaratIjazahSMAPath
 * @property integer $HadirWisuda
 * @property integer $UlangWisuda
 * @property integer $LamaStudi
 * @property string $JumlahTransferSKS
 * @property integer $TahapWisudaID
 *
 * @property AkdmstMahasiswa $mahasiswa
 */
class Akdtrxwisuda extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdtrx_wisuda';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'MahasiswaID', 'NomorIjazah', 'GelarID', 'SyaratBuktiLaporan', 'SyaratBebasPustaka', 'SyaratLembarPengesahan', 'SyaratPasFoto', 'SyaratBuktiBayar', 'SyaratIjazahSMA', 'HadirWisuda', 'UlangWisuda', 'LamaStudi', 'TahapWisudaID'], 'integer'],
            [['TanggalLulus', 'TanggalIjazah', 'TanggalDaftar'], 'safe'],
            [['NIM'], 'string', 'max' => 50],
            [['SyaratBuktiLaporanPath', 'SyaratBebasPustakaPath', 'SyaratLembarPengesahanPath', 'SyaratPasFotoPath', 'SyaratBuktiBayarPath', 'SyaratIjazahSMAPath', 'JumlahTransferSKS'], 'string', 'max' => 45],
            [['MahasiswaID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdmstMahasiswa::className(), 'targetAttribute' => ['MahasiswaID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MahasiswaID' => Yii::t('app', 'Mahasiswa ID'),
            'NIM' => Yii::t('app', 'Nim'),
            'NomorIjazah' => Yii::t('app', 'Nomor Ijazah'),
            'TanggalLulus' => Yii::t('app', 'Tanggal Lulus'),
            'TanggalIjazah' => Yii::t('app', 'Tanggal Ijazah'),
            'TanggalDaftar' => Yii::t('app', 'Tanggal Daftar'),
            'GelarID' => Yii::t('app', 'Gelar ID'),
            'SyaratBuktiLaporan' => Yii::t('app', 'Syarat Bukti Laporan'),
            'SyaratBuktiLaporanPath' => Yii::t('app', 'Syarat Bukti Laporan Path'),
            'SyaratBebasPustaka' => Yii::t('app', 'Syarat Bebas Pustaka'),
            'SyaratBebasPustakaPath' => Yii::t('app', 'Syarat Bebas Pustaka Path'),
            'SyaratLembarPengesahan' => Yii::t('app', 'Syarat Lembar Pengesahan'),
            'SyaratLembarPengesahanPath' => Yii::t('app', 'Syarat Lembar Pengesahan Path'),
            'SyaratPasFoto' => Yii::t('app', 'Syarat Pas Foto'),
            'SyaratPasFotoPath' => Yii::t('app', 'Syarat Pas Foto Path'),
            'SyaratBuktiBayar' => Yii::t('app', 'Syarat Bukti Bayar'),
            'SyaratBuktiBayarPath' => Yii::t('app', 'Syarat Bukti Bayar Path'),
            'SyaratIjazahSMA' => Yii::t('app', 'Syarat Ijazah Sma'),
            'SyaratIjazahSMAPath' => Yii::t('app', 'Syarat Ijazah Smapath'),
            'HadirWisuda' => Yii::t('app', 'Hadir Wisuda'),
            'UlangWisuda' => Yii::t('app', 'Ulang Wisuda'),
            'LamaStudi' => Yii::t('app', 'Lama Studi'),
            'JumlahTransferSKS' => Yii::t('app', 'Jumlah Transfer Sks'),
            'TahapWisudaID' => Yii::t('app', 'Tahap Wisuda ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(AkdmstMahasiswa::className(), ['ID' => 'MahasiswaID']);
    }
}
