<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rangkuman;

/**
 * RangkumanSearch represents the model behind the search form about `app\models\Rangkuman`.
 */
class RangkumanSearch extends Rangkuman
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fakultas', 'Departemen', 'KodeMayor', 'NIM', 'JenisKelamin', 'StatusAkademik'], 'safe'],
            [['TahunMasuk', 'Usia'], 'integer'],
            [['IPK'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rangkuman::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'TahunMasuk' => $this->TahunMasuk,
            'IPK' => $this->IPK,
            'Usia' => $this->Usia,
        ]);

        $query->andFilterWhere(['like', 'Fakultas', $this->Fakultas])
            ->andFilterWhere(['like', 'Departemen', $this->Departemen])
            ->andFilterWhere(['like', 'KodeMayor', $this->KodeMayor])
            ->andFilterWhere(['like', 'NIM', $this->NIM])
            ->andFilterWhere(['like', 'JenisKelamin', $this->JenisKelamin])
            ->andFilterWhere(['like', 'StatusAkademik', $this->StatusAkademik]);

        return $dataProvider;
    }
}
