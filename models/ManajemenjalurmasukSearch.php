<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Manajemenjalurmasuk;

/**
 * ManajemenjalurmasukSearch represents the model behind the search form about `app\models\Manajemenjalurmasuk`.
 */
class ManajemenjalurmasukSearch extends Manajemenjalurmasuk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'ProgramStudi_id', 'jalurmasuk_id', 'program_id', 'aktif', 'strata', 'biayapendidikan_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Manajemenjalurmasuk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ProgramStudi_id' => $this->ProgramStudi_id,
            'jalurmasuk_id' => $this->jalurmasuk_id,
            'program_id' => $this->program_id,
            'aktif' => $this->aktif,
            'strata' => $this->strata,
            'biayapendidikan_id' => $this->biayapendidikan_id,
        ]);

        return $dataProvider;
    }
}
