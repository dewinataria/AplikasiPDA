<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alamat".
 *
 * @property integer $id
 * @property string $jalan
 * @property string $kodepos
 * @property string $rt
 * @property string $rw
 * @property string $desakelurahan_kode
 *
 * @property InstitusiHasAlamat[] $institusiHasAlamats
 * @property Institusi[] $institusis
 * @property IpbmstOrangHasAlamat[] $ipbmstOrangHasAlamats
 * @property IpbmstOrang[] $ipbmstOrangs
 */
class Alamat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alamat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['jalan'], 'string', 'max' => 200],
            [['kodepos', 'rw'], 'string', 'max' => 45],
            [['rt', 'desakelurahan_kode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'jalan' => Yii::t('app', 'Jalan'),
            'kodepos' => Yii::t('app', 'Kodepos'),
            'rt' => Yii::t('app', 'Rt'),
            'rw' => Yii::t('app', 'Rw'),
            'desakelurahan_kode' => Yii::t('app', 'Desakelurahan Kode'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitusiHasAlamats()
    {
        return $this->hasMany(InstitusiHasAlamat::className(), ['alamat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitusis()
    {
        return $this->hasMany(Institusi::className(), ['ID' => 'institusi_ID'])->viaTable('institusi_has_alamat', ['alamat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpbmstOrangHasAlamats()
    {
        return $this->hasMany(IpbmstOrangHasAlamat::className(), ['alamat_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpbmstOrangs()
    {
        return $this->hasMany(IpbmstOrang::className(), ['ID' => 'ipbmst_orang_ID'])->viaTable('ipbmst_orang_has_alamat', ['alamat_id' => 'id']);
    }
}
