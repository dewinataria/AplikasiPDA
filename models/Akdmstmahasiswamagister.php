<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdmst_mahasiswamagister".
 *
 * @property integer $ID
 * @property integer $MahasiswaID
 * @property string $NIM
 * @property integer $MayorID
 * @property integer $MinorID
 * @property integer $JalurMasukID
 * @property string $TanggalMasuk
 * @property integer $StatusAkademikID
 * @property integer $BatasStudi
 * @property integer $TahunAkademikID
 */
class Akdmstmahasiswamagister extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdmst_mahasiswamagister';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MahasiswaID', 'NIM', 'MayorID', 'MinorID', 'JalurMasukID', 'TahunAkademikID'], 'required'],
            [['ID', 'MahasiswaID', 'MayorID', 'MinorID', 'JalurMasukID', 'StatusAkademikID', 'BatasStudi', 'TahunAkademikID'], 'integer'],
            [['TanggalMasuk'], 'safe'],
            [['NIM'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MahasiswaID' => Yii::t('app', 'Mahasiswa ID'),
            'NIM' => Yii::t('app', 'Nim'),
            'MayorID' => Yii::t('app', 'Mayor ID'),
            'MinorID' => Yii::t('app', 'Minor ID'),
            'JalurMasukID' => Yii::t('app', 'Jalur Masuk ID'),
            'TanggalMasuk' => Yii::t('app', 'Tanggal Masuk'),
            'StatusAkademikID' => Yii::t('app', 'Status Akademik ID'),
            'BatasStudi' => Yii::t('app', 'Batas Studi'),
            'TahunAkademikID' => Yii::t('app', 'Tahun Akademik ID'),
        ];
    }
}
