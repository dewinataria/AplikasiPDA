<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdmst_mahasiswa".
 *
 * @property integer $ID
 * @property integer $OrangID
 * @property integer $StrataID
 * @property string $nimkey
 *
 * @property AkdhisKelanjutanstudi[] $akdhisKelanjutanstudis
 * @property AkdhisStatusmahasiswa[] $akdhisStatusmahasiswas
 * @property AkdrefStrata $strata
 * @property IpbmstOrang $orang
 * @property AkdmstMahasiswadoktor[] $akdmstMahasiswadoktors
 * @property AkdmstMahasiswamagister1[] $akdmstMahasiswamagister1s
 * @property AkdtrxWisuda[] $akdtrxWisudas
 */
class Akdmstmahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdmst_mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'OrangID', 'StrataID'], 'integer'],
            [['nimkey'], 'string', 'max' => 45],
            [['StrataID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefStrata::className(), 'targetAttribute' => ['StrataID' => 'ID']],
            [['OrangID'], 'exist', 'skipOnError' => true, 'targetClass' => IpbmstOrang::className(), 'targetAttribute' => ['OrangID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'OrangID' => Yii::t('app', 'Orang ID'),
            'StrataID' => Yii::t('app', 'Strata ID'),
            'nimkey' => Yii::t('app', 'Nimkey'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisKelanjutanstudis()
    {
        return $this->hasMany(AkdhisKelanjutanstudi::className(), ['MahasiswaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisStatusmahasiswas()
    {
        return $this->hasMany(AkdhisStatusmahasiswa::className(), ['MahasiswaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrata()
    {
        return $this->hasOne(AkdrefStrata::className(), ['ID' => 'StrataID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrang()
    {
        return $this->hasOne(IpbmstOrang::className(), ['ID' => 'OrangID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswadoktors()
    {
        return $this->hasMany(AkdmstMahasiswadoktor::className(), ['MahasiswaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswamagister1s()
    {
        return $this->hasMany(AkdmstMahasiswamagister1::className(), ['MahasiswaID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdtrxWisudas()
    {
        return $this->hasMany(AkdtrxWisuda::className(), ['MahasiswaID' => 'ID']);
    }
}
