<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipk".
 *
 * @property string $NIM
 * @property double $IPK
 * @property string $Fakultas
 * @property string $Departemen
 * @property string $KodeMayor
 * @property integer $TahunMasuk
 * @property string $StatusAkademik
 * @property integer $TahunAkademikID
 * @property string $Semester
 */
class Ipk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ipk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NIM'], 'required'],
            [['IPK'], 'number'],
            [['TahunMasuk', 'TahunAkademikID'], 'integer'],
            [['NIM'], 'string', 'max' => 11],
            [['Fakultas', 'Departemen'], 'string', 'max' => 5],
            [['KodeMayor'], 'string', 'max' => 255],
            [['StatusAkademik'], 'string', 'max' => 30],
            [['Semester'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'NIM' => Yii::t('app', 'Nim'),
            'IPK' => Yii::t('app', 'Ipk'),
            'Fakultas' => Yii::t('app', 'Fakultas'),
            'Departemen' => Yii::t('app', 'Departemen'),
            'KodeMayor' => Yii::t('app', 'Kode Mayor'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'StatusAkademik' => Yii::t('app', 'Status Akademik'),
            'TahunAkademikID' => Yii::t('app', 'Tahun Akademik ID'),
            'Semester' => Yii::t('app', 'Semester'),
        ];
    }

    public static function primaryKey()
    {
        return array('NIM');
    }
}
