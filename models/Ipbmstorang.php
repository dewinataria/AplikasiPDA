<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipbmst_orang".
 *
 * @property integer $ID
 * @property string $Nama
 * @property string $TempatLahir
 * @property integer $TempatLahirID
 * @property string $TanggalLahir
 * @property integer $JenisKelaminID
 * @property string $NIMS1Key
 * @property string $NIMS2Key
 * @property string $NIMS3KEY
 * @property string $Pgwawikey
 * @property string $PasanganKey
 * @property string $Anakvkey
 * @property string $S2Lama
 * @property string $S3Lama
 *
 * @property AkdmstMahasiswa[] $akdmstMahasiswas
 * @property IpbrefJeniskelamin $jenisKelamin
 * @property IpbmstOrangHasAlamat[] $ipbmstOrangHasAlamats
 * @property Alamat[] $alamats
 * @property Pekerjaan[] $pekerjaans
 * @property Pendaftaran[] $pendaftarans
 */
class Ipbmstorang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ipbmst_orang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'TempatLahirID', 'JenisKelaminID'], 'integer'],
            [['TanggalLahir'], 'safe'],
            [['Nama'], 'string', 'max' => 100],
            [['TempatLahir', 'NIMS1Key', 'NIMS2Key', 'NIMS3KEY', 'Pgwawikey', 'PasanganKey', 'Anakvkey', 'S2Lama', 'S3Lama'], 'string', 'max' => 50],
            [['JenisKelaminID'], 'exist', 'skipOnError' => true, 'targetClass' => IpbrefJeniskelamin::className(), 'targetAttribute' => ['JenisKelaminID' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nama' => Yii::t('app', 'Nama'),
            'TempatLahir' => Yii::t('app', 'Tempat Lahir'),
            'TempatLahirID' => Yii::t('app', 'Tempat Lahir ID'),
            'TanggalLahir' => Yii::t('app', 'Tanggal Lahir'),
            'JenisKelaminID' => Yii::t('app', 'Jenis Kelamin ID'),
            'NIMS1Key' => Yii::t('app', 'Nims1 Key'),
            'NIMS2Key' => Yii::t('app', 'Nims2 Key'),
            'NIMS3KEY' => Yii::t('app', 'Nims3 Key'),
            'Pgwawikey' => Yii::t('app', 'Pgwawikey'),
            'PasanganKey' => Yii::t('app', 'Pasangan Key'),
            'Anakvkey' => Yii::t('app', 'Anakvkey'),
            'S2Lama' => Yii::t('app', 'S2 Lama'),
            'S3Lama' => Yii::t('app', 'S3 Lama'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswas()
    {
        return $this->hasMany(AkdmstMahasiswa::className(), ['OrangID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisKelamin()
    {
        return $this->hasOne(IpbrefJeniskelamin::className(), ['id' => 'JenisKelaminID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpbmstOrangHasAlamats()
    {
        return $this->hasMany(IpbmstOrangHasAlamat::className(), ['ipbmst_orang_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlamats()
    {
        return $this->hasMany(Alamat::className(), ['id' => 'alamat_id'])->viaTable('ipbmst_orang_has_alamat', ['ipbmst_orang_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaans()
    {
        return $this->hasMany(Pekerjaan::className(), ['orang_id' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendaftarans()
    {
        return $this->hasMany(Pendaftaran::className(), ['OrangID' => 'ID']);
    }
}
