<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "institusi".
 *
 * @property integer $ID
 * @property string $Nama
 * @property string $KodePT
 * @property string $tlp
 * @property string $fax
 * @property string $email
 * @property string $website
 * @property string $tanggalBerdiri
 * @property string $nomorSKPT
 * @property string $tanggalSKPT
 * @property integer $status
 * @property integer $negara_id
 * @property integer $jenisInstitusi_id
 *
 * @property Jenisinstitusi $jenisInstitusi
 * @property InstitusiHasAlamat[] $institusiHasAlamats
 * @property Alamat[] $alamats
 * @property PekerjaanHasInstitusi[] $pekerjaanHasInstitusis
 * @property Pekerjaan[] $pekerjaans
 */
class Institusi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'institusi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'status', 'negara_id', 'jenisInstitusi_id'], 'integer'],
            [['tanggalBerdiri', 'tanggalSKPT'], 'safe'],
            [['Nama'], 'string', 'max' => 250],
            [['KodePT'], 'string', 'max' => 15],
            [['tlp', 'nomorSKPT'], 'string', 'max' => 45],
            [['fax', 'website'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['jenisInstitusi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jenisinstitusi::className(), 'targetAttribute' => ['jenisInstitusi_id' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nama' => Yii::t('app', 'Nama'),
            'KodePT' => Yii::t('app', 'Kode Pt'),
            'tlp' => Yii::t('app', 'Tlp'),
            'fax' => Yii::t('app', 'Fax'),
            'email' => Yii::t('app', 'Email'),
            'website' => Yii::t('app', 'Website'),
            'tanggalBerdiri' => Yii::t('app', 'Tanggal Berdiri'),
            'nomorSKPT' => Yii::t('app', 'Nomor Skpt'),
            'tanggalSKPT' => Yii::t('app', 'Tanggal Skpt'),
            'status' => Yii::t('app', 'Status'),
            'negara_id' => Yii::t('app', 'Negara ID'),
            'jenisInstitusi_id' => Yii::t('app', 'Jenis Institusi ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisInstitusi()
    {
        return $this->hasOne(Jenisinstitusi::className(), ['ID' => 'jenisInstitusi_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitusiHasAlamats()
    {
        return $this->hasMany(InstitusiHasAlamat::className(), ['institusi_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlamats()
    {
        return $this->hasMany(Alamat::className(), ['id' => 'alamat_id'])->viaTable('institusi_has_alamat', ['institusi_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaanHasInstitusis()
    {
        return $this->hasMany(PekerjaanHasInstitusi::className(), ['institusi_ID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaans()
    {
        return $this->hasMany(Pekerjaan::className(), ['id' => 'pekerjaan_id'])->viaTable('pekerjaan_has_institusi', ['institusi_ID' => 'ID']);
    }
}
