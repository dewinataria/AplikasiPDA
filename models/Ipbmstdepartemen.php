<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipbmst_departemen".
 *
 * @property integer $ID
 * @property integer $FakultasID
 * @property string $Kode
 * @property integer $Kadep
 * @property string $TanggalDibentuk
 * @property string $DepartemenBKey
 *
 * @property AkdmstMayor[] $akdmstMayors
 * @property IpbmstFakultas $fakultas
 */
class Ipbmstdepartemen extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ipbmst_departemen';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'FakultasID', 'Kode'], 'required'],
            [['ID', 'FakultasID', 'Kadep'], 'integer'],
            [['TanggalDibentuk'], 'safe'],
            [['Kode'], 'string', 'max' => 5],
            [['DepartemenBKey'], 'string', 'max' => 10],
            [['FakultasID'], 'exist', 'skipOnError' => true, 'targetClass' => IpbmstFakultas::className(), 'targetAttribute' => ['FakultasID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'FakultasID' => Yii::t('app', 'Fakultas ID'),
            'Kode' => Yii::t('app', 'Kode'),
            'Kadep' => Yii::t('app', 'Kadep'),
            'TanggalDibentuk' => Yii::t('app', 'Tanggal Dibentuk'),
            'DepartemenBKey' => Yii::t('app', 'Departemen Bkey'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMayors()
    {
        return $this->hasMany(AkdmstMayor::className(), ['DepartemenID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFakultas()
    {
        return $this->hasOne(IpbmstFakultas::className(), ['ID' => 'FakultasID']);
    }
}
