<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_strata".
 *
 * @property integer $ID
 * @property string $Kode
 * @property string $Nama
 *
 * @property AkdmstMahasiswa[] $akdmstMahasiswas
 * @property AkdmstMayor[] $akdmstMayors
 * @property AkdrefJalurmasuk[] $akdrefJalurmasuks
 * @property Manajemenjalurmasuk[] $manajemenjalurmasuks
 */
class Akdrefstrata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_strata';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'integer'],
            [['Kode'], 'string', 'max' => 5],
            [['Nama'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Kode' => Yii::t('app', 'Kode'),
            'Nama' => Yii::t('app', 'Nama'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswas()
    {
        return $this->hasMany(AkdmstMahasiswa::className(), ['StrataID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMayors()
    {
        return $this->hasMany(AkdmstMayor::className(), ['StrataID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdrefJalurmasuks()
    {
        return $this->hasMany(AkdrefJalurmasuk::className(), ['StrataID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManajemenjalurmasuks()
    {
        return $this->hasMany(Manajemenjalurmasuk::className(), ['strata' => 'ID']);
    }
}
