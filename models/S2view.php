<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "s2view".
 *
 * @property string $Fakultas
 * @property string $Departemen
 * @property string $KodeMayor
 * @property string $NIM
 * @property integer $TahunMasuk
 */
class S2view extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 's2view';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fakultas', 'Departemen', 'NIM'], 'required'],
            [['TahunMasuk'], 'integer'],
            [['Fakultas', 'Departemen'], 'string', 'max' => 5],
            [['KodeMayor'], 'string', 'max' => 10],
            [['NIM'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Fakultas' => Yii::t('app', 'Fakultas'),
            'Departemen' => Yii::t('app', 'Departemen'),
            'KodeMayor' => Yii::t('app', 'KodeMayor'),
            'NIM' => Yii::t('app', 'NIM'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
        ];
    }
     public static function primaryKey()
    {
        return array('NIM');
    }
}
