<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usia2011".
 *
 * @property string $Mayor
 * @property string $lessthan25
 * @property string $btwn25upto29
 * @property string $btwn30upto34
 * @property string $btwn35upto39
 * @property string $btwn40upto44
 * @property string $btwn45upto49
 * @property string $morethaneqs50
 * @property string $NotComplete
 * @property string $Total
 */
class Usia2011 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usia2011';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lessthan25', 'btwn25upto29', 'btwn30upto34', 'btwn35upto39', 'btwn40upto44', 'btwn45upto49', 'morethaneqs50', 'NotComplete', 'Total'], 'integer'],
            [['Mayor'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Mayor' => Yii::t('app', 'Mayor'),
            'lessthan25' => Yii::t('app', 'Lessthan25'),
            'btwn25upto29' => Yii::t('app', 'Btwn25upto29'),
            'btwn30upto34' => Yii::t('app', 'Btwn30upto34'),
            'btwn35upto39' => Yii::t('app', 'Btwn35upto39'),
            'btwn40upto44' => Yii::t('app', 'Btwn40upto44'),
            'btwn45upto49' => Yii::t('app', 'Btwn45upto49'),
            'morethaneqs50' => Yii::t('app', 'Morethaneqs50'),
            'NotComplete' => Yii::t('app', 'Not Complete'),
            'Total' => Yii::t('app', 'Total'),
        ];
    }
    public static function primaryKey()
    {
        return array('Mayor');
    }
}
