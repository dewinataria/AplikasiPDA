<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jalurmasuk".
 *
 * @property integer $id
 * @property string $nama
 * @property string $deskripsi
 *
 * @property Manajemenjalurmasuk[] $manajemenjalurmasuks
 */
class Jalurmasuk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jalurmasuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['deskripsi'], 'string'],
            [['nama'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManajemenjalurmasuks()
    {
        return $this->hasMany(Manajemenjalurmasuk::className(), ['jalurmasuk_id' => 'id']);
    }
}
