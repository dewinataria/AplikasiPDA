<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_statuskelanjutanstudi".
 *
 * @property integer $ID
 * @property string $Nama
 *
 * @property AkdhisKelanjutanstudi[] $akdhisKelanjutanstudis
 */
class Akdrefstatuskelanjutanstudi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_statuskelanjutanstudi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'integer'],
            [['Nama'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nama' => Yii::t('app', 'Nama'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisKelanjutanstudis()
    {
        return $this->hasMany(AkdhisKelanjutanstudi::className(), ['StatusKelanjutanStudiID' => 'ID']);
    }
}
