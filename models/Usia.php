<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usia".
 *
 * @property integer $TahunMasuk
 * @property string $lessthan25
 * @property string $btween25upto29
 * @property string $btween30upto34
 * @property string $btween35upto39
 * @property string $btween40upto44
 * @property string $btween45upto49
 * @property string $morethanequals50
 * @property string $TidakLengkap
 * @property string $Total
 */
class Usia extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TahunMasuk', 'lessthan25', 'btween25upto29', 'btween30upto34', 'btween35upto39', 'btween40upto44', 'btween45upto49', 'morethanequals50', 'TidakLengkap', 'Total'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'lessthan25' => Yii::t('app', 'Lessthan25'),
            'btween25upto29' => Yii::t('app', 'Btween25upto29'),
            'btween30upto34' => Yii::t('app', 'Btween30upto34'),
            'btween35upto39' => Yii::t('app', 'Btween35upto39'),
            'btween40upto44' => Yii::t('app', 'Btween40upto44'),
            'btween45upto49' => Yii::t('app', 'Btween45upto49'),
            'morethanequals50' => Yii::t('app', 'Morethanequals50'),
            'TidakLengkap' => Yii::t('app', 'Tidak Lengkap'),
            'Total' => Yii::t('app', 'Total'),
        ];
    }
    public static function primaryKey()
    {
        return array('TahunMasuk');
    }
}
