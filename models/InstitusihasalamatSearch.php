<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Institusihasalamat;

/**
 * InstitusihasalamatSearch represents the model behind the search form about `app\models\Institusihasalamat`.
 */
class InstitusihasalamatSearch extends Institusihasalamat
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['institusi_ID', 'alamat_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Institusihasalamat::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'institusi_ID' => $this->institusi_ID,
            'alamat_id' => $this->alamat_id,
        ]);

        return $dataProvider;
    }
}
