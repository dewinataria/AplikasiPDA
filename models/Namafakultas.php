<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "namafakultas".
 *
 * @property string $ID
 * @property string $Nama
 */
class Namafakultas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'namafakultas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Nama'], 'required'],
            [['ID'], 'string', 'max' => 10],
            [['Nama'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nama' => Yii::t('app', 'Nama'),
        ];
    }
}
