<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ipbmstorang;

/**
 * IpbmstorangSearch represents the model behind the search form about `app\models\Ipbmstorang`.
 */
class IpbmstorangSearch extends Ipbmstorang
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TempatLahirID', 'JenisKelaminID'], 'integer'],
            [['Nama', 'TempatLahir', 'TanggalLahir', 'NIMS1Key', 'NIMS2Key', 'NIMS3KEY', 'Pgwawikey', 'PasanganKey', 'Anakvkey', 'S2Lama', 'S3Lama'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ipbmstorang::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'TempatLahirID' => $this->TempatLahirID,
            'TanggalLahir' => $this->TanggalLahir,
            'JenisKelaminID' => $this->JenisKelaminID,
        ]);

        $query->andFilterWhere(['like', 'Nama', $this->Nama])
            ->andFilterWhere(['like', 'TempatLahir', $this->TempatLahir])
            ->andFilterWhere(['like', 'NIMS1Key', $this->NIMS1Key])
            ->andFilterWhere(['like', 'NIMS2Key', $this->NIMS2Key])
            ->andFilterWhere(['like', 'NIMS3KEY', $this->NIMS3KEY])
            ->andFilterWhere(['like', 'Pgwawikey', $this->Pgwawikey])
            ->andFilterWhere(['like', 'PasanganKey', $this->PasanganKey])
            ->andFilterWhere(['like', 'Anakvkey', $this->Anakvkey])
            ->andFilterWhere(['like', 'S2Lama', $this->S2Lama])
            ->andFilterWhere(['like', 'S3Lama', $this->S3Lama]);

        return $dataProvider;
    }
}
