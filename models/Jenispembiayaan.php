<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenispembiayaan".
 *
 * @property integer $ID
 * @property string $title
 * @property string $deskripsi
 *
 * @property Rencanapembiayaan[] $rencanapembiayaans
 */
class Jenispembiayaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenispembiayaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'integer'],
            [['deskripsi'], 'string'],
            [['title'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRencanapembiayaans()
    {
        return $this->hasMany(Rencanapembiayaan::className(), ['JenisPembiayaanID' => 'ID']);
    }
}
