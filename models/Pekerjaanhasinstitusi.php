<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pekerjaan_has_institusi".
 *
 * @property integer $pekerjaan_id
 * @property integer $institusi_ID
 *
 * @property Institusi $institusi
 * @property Pekerjaan $pekerjaan
 */
class Pekerjaanhasinstitusi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pekerjaan_has_institusi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pekerjaan_id', 'institusi_ID'], 'required'],
            [['pekerjaan_id', 'institusi_ID'], 'integer'],
            [['institusi_ID'], 'exist', 'skipOnError' => true, 'targetClass' => Institusi::className(), 'targetAttribute' => ['institusi_ID' => 'ID']],
            [['pekerjaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pekerjaan::className(), 'targetAttribute' => ['pekerjaan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pekerjaan_id' => Yii::t('app', 'Pekerjaan ID'),
            'institusi_ID' => Yii::t('app', 'Institusi  ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitusi()
    {
        return $this->hasOne(Institusi::className(), ['ID' => 'institusi_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPekerjaan()
    {
        return $this->hasOne(Pekerjaan::className(), ['id' => 'pekerjaan_id']);
    }
}
