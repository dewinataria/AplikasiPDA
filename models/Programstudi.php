<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "programstudi".
 *
 * @property integer $id
 * @property string $kode
 * @property string $nama
 * @property string $nama_en
 * @property integer $aktif
 * @property integer $strata
 * @property string $inisial
 * @property string $kode_nasional
 * @property string $sk_pendirian
 * @property string $mandat
 * @property string $visi_misi
 * @property integer $departemen_id
 *
 * @property Manajemenjalurmasuk[] $manajemenjalurmasuks
 * @property PendaftaranHasProgramstudi[] $pendaftaranHasProgramstudis
 */
class Programstudi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'programstudi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'aktif', 'strata', 'departemen_id'], 'integer'],
            [['sk_pendirian', 'mandat', 'visi_misi'], 'string'],
            [['kode'], 'string', 'max' => 5],
            [['nama', 'nama_en'], 'string', 'max' => 255],
            [['inisial', 'kode_nasional'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'kode' => Yii::t('app', 'Kode'),
            'nama' => Yii::t('app', 'Nama'),
            'nama_en' => Yii::t('app', 'Nama En'),
            'aktif' => Yii::t('app', 'Aktif'),
            'strata' => Yii::t('app', 'Strata'),
            'inisial' => Yii::t('app', 'Inisial'),
            'kode_nasional' => Yii::t('app', 'Kode Nasional'),
            'sk_pendirian' => Yii::t('app', 'Sk Pendirian'),
            'mandat' => Yii::t('app', 'Mandat'),
            'visi_misi' => Yii::t('app', 'Visi Misi'),
            'departemen_id' => Yii::t('app', 'Departemen ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManajemenjalurmasuks()
    {
        return $this->hasMany(Manajemenjalurmasuk::className(), ['ProgramStudi_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendaftaranHasProgramstudis()
    {
        return $this->hasMany(PendaftaranHasProgramstudi::className(), ['ProgramStudi_id' => 'id']);
    }
}
