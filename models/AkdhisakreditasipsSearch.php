<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdhisakreditasips;

/**
 * AkdhisakreditasipsSearch represents the model behind the search form about `app\models\Akdhisakreditasips`.
 */
class AkdhisakreditasipsSearch extends Akdhisakreditasips
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MayorID', 'NilaiAkreditasi', 'LembagaPengakreditasiID', 'LingkupAkreditasiID'], 'integer'],
            [['PredikatAkreditasi', 'TMTAkreditasi', 'TSTAkreditasi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdhisakreditasips::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'MayorID' => $this->MayorID,
            'NilaiAkreditasi' => $this->NilaiAkreditasi,
            'TMTAkreditasi' => $this->TMTAkreditasi,
            'TSTAkreditasi' => $this->TSTAkreditasi,
            'LembagaPengakreditasiID' => $this->LembagaPengakreditasiID,
            'LingkupAkreditasiID' => $this->LingkupAkreditasiID,
        ]);

        $query->andFilterWhere(['like', 'PredikatAkreditasi', $this->PredikatAkreditasi]);

        return $dataProvider;
    }
}
