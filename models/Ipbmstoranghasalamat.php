<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ipbmst_orang_has_alamat".
 *
 * @property integer $ipbmst_orang_ID
 * @property integer $alamat_id
 *
 * @property Alamat $alamat
 * @property IpbmstOrang $ipbmstOrang
 */
class Ipbmstoranghasalamat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ipbmst_orang_has_alamat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ipbmst_orang_ID', 'alamat_id'], 'required'],
            [['ipbmst_orang_ID', 'alamat_id'], 'integer'],
            [['alamat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Alamat::className(), 'targetAttribute' => ['alamat_id' => 'id']],
            [['ipbmst_orang_ID'], 'exist', 'skipOnError' => true, 'targetClass' => IpbmstOrang::className(), 'targetAttribute' => ['ipbmst_orang_ID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ipbmst_orang_ID' => Yii::t('app', 'Ipbmst Orang  ID'),
            'alamat_id' => Yii::t('app', 'Alamat ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlamat()
    {
        return $this->hasOne(Alamat::className(), ['id' => 'alamat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIpbmstOrang()
    {
        return $this->hasOne(IpbmstOrang::className(), ['ID' => 'ipbmst_orang_ID']);
    }
}
