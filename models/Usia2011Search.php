<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usia2011;

/**
 * Usia2011Search represents the model behind the search form about `app\models\Usia2011`.
 */
class Usia2011Search extends Usia2011
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Mayor'], 'safe'],
            [['lessthan25', 'btwn25upto29', 'btwn30upto34', 'btwn35upto39', 'btwn40upto44', 'btwn45upto49', 'morethaneqs50', 'NotComplete', 'Total'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usia2011::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'lessthan25' => $this->lessthan25,
            'btwn25upto29' => $this->btwn25upto29,
            'btwn30upto34' => $this->btwn30upto34,
            'btwn35upto39' => $this->btwn35upto39,
            'btwn40upto44' => $this->btwn40upto44,
            'btwn45upto49' => $this->btwn45upto49,
            'morethaneqs50' => $this->morethaneqs50,
            'NotComplete' => $this->NotComplete,
            'Total' => $this->Total,
        ]);

        $query->andFilterWhere(['like', 'Mayor', $this->Mayor]);

        return $dataProvider;
    }
}
