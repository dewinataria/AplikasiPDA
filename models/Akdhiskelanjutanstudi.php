<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdhis_kelanjutanstudi".
 *
 * @property integer $ID
 * @property integer $MahasiswaID
 * @property string $NIM
 * @property integer $TahunSemesterID
 * @property integer $SemesterMahasiswa
 * @property integer $SKSBerjalan
 * @property integer $SKSKumulatif
 * @property double $NilaiTerbobotiBerjalan
 * @property double $NilaiTerbobotiKumulatif
 * @property double $IP
 * @property double $IPK
 * @property integer $StatusKelanjutanStudiID
 *
 * @property AkdmstMahasiswa $mahasiswa
 * @property AkdrefStatuskelanjutanstudi $statusKelanjutanStudi
 * @property AkdrefTahunsemester $tahunSemester
 */
class Akdhiskelanjutanstudi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdhis_kelanjutanstudi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'MahasiswaID', 'TahunSemesterID', 'SemesterMahasiswa', 'SKSBerjalan', 'SKSKumulatif', 'StatusKelanjutanStudiID'], 'integer'],
            [['NilaiTerbobotiBerjalan', 'NilaiTerbobotiKumulatif', 'IP', 'IPK'], 'number'],
            [['NIM'], 'string', 'max' => 45],
            [['MahasiswaID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdmstMahasiswa::className(), 'targetAttribute' => ['MahasiswaID' => 'ID']],
            [['StatusKelanjutanStudiID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefStatuskelanjutanstudi::className(), 'targetAttribute' => ['StatusKelanjutanStudiID' => 'ID']],
            [['TahunSemesterID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefTahunsemester::className(), 'targetAttribute' => ['TahunSemesterID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MahasiswaID' => Yii::t('app', 'Mahasiswa ID'),
            'NIM' => Yii::t('app', 'Nim'),
            'TahunSemesterID' => Yii::t('app', 'Tahun Semester ID'),
            'SemesterMahasiswa' => Yii::t('app', 'Semester Mahasiswa'),
            'SKSBerjalan' => Yii::t('app', 'Sksberjalan'),
            'SKSKumulatif' => Yii::t('app', 'Skskumulatif'),
            'NilaiTerbobotiBerjalan' => Yii::t('app', 'Nilai Terboboti Berjalan'),
            'NilaiTerbobotiKumulatif' => Yii::t('app', 'Nilai Terboboti Kumulatif'),
            'IP' => Yii::t('app', 'Ip'),
            'IPK' => Yii::t('app', 'Ipk'),
            'StatusKelanjutanStudiID' => Yii::t('app', 'Status Kelanjutan Studi ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(AkdmstMahasiswa::className(), ['ID' => 'MahasiswaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusKelanjutanStudi()
    {
        return $this->hasOne(AkdrefStatuskelanjutanstudi::className(), ['ID' => 'StatusKelanjutanStudiID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTahunSemester()
    {
        return $this->hasOne(AkdrefTahunsemester::className(), ['ID' => 'TahunSemesterID']);
    }
}
