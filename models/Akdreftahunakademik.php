<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_tahunakademik".
 *
 * @property integer $ID
 * @property integer $TahunAwal
 * @property string $TahunAkademik
 */
class Akdreftahunakademik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_tahunakademik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TahunAwal', 'TahunAkademik'], 'required'],
            [['ID', 'TahunAwal'], 'integer'],
            [['TahunAkademik'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TahunAwal' => Yii::t('app', 'Tahun Awal'),
            'TahunAkademik' => Yii::t('app', 'Tahun Akademik'),
        ];
    }
}
