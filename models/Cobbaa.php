<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cobbaa".
 *
 * @property integer $Year
 * @property string $Male
 * @property string $Female
 * @property string $Lainlain
 */
class Cobbaa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cobbaa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Year', 'Male', 'Female', 'Lainlain'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Year' => Yii::t('app', 'Year'),
            'Male' => Yii::t('app', 'Male'),
            'Female' => Yii::t('app', 'Female'),
            'Lainlain' => Yii::t('app', 'Lainlain'),
        ];
    }

    public static function primaryKey()
    {
        return array('Year');
    }
}
