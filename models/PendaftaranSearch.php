<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Pendaftaran;

/**
 * PendaftaranSearch represents the model behind the search form about `app\models\Pendaftaran`.
 */
class PendaftaranSearch extends Pendaftaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['noPendaftaran', 'PaketPendaftaranID', 'waktuBuat', 'waktuubah'], 'safe'],
            [['OrangID', 'RencanaPembiayaanID', 'ManajemenJalurMasukID', 'setujuSyarat', 'verifikasiPMB'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pendaftaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'OrangID' => $this->OrangID,
            'RencanaPembiayaanID' => $this->RencanaPembiayaanID,
            'ManajemenJalurMasukID' => $this->ManajemenJalurMasukID,
            'setujuSyarat' => $this->setujuSyarat,
            'verifikasiPMB' => $this->verifikasiPMB,
            'waktuBuat' => $this->waktuBuat,
            'waktuubah' => $this->waktuubah,
        ]);

        $query->andFilterWhere(['like', 'noPendaftaran', $this->noPendaftaran])
            ->andFilterWhere(['like', 'PaketPendaftaranID', $this->PaketPendaftaranID]);

        return $dataProvider;
    }
}
