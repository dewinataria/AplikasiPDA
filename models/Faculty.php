<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faculty".
 *
 * @property string $Faculty
 * @property integer $lessthan25
 * @property integer $btween25to29
 */
class Faculty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faculty';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Faculty', 'lessthan25', 'btween25to29'], 'required'],
            [['lessthan25', 'btween25to29'], 'integer'],
            [['Faculty'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Faculty' => Yii::t('app', 'Faculty'),
            'lessthan25' => Yii::t('app', 'Lessthan25'),
            'btween25to29' => Yii::t('app', 'Btween25to29'),
        ];
    }
}
