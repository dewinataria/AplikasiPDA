<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdtrxwisuda;

/**
 * AkdtrxwisudaSearch represents the model behind the search form about `app\models\Akdtrxwisuda`.
 */
class AkdtrxwisudaSearch extends Akdtrxwisuda
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MahasiswaID', 'NomorIjazah', 'GelarID', 'SyaratBuktiLaporan', 'SyaratBebasPustaka', 'SyaratLembarPengesahan', 'SyaratPasFoto', 'SyaratBuktiBayar', 'SyaratIjazahSMA', 'HadirWisuda', 'UlangWisuda', 'LamaStudi', 'TahapWisudaID'], 'integer'],
            [['NIM', 'TanggalLulus', 'TanggalIjazah', 'TanggalDaftar', 'SyaratBuktiLaporanPath', 'SyaratBebasPustakaPath', 'SyaratLembarPengesahanPath', 'SyaratPasFotoPath', 'SyaratBuktiBayarPath', 'SyaratIjazahSMAPath', 'JumlahTransferSKS'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdtrxwisuda::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'MahasiswaID' => $this->MahasiswaID,
            'NomorIjazah' => $this->NomorIjazah,
            'TanggalLulus' => $this->TanggalLulus,
            'TanggalIjazah' => $this->TanggalIjazah,
            'TanggalDaftar' => $this->TanggalDaftar,
            'GelarID' => $this->GelarID,
            'SyaratBuktiLaporan' => $this->SyaratBuktiLaporan,
            'SyaratBebasPustaka' => $this->SyaratBebasPustaka,
            'SyaratLembarPengesahan' => $this->SyaratLembarPengesahan,
            'SyaratPasFoto' => $this->SyaratPasFoto,
            'SyaratBuktiBayar' => $this->SyaratBuktiBayar,
            'SyaratIjazahSMA' => $this->SyaratIjazahSMA,
            'HadirWisuda' => $this->HadirWisuda,
            'UlangWisuda' => $this->UlangWisuda,
            'LamaStudi' => $this->LamaStudi,
            'TahapWisudaID' => $this->TahapWisudaID,
        ]);

        $query->andFilterWhere(['like', 'NIM', $this->NIM])
            ->andFilterWhere(['like', 'SyaratBuktiLaporanPath', $this->SyaratBuktiLaporanPath])
            ->andFilterWhere(['like', 'SyaratBebasPustakaPath', $this->SyaratBebasPustakaPath])
            ->andFilterWhere(['like', 'SyaratLembarPengesahanPath', $this->SyaratLembarPengesahanPath])
            ->andFilterWhere(['like', 'SyaratPasFotoPath', $this->SyaratPasFotoPath])
            ->andFilterWhere(['like', 'SyaratBuktiBayarPath', $this->SyaratBuktiBayarPath])
            ->andFilterWhere(['like', 'SyaratIjazahSMAPath', $this->SyaratIjazahSMAPath])
            ->andFilterWhere(['like', 'JumlahTransferSKS', $this->JumlahTransferSKS]);

        return $dataProvider;
    }
}
