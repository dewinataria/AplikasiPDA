<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rencanapembiayaan".
 *
 * @property integer $ID
 * @property integer $JenisPembiayaanID
 * @property string $deskripsi
 *
 * @property Pendaftaran[] $pendaftarans
 * @property Jenispembiayaan $jenisPembiayaan
 */
class Rencanapembiayaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rencanapembiayaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'JenisPembiayaanID'], 'integer'],
            [['deskripsi'], 'string'],
            [['JenisPembiayaanID'], 'exist', 'skipOnError' => true, 'targetClass' => Jenispembiayaan::className(), 'targetAttribute' => ['JenisPembiayaanID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'JenisPembiayaanID' => Yii::t('app', 'Jenis Pembiayaan ID'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendaftarans()
    {
        return $this->hasMany(Pendaftaran::className(), ['RencanaPembiayaanID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPembiayaan()
    {
        return $this->hasOne(Jenispembiayaan::className(), ['ID' => 'JenisPembiayaanID']);
    }
}
