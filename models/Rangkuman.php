<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rangkuman".
 *
 * @property string $Fakultas
 * @property string $Departemen
 * @property string $KodeMayor
 * @property string $NIM
 * @property integer $TahunMasuk
 * @property string $JenisKelamin
 * @property string $StatusAkademik
 * @property double $IPK
 * @property string $Usia
 */
class Rangkuman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rangkuman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fakultas', 'Departemen', 'NIM', 'StatusAkademik'], 'required'],
            [['TahunMasuk', 'Usia'], 'integer'],
            [['IPK'], 'number'],
            [['Fakultas', 'Departemen'], 'string', 'max' => 5],
            [['KodeMayor'], 'string', 'max' => 10],
            [['NIM'], 'string', 'max' => 11],
            [['JenisKelamin'], 'string', 'max' => 45],
            [['StatusAkademik'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Fakultas' => Yii::t('app', 'Fakultas'),
            'Departemen' => Yii::t('app', 'Departemen'),
            'KodeMayor' => Yii::t('app', 'Kode Mayor'),
            'NIM' => Yii::t('app', 'Nim'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'JenisKelamin' => Yii::t('app', 'Jenis Kelamin'),
            'StatusAkademik' => Yii::t('app', 'Status Akademik'),
            'IPK' => Yii::t('app', 'Ipk'),
            'Usia' => Yii::t('app', 'Usia'),
        ];
    }

    public static function primaryKey()
    {
        return array('NIM');
    }
}
