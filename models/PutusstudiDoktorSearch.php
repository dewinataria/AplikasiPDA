<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PutusstudiDoktor;

/**
 * PutusstudiDoktorSearch represents the model behind the search form about `app\models\PutusstudiDoktor`.
 */
class PutusstudiDoktorSearch extends PutusstudiDoktor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['StatusAkademik', 'JenisKelaminID', 'TahunMasuk'], 'integer'],
            [['fakultas', 'NIM'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PutusstudiDoktor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'StatusAkademik' => $this->StatusAkademik,
            'JenisKelaminID' => $this->JenisKelaminID,
            'TahunMasuk' => $this->TahunMasuk,
        ]);

        $query->andFilterWhere(['like', 'fakultas', $this->fakultas])
            ->andFilterWhere(['like', 'NIM', $this->NIM]);

        return $dataProvider;
    }
}
