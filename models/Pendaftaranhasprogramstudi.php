<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pendaftaran_has_programstudi".
 *
 * @property integer $id
 * @property string $Pendaftaran_noPendaftaran
 * @property integer $ProgramStudi_id
 * @property integer $urutan
 *
 * @property Pendaftaran $pendaftaranNoPendaftaran
 * @property Programstudi $programStudi
 */
class Pendaftaranhasprogramstudi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pendaftaran_has_programstudi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'Pendaftaran_noPendaftaran', 'ProgramStudi_id'], 'required'],
            [['id', 'ProgramStudi_id', 'urutan'], 'integer'],
            [['Pendaftaran_noPendaftaran'], 'string', 'max' => 100],
            [['Pendaftaran_noPendaftaran'], 'exist', 'skipOnError' => true, 'targetClass' => Pendaftaran::className(), 'targetAttribute' => ['Pendaftaran_noPendaftaran' => 'noPendaftaran']],
            [['ProgramStudi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Programstudi::className(), 'targetAttribute' => ['ProgramStudi_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'Pendaftaran_noPendaftaran' => Yii::t('app', 'Pendaftaran No Pendaftaran'),
            'ProgramStudi_id' => Yii::t('app', 'Program Studi ID'),
            'urutan' => Yii::t('app', 'Urutan'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPendaftaranNoPendaftaran()
    {
        return $this->hasOne(Pendaftaran::className(), ['noPendaftaran' => 'Pendaftaran_noPendaftaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgramStudi()
    {
        return $this->hasOne(Programstudi::className(), ['id' => 'ProgramStudi_id']);
    }
}
