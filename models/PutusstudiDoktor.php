<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "putusstudi_doktor".
 *
 * @property integer $StatusAkademik
 * @property integer $JenisKelaminID
 * @property integer $TahunMasuk
 * @property string $fakultas
 * @property string $NIM
 */
class PutusstudiDoktor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'putusstudi_doktor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['StatusAkademik', 'JenisKelaminID', 'TahunMasuk'], 'integer'],
            [['JenisKelaminID', 'fakultas', 'NIM'], 'required'],
            [['fakultas'], 'string', 'max' => 5],
            [['NIM'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'StatusAkademik' => Yii::t('app', 'Status Akademik'),
            //'JenisKelaminID' => Yii::t('app', 'Jenis Kelamin ID'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'fakultas' => Yii::t('app', 'Fakultas'),
            'NIM' => Yii::t('app', 'NIM'),
        ];
    }
     public static function primaryKey()
    {
        return array('NIM');
    }
}
