<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ipk;

/**
 * IpkSearch represents the model behind the search form about `app\models\Ipk`.
 */
class IpkSearch extends Ipk
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NIM', 'Fakultas', 'Departemen', 'KodeMayor', 'StatusAkademik', 'Semester'], 'safe'],
            [['IPK'], 'number'],
            [['TahunMasuk', 'TahunAkademikID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ipk::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'IPK' => $this->IPK,
            'TahunMasuk' => $this->TahunMasuk,
            'TahunAkademikID' => $this->TahunAkademikID,
        ]);

        $query->andFilterWhere(['like', 'NIM', $this->NIM])
            ->andFilterWhere(['like', 'Fakultas', $this->Fakultas])
            ->andFilterWhere(['like', 'Departemen', $this->Departemen])
            ->andFilterWhere(['like', 'KodeMayor', $this->KodeMayor])
            ->andFilterWhere(['like', 'StatusAkademik', $this->StatusAkademik])
            ->andFilterWhere(['like', 'Semester', $this->Semester]);

        return $dataProvider;
    }
}
