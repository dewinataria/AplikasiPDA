<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_tahunsemester".
 *
 * @property integer $ID
 * @property integer $TahunAkademikID
 * @property integer $SemesterID
 *
 * @property AkdhisKelanjutanstudi[] $akdhisKelanjutanstudis
 * @property AkdhisStatusmahasiswa[] $akdhisStatusmahasiswas
 */
class Akdreftahunsemester extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_tahunsemester';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID', 'TahunAkademikID', 'SemesterID'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'TahunAkademikID' => Yii::t('app', 'Tahun Akademik ID'),
            'SemesterID' => Yii::t('app', 'Semester ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisKelanjutanstudis()
    {
        return $this->hasMany(AkdhisKelanjutanstudi::className(), ['TahunSemesterID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisStatusmahasiswas()
    {
        return $this->hasMany(AkdhisStatusmahasiswa::className(), ['TahunSemesterID' => 'ID']);
    }
}
