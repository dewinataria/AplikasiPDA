<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdmstmayor;

/**
 * AkdmstmayorSearch represents the model behind the search form about `app\models\Akdmstmayor`.
 */
class AkdmstmayorSearch extends Akdmstmayor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'StrukturOrganisasiID', 'DepartemenID', 'StrataID', 'Kaprodi'], 'integer'],
            [['Kode', 'KodePDPT', 'Nama', 'NamaEn', 'Inisial', 'TanggalDibentuk', 'NomorSKDibentuk', 'MayorSarjanaBKey', 'MayorPascasarjanaBKey', 'TanggalDitutup', 'NomorSKDitutup'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdmstmayor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'StrukturOrganisasiID' => $this->StrukturOrganisasiID,
            'DepartemenID' => $this->DepartemenID,
            'StrataID' => $this->StrataID,
            'Kaprodi' => $this->Kaprodi,
            'TanggalDibentuk' => $this->TanggalDibentuk,
            'TanggalDitutup' => $this->TanggalDitutup,
        ]);

        $query->andFilterWhere(['like', 'Kode', $this->Kode])
            ->andFilterWhere(['like', 'KodePDPT', $this->KodePDPT])
            ->andFilterWhere(['like', 'Nama', $this->Nama])
            ->andFilterWhere(['like', 'NamaEn', $this->NamaEn])
            ->andFilterWhere(['like', 'Inisial', $this->Inisial])
            ->andFilterWhere(['like', 'NomorSKDibentuk', $this->NomorSKDibentuk])
            ->andFilterWhere(['like', 'MayorSarjanaBKey', $this->MayorSarjanaBKey])
            ->andFilterWhere(['like', 'MayorPascasarjanaBKey', $this->MayorPascasarjanaBKey])
            ->andFilterWhere(['like', 'NomorSKDitutup', $this->NomorSKDitutup]);

        return $dataProvider;
    }
}
