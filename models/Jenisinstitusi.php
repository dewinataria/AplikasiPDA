<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenisinstitusi".
 *
 * @property integer $ID
 * @property string $jenis
 *
 * @property Institusi[] $institusis
 */
class Jenisinstitusi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenisinstitusi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID'], 'required'],
            [['ID'], 'integer'],
            [['jenis'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'jenis' => Yii::t('app', 'Jenis'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitusis()
    {
        return $this->hasMany(Institusi::className(), ['jenisInstitusi_id' => 'ID']);
    }
}
