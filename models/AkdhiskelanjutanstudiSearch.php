<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdhiskelanjutanstudi;

/**
 * AkdhiskelanjutanstudiSearch represents the model behind the search form about `app\models\Akdhiskelanjutanstudi`.
 */
class AkdhiskelanjutanstudiSearch extends Akdhiskelanjutanstudi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MahasiswaID', 'TahunSemesterID', 'SemesterMahasiswa', 'SKSBerjalan', 'SKSKumulatif', 'StatusKelanjutanStudiID'], 'integer'],
            [['NIM'], 'safe'],
            [['NilaiTerbobotiBerjalan', 'NilaiTerbobotiKumulatif', 'IP', 'IPK'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdhiskelanjutanstudi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'MahasiswaID' => $this->MahasiswaID,
            'TahunSemesterID' => $this->TahunSemesterID,
            'SemesterMahasiswa' => $this->SemesterMahasiswa,
            'SKSBerjalan' => $this->SKSBerjalan,
            'SKSKumulatif' => $this->SKSKumulatif,
            'NilaiTerbobotiBerjalan' => $this->NilaiTerbobotiBerjalan,
            'NilaiTerbobotiKumulatif' => $this->NilaiTerbobotiKumulatif,
            'IP' => $this->IP,
            'IPK' => $this->IPK,
            'StatusKelanjutanStudiID' => $this->StatusKelanjutanStudiID,
        ]);

        $query->andFilterWhere(['like', 'NIM', $this->NIM]);

        return $dataProvider;
    }
}
