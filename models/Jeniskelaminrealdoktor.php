<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jeniskelaminrealdoktor".
 *
 * @property string $Fakultas
 * @property string $Departemen
 * @property string $Mayor
 * @property string $NIM
 * @property string $JenisKelamin
 * @property string $Usia
 * @property integer $TahunMasuk
 */
class Jeniskelaminrealdoktor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jeniskelaminrealdoktor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['NIM'], 'required'],
            [['Usia', 'TahunMasuk'], 'integer'],
            [['Fakultas', 'Departemen'], 'string', 'max' => 5],
            [['Mayor'], 'string', 'max' => 255],
            [['NIM'], 'string', 'max' => 11],
            [['JenisKelamin'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'Fakultas' => Yii::t('app', 'Fakultas'),
            'Departemen' => Yii::t('app', 'Departemen'),
            'Mayor' => Yii::t('app', 'Mayor'),
            'NIM' => Yii::t('app', 'Nim'),
            'JenisKelamin' => Yii::t('app', 'Jenis Kelamin'),
            'Usia' => Yii::t('app', 'Usia'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
        ];
    }

    public static function primaryKey()
    {
        return array('NIM');
    }
}
