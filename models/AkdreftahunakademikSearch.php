<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdreftahunakademik;

/**
 * AkdreftahunakademikSearch represents the model behind the search form about `app\models\Akdreftahunakademik`.
 */
class AkdreftahunakademikSearch extends Akdreftahunakademik
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'TahunAwal'], 'integer'],
            [['TahunAkademik'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdreftahunakademik::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'TahunAwal' => $this->TahunAwal,
        ]);

        $query->andFilterWhere(['like', 'TahunAkademik', $this->TahunAkademik]);

        return $dataProvider;
    }
}
