<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_statusakademik".
 *
 * @property integer $ID
 * @property string $Nama
 * @property integer $isOutIPB
 *
 * @property AkdhisStatusmahasiswa[] $akdhisStatusmahasiswas
 */
class Akdrefstatusakademik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_statusakademik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Nama'], 'required'],
            [['ID', 'isOutIPB'], 'integer'],
            [['Nama'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nama' => Yii::t('app', 'Nama'),
            'isOutIPB' => Yii::t('app', 'Is Out Ipb'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdhisStatusmahasiswas()
    {
        return $this->hasMany(AkdhisStatusmahasiswa::className(), ['StatusAkademikID' => 'ID']);
    }
}
