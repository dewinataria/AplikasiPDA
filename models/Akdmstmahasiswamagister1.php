<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdmst_mahasiswamagister1".
 *
 * @property integer $ID
 * @property integer $MahasiswaID
 * @property string $NIM
 * @property integer $MayorID
 * @property integer $JalurMasukID
 * @property string $TanggalMasuk
 * @property integer $TahunMasuk
 * @property integer $BatasStudi
 * @property integer $AwalSemester
 * @property string $GelarDepan
 * @property string $GelarBelakang
 * @property integer $StatusVerifikasi
 * @property integer $StatusAkademikID
 * @property integer $PindahMayor
 *
 * @property AkdmstMahasiswa $mahasiswa
 * @property AkdmstMayor $mayor
 * @property AkdrefJalurmasuk $jalurMasuk
 */
class Akdmstmahasiswamagister1 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdmst_mahasiswamagister1';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MahasiswaID', 'NIM', 'MayorID', 'JalurMasukID'], 'required'],
            [['ID', 'MahasiswaID', 'MayorID', 'JalurMasukID', 'TahunMasuk', 'BatasStudi', 'AwalSemester', 'StatusVerifikasi', 'StatusAkademikID', 'PindahMayor'], 'integer'],
            [['TanggalMasuk'], 'safe'],
            [['NIM'], 'string', 'max' => 11],
            [['GelarDepan'], 'string', 'max' => 10],
            [['GelarBelakang'], 'string', 'max' => 20],
            [['MahasiswaID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdmstMahasiswa::className(), 'targetAttribute' => ['MahasiswaID' => 'ID']],
            [['MayorID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdmstMayor::className(), 'targetAttribute' => ['MayorID' => 'ID']],
            [['JalurMasukID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefJalurmasuk::className(), 'targetAttribute' => ['JalurMasukID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'MahasiswaID' => Yii::t('app', 'Mahasiswa ID'),
            'NIM' => Yii::t('app', 'Nim'),
            'MayorID' => Yii::t('app', 'Mayor ID'),
            'JalurMasukID' => Yii::t('app', 'Jalur Masuk ID'),
            'TanggalMasuk' => Yii::t('app', 'Tanggal Masuk'),
            'TahunMasuk' => Yii::t('app', 'Tahun Masuk'),
            'BatasStudi' => Yii::t('app', 'Batas Studi'),
            'AwalSemester' => Yii::t('app', 'Awal Semester'),
            'GelarDepan' => Yii::t('app', 'Gelar Depan'),
            'GelarBelakang' => Yii::t('app', 'Gelar Belakang'),
            'StatusVerifikasi' => Yii::t('app', 'Status Verifikasi'),
            'StatusAkademikID' => Yii::t('app', 'Status Akademik ID'),
            'PindahMayor' => Yii::t('app', 'Pindah Mayor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(AkdmstMahasiswa::className(), ['ID' => 'MahasiswaID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMayor()
    {
        return $this->hasOne(AkdmstMayor::className(), ['ID' => 'MayorID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJalurMasuk()
    {
        return $this->hasOne(AkdrefJalurmasuk::className(), ['ID' => 'JalurMasukID']);
    }
}
