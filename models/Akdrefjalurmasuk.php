<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "akdref_jalurmasuk".
 *
 * @property integer $ID
 * @property string $Nama
 * @property string $Keterangan
 * @property integer $StrataID
 * @property string $TahunBerlaku
 *
 * @property AkdmstMahasiswadoktor[] $akdmstMahasiswadoktors
 * @property AkdmstMahasiswamagister1[] $akdmstMahasiswamagister1s
 * @property AkdrefStrata $strata
 */
class Akdrefjalurmasuk extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'akdref_jalurmasuk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'Nama'], 'required'],
            [['ID', 'StrataID'], 'integer'],
            [['Nama'], 'string', 'max' => 50],
            [['Keterangan'], 'string', 'max' => 100],
            [['TahunBerlaku'], 'string', 'max' => 4],
            [['StrataID'], 'exist', 'skipOnError' => true, 'targetClass' => AkdrefStrata::className(), 'targetAttribute' => ['StrataID' => 'ID']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => Yii::t('app', 'ID'),
            'Nama' => Yii::t('app', 'Nama'),
            'Keterangan' => Yii::t('app', 'Keterangan'),
            'StrataID' => Yii::t('app', 'Strata ID'),
            'TahunBerlaku' => Yii::t('app', 'Tahun Berlaku'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswadoktors()
    {
        return $this->hasMany(AkdmstMahasiswadoktor::className(), ['JalurMasukID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAkdmstMahasiswamagister1s()
    {
        return $this->hasMany(AkdmstMahasiswamagister1::className(), ['JalurMasukID' => 'ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStrata()
    {
        return $this->hasOne(AkdrefStrata::className(), ['ID' => 'StrataID']);
    }
}
