<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Akdmstmahasiswadoktor;

/**
 * AkdmstmahasiswadoktorSearch represents the model behind the search form about `app\models\Akdmstmahasiswadoktor`.
 */
class AkdmstmahasiswadoktorSearch extends Akdmstmahasiswadoktor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'MahasiswaID', 'MayorID', 'JalurMasukID', 'TahunMasuk', 'BatasStudi', 'AwalSemester', 'StatusVerifikasi', 'StatusAkademikID', 'PindahMayor'], 'integer'],
            [['NIM', 'TanggalMasuk', 'GelarDepan', 'GelarBelakang'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Akdmstmahasiswadoktor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'MahasiswaID' => $this->MahasiswaID,
            'MayorID' => $this->MayorID,
            'JalurMasukID' => $this->JalurMasukID,
            'TanggalMasuk' => $this->TanggalMasuk,
            'TahunMasuk' => $this->TahunMasuk,
            'BatasStudi' => $this->BatasStudi,
            'AwalSemester' => $this->AwalSemester,
            'StatusVerifikasi' => $this->StatusVerifikasi,
            'StatusAkademikID' => $this->StatusAkademikID,
            'PindahMayor' => $this->PindahMayor,
        ]);

        $query->andFilterWhere(['like', 'NIM', $this->NIM])
            ->andFilterWhere(['like', 'GelarDepan', $this->GelarDepan])
            ->andFilterWhere(['like', 'GelarBelakang', $this->GelarBelakang]);

        return $dataProvider;
    }
}
