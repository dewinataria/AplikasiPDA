<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Institusi;

/**
 * InstitusiSearch represents the model behind the search form about `app\models\Institusi`.
 */
class InstitusiSearch extends Institusi
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'status', 'negara_id', 'jenisInstitusi_id'], 'integer'],
            [['Nama', 'KodePT', 'tlp', 'fax', 'email', 'website', 'tanggalBerdiri', 'nomorSKPT', 'tanggalSKPT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Institusi::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'tanggalBerdiri' => $this->tanggalBerdiri,
            'tanggalSKPT' => $this->tanggalSKPT,
            'status' => $this->status,
            'negara_id' => $this->negara_id,
            'jenisInstitusi_id' => $this->jenisInstitusi_id,
        ]);

        $query->andFilterWhere(['like', 'Nama', $this->Nama])
            ->andFilterWhere(['like', 'KodePT', $this->KodePT])
            ->andFilterWhere(['like', 'tlp', $this->tlp])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'website', $this->website])
            ->andFilterWhere(['like', 'nomorSKPT', $this->nomorSKPT]);

        return $dataProvider;
    }
}
