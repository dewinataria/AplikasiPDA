<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Hapus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="hapus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TahunMasuk')->textInput() ?>

    <?= $form->field($model, 'Perempuan')->textInput() ?>

    <?= $form->field($model, 'Lakilaki')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
