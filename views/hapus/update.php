<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Hapus */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Hapus',
]) . $model->TahunMasuk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Hapuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TahunMasuk, 'url' => ['view', 'id' => $model->TahunMasuk]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="hapus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
