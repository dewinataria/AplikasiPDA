<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IpbmstdepartemenSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipbmstdepartemen-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'FakultasID') ?>

    <?= $form->field($model, 'Kode') ?>

    <?= $form->field($model, 'Kadep') ?>

    <?= $form->field($model, 'TanggalDibentuk') ?>

    <?php // echo $form->field($model, 'DepartemenBKey') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
