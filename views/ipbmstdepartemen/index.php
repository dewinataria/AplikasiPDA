<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IpbmstdepartemenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ipbmstdepartemens');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipbmstdepartemen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ipbmstdepartemen'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'FakultasID',
            'Kode',
            'Kadep',
            'TanggalDibentuk',
            // 'DepartemenBKey',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
