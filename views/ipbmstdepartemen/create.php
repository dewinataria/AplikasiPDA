<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ipbmstdepartemen */

$this->title = Yii::t('app', 'Create Ipbmstdepartemen');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipbmstdepartemens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipbmstdepartemen-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
