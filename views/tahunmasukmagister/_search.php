<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TahunmasukmagisterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tahunmasukmagister-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'TahunMasuk') ?>

    <?= $form->field($model, 'JenisKelaminID') ?>

    <?= $form->field($model, 'Usia') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
