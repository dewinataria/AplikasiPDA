<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tahunmasukmagister */

$this->title = Yii::t('app', 'Create Tahunmasukmagister');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tahunmasukmagisters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tahunmasukmagister-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
