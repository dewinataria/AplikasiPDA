<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdrefsemester */

$this->title = Yii::t('app', 'Create Akdrefsemester');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdrefsemesters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdrefsemester-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
