<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Akdrefsemester */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Akdrefsemester',
]) . $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdrefsemesters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="akdrefsemester-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
