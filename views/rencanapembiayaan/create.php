<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rencanapembiayaan */

$this->title = Yii::t('app', 'Create Rencanapembiayaan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rencanapembiayaans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rencanapembiayaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
