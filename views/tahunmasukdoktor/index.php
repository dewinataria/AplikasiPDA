<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TahunmasukdoktorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'S3 - Tahun Masuk dan Usia');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tahunmasukdoktor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Jenis Kelamin:</h3> <br>
    1- Pria <br>
    2 - Wanita <br><br>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Tahunmasukdoktor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NIM',
            'TahunMasuk',
            'JenisKelaminID',
            'Usia',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
