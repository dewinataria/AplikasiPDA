<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ipk */

$this->title = Yii::t('app', 'Create Ipk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
