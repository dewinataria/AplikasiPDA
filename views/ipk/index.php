<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IpkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ipks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ipk'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NIM',
            'IPK',
            'Fakultas',
            'Departemen',
            'KodeMayor',
            // 'TahunMasuk',
            // 'StatusAkademik',
            // 'TahunAkademikID',
            // 'Semester',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
