<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pendaftaranhasprogramstudi */

$this->title = Yii::t('app', 'Create Pendaftaranhasprogramstudi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pendaftaranhasprogramstudis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendaftaranhasprogramstudi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
