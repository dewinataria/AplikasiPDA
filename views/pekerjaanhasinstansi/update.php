<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pekerjaanhasinstansi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pekerjaanhasinstansi',
]) . $model->pekerjaan_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pekerjaanhasinstansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pekerjaan_id, 'url' => ['view', 'pekerjaan_id' => $model->pekerjaan_id, 'instansi_id' => $model->instansi_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pekerjaanhasinstansi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
