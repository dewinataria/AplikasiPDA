<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pekerjaanhasinstansi */

$this->title = Yii::t('app', 'Create Pekerjaanhasinstansi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pekerjaanhasinstansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pekerjaanhasinstansi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
