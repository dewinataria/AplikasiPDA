<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jeniskelaminreal */

$this->title = Yii::t('app', 'Create Jeniskelaminreal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jeniskelaminreals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jeniskelaminreal-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
