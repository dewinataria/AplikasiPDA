<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jeniskelaminreal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jeniskelaminreal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Fakultas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Departemen')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Mayor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'JenisKelamin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Usia')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TahunMasuk')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
