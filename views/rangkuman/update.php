<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rangkuman */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Rangkuman',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rangkumen'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rangkuman-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
