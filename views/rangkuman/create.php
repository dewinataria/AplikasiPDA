<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rangkuman */

$this->title = Yii::t('app', 'Create Rangkuman');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rangkumen'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rangkuman-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
