<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ManajemenjalurmasukSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Manajemenjalurmasuks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manajemenjalurmasuk-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Manajemenjalurmasuk'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'ProgramStudi_id',
            'jalurmasuk_id',
            'program_id',
            'aktif',
            // 'strata',
            // 'biayapendidikan_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
