<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Manajemenjalurmasuk */

$this->title = Yii::t('app', 'Create Manajemenjalurmasuk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manajemenjalurmasuks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manajemenjalurmasuk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
