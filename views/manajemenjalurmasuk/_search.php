<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ManajemenjalurmasukSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manajemenjalurmasuk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ProgramStudi_id') ?>

    <?= $form->field($model, 'jalurmasuk_id') ?>

    <?= $form->field($model, 'program_id') ?>

    <?= $form->field($model, 'aktif') ?>

    <?php // echo $form->field($model, 'strata') ?>

    <?php // echo $form->field($model, 'biayapendidikan_id') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
