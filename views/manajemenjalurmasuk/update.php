<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Manajemenjalurmasuk */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Manajemenjalurmasuk',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manajemenjalurmasuks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="manajemenjalurmasuk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
