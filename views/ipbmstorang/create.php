<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ipbmstorang */

$this->title = Yii::t('app', 'Create Ipbmstorang');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipbmstorangs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipbmstorang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
