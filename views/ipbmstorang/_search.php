<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IpbmstorangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipbmstorang-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Nama') ?>

    <?= $form->field($model, 'TempatLahir') ?>

    <?= $form->field($model, 'TempatLahirID') ?>

    <?= $form->field($model, 'TanggalLahir') ?>

    <?php // echo $form->field($model, 'JenisKelaminID') ?>

    <?php // echo $form->field($model, 'NIMS1Key') ?>

    <?php // echo $form->field($model, 'NIMS2Key') ?>

    <?php // echo $form->field($model, 'NIMS3KEY') ?>

    <?php // echo $form->field($model, 'Pgwawikey') ?>

    <?php // echo $form->field($model, 'PasanganKey') ?>

    <?php // echo $form->field($model, 'Anakvkey') ?>

    <?php // echo $form->field($model, 'S2Lama') ?>

    <?php // echo $form->field($model, 'S3Lama') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
