<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IpbmstorangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ipbmstorangs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipbmstorang-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ipbmstorang'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'Nama',
            'TempatLahir',
            'TempatLahirID',
            'TanggalLahir',
            // 'JenisKelaminID',
            // 'NIMS1Key',
            // 'NIMS2Key',
            // 'NIMS3KEY',
            // 'Pgwawikey',
            // 'PasanganKey',
            // 'Anakvkey',
            // 'S2Lama',
            // 'S3Lama',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
