<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ipbmstorang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipbmstorang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'Nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TempatLahir')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TempatLahirID')->textInput() ?>

    <?= $form->field($model, 'TanggalLahir')->textInput() ?>

    <?= $form->field($model, 'JenisKelaminID')->textInput() ?>

    <?= $form->field($model, 'NIMS1Key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIMS2Key')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIMS3KEY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Pgwawikey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'PasanganKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Anakvkey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'S2Lama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'S3Lama')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
