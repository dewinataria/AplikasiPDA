<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PutusstudiDoktor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Putusstudi Doktor',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Putusstudi Doktors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="putusstudi-doktor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
