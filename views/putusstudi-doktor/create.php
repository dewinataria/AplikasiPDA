<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PutusstudiDoktor */

$this->title = Yii::t('app', 'Create Putusstudi Doktor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Putusstudi Doktors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="putusstudi-doktor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
