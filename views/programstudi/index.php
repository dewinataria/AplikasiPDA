<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramstudiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Programstudis');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="programstudi-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Programstudi'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'kode',
            'nama',
            'nama_en',
            'aktif',
            // 'strata',
            // 'inisial',
            // 'kode_nasional',
            // 'sk_pendirian:ntext',
            // 'mandat:ntext',
            // 'visi_misi:ntext',
            // 'departemen_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
