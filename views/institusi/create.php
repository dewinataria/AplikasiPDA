<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Institusi */

$this->title = Yii::t('app', 'Create Institusi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Institusis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institusi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
