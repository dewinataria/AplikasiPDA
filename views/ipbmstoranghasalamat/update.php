<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ipbmstoranghasalamat */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ipbmstoranghasalamat',
]) . $model->ipbmst_orang_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipbmstoranghasalamats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ipbmst_orang_ID, 'url' => ['view', 'ipbmst_orang_ID' => $model->ipbmst_orang_ID, 'alamat_id' => $model->alamat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ipbmstoranghasalamat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
