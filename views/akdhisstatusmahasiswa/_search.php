<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkdhisstatusmahasiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdhisstatusmahasiswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'MahasiswaID') ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'TahunSemesterID') ?>

    <?= $form->field($model, 'SemesterMahasiswa') ?>

    <?php // echo $form->field($model, 'StatusAkademikID') ?>

    <?php // echo $form->field($model, 'TanggalStatus') ?>

    <?php // echo $form->field($model, 'NoSurat') ?>

    <?php // echo $form->field($model, 'Keterangan') ?>

    <?php // echo $form->field($model, 'StatusMahasiswaBKey') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
