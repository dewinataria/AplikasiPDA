<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akdhisstatusmahasiswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdhisstatusmahasiswa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'MahasiswaID')->textInput() ?>

    <?= $form->field($model, 'NIM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TahunSemesterID')->textInput() ?>

    <?= $form->field($model, 'SemesterMahasiswa')->textInput() ?>

    <?= $form->field($model, 'StatusAkademikID')->textInput() ?>

    <?= $form->field($model, 'TanggalStatus')->textInput() ?>

    <?= $form->field($model, 'NoSurat')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Keterangan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'StatusMahasiswaBKey')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
