<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akdhiskelanjutanstudi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdhiskelanjutanstudi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'MahasiswaID')->textInput() ?>

    <?= $form->field($model, 'NIM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TahunSemesterID')->textInput() ?>

    <?= $form->field($model, 'SemesterMahasiswa')->textInput() ?>

    <?= $form->field($model, 'SKSBerjalan')->textInput() ?>

    <?= $form->field($model, 'SKSKumulatif')->textInput() ?>

    <?= $form->field($model, 'NilaiTerbobotiBerjalan')->textInput() ?>

    <?= $form->field($model, 'NilaiTerbobotiKumulatif')->textInput() ?>

    <?= $form->field($model, 'IP')->textInput() ?>

    <?= $form->field($model, 'IPK')->textInput() ?>

    <?= $form->field($model, 'StatusKelanjutanStudiID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
