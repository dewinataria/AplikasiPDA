<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdhiskelanjutanstudi */

$this->title = Yii::t('app', 'Create Akdhiskelanjutanstudi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdhiskelanjutanstudis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdhiskelanjutanstudi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
