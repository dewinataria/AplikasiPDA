<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkdhiskelanjutanstudiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdhiskelanjutanstudi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'MahasiswaID') ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'TahunSemesterID') ?>

    <?= $form->field($model, 'SemesterMahasiswa') ?>

    <?php // echo $form->field($model, 'SKSBerjalan') ?>

    <?php // echo $form->field($model, 'SKSKumulatif') ?>

    <?php // echo $form->field($model, 'NilaiTerbobotiBerjalan') ?>

    <?php // echo $form->field($model, 'NilaiTerbobotiKumulatif') ?>

    <?php // echo $form->field($model, 'IP') ?>

    <?php // echo $form->field($model, 'IPK') ?>

    <?php // echo $form->field($model, 'StatusKelanjutanStudiID') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
