<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Institusihasalamat */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Institusihasalamat',
]) . $model->institusi_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Institusihasalamats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->institusi_ID, 'url' => ['view', 'institusi_ID' => $model->institusi_ID, 'alamat_id' => $model->alamat_id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="institusihasalamat-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
