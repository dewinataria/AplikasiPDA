<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Institusihasalamat */

$this->title = Yii::t('app', 'Create Institusihasalamat');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Institusihasalamats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institusihasalamat-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
