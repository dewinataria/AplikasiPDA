<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Institusihasalamat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="institusihasalamat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'institusi_ID')->textInput() ?>

    <?= $form->field($model, 'alamat_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
