<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Institusihasalamat */

$this->title = $model->institusi_ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Institusihasalamats'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="institusihasalamat-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'institusi_ID' => $model->institusi_ID, 'alamat_id' => $model->alamat_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'institusi_ID' => $model->institusi_ID, 'alamat_id' => $model->alamat_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'institusi_ID',
            'alamat_id',
        ],
    ]) ?>

</div>
