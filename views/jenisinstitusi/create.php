<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jenisinstitusi */

$this->title = Yii::t('app', 'Create Jenisinstitusi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenisinstitusis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenisinstitusi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
