<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="studi-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Fakultas') ?>

    <?= $form->field($model, 'Departemen') ?>

    <?= $form->field($model, 'Mayor') ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'LamaStudi') ?>

    <?php // echo $form->field($model, 'TahunMasuk') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
