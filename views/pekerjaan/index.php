<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PekerjaanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pekerjaans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pekerjaan-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pekerjaan'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'tanggalMasuk',
            'tanggalKeluar',
            'jabatan',
            'noIdentitas',
            // 'jenisInstansi_id',
            // 'orang_id',
            // 'pekerjaanLainnya',
            // 'waktuBuat',
            // 'waktuUbah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
