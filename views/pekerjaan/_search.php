<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PekerjaanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pekerjaan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'tanggalMasuk') ?>

    <?= $form->field($model, 'tanggalKeluar') ?>

    <?= $form->field($model, 'jabatan') ?>

    <?= $form->field($model, 'noIdentitas') ?>

    <?php // echo $form->field($model, 'jenisInstansi_id') ?>

    <?php // echo $form->field($model, 'orang_id') ?>

    <?php // echo $form->field($model, 'pekerjaanLainnya') ?>

    <?php // echo $form->field($model, 'waktuBuat') ?>

    <?php // echo $form->field($model, 'waktuUbah') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
