<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pekerjaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pekerjaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'tanggalMasuk')->textInput() ?>

    <?= $form->field($model, 'tanggalKeluar')->textInput() ?>

    <?= $form->field($model, 'jabatan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'noIdentitas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenisInstansi_id')->textInput() ?>

    <?= $form->field($model, 'orang_id')->textInput() ?>

    <?= $form->field($model, 'pekerjaanLainnya')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'waktuBuat')->textInput() ?>

    <?= $form->field($model, 'waktuUbah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
