<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AlamatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alamat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'jalan') ?>

    <?= $form->field($model, 'kodepos') ?>

    <?= $form->field($model, 'rt') ?>

    <?= $form->field($model, 'rw') ?>

    <?php // echo $form->field($model, 'desakelurahan_kode') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
