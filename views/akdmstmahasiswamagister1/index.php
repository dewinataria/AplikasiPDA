<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Akdmstmahasiswamagister1Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Akdmstmahasiswamagister1s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdmstmahasiswamagister1-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Akdmstmahasiswamagister1'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'MahasiswaID',
            'NIM',
            'MayorID',
            'JalurMasukID',
            // 'TanggalMasuk',
            // 'TahunMasuk',
            // 'BatasStudi',
            // 'AwalSemester',
            // 'GelarDepan',
            // 'GelarBelakang',
            // 'StatusVerifikasi',
            // 'StatusAkademikID',
            // 'PindahMayor',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
