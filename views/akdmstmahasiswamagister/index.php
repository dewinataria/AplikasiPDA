<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AkdmstmahasiswamagisterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Akdmstmahasiswamagisters');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdmstmahasiswamagister-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Akdmstmahasiswamagister'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'MahasiswaID',
            'NIM',
            'MayorID',
            'MinorID',
            // 'JalurMasukID',
            // 'TanggalMasuk',
            // 'StatusAkademikID',
            // 'BatasStudi',
            // 'TahunAkademikID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
