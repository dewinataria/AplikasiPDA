<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkdmstmahasiswamagisterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdmstmahasiswamagister-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'MahasiswaID') ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'MayorID') ?>

    <?= $form->field($model, 'MinorID') ?>

    <?php // echo $form->field($model, 'JalurMasukID') ?>

    <?php // echo $form->field($model, 'TanggalMasuk') ?>

    <?php // echo $form->field($model, 'StatusAkademikID') ?>

    <?php // echo $form->field($model, 'BatasStudi') ?>

    <?php // echo $form->field($model, 'TahunAkademikID') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
