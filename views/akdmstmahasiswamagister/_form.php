<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akdmstmahasiswamagister */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdmstmahasiswamagister-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'MahasiswaID')->textInput() ?>

    <?= $form->field($model, 'NIM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MayorID')->textInput() ?>

    <?= $form->field($model, 'MinorID')->textInput() ?>

    <?= $form->field($model, 'JalurMasukID')->textInput() ?>

    <?= $form->field($model, 'TanggalMasuk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'StatusAkademikID')->textInput() ?>

    <?= $form->field($model, 'BatasStudi')->textInput() ?>

    <?= $form->field($model, 'TahunAkademikID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
