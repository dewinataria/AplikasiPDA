<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cobbaa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cobbaa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Year')->textInput() ?>

    <?= $form->field($model, 'Male')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Female')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Lainlain')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
