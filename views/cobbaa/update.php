<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cobbaa */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Cobbaa',
]) . $model->Year;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cobbaas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Year, 'url' => ['view', 'id' => $model->Year]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cobbaa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
