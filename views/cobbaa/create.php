<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cobbaa */

$this->title = Yii::t('app', 'Create Cobbaa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cobbaas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cobbaa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
