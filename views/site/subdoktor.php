<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'SPs IPB Dalam Angka - Data Program Magister';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-magister">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <h3>Silakan pilih jenis rekap yang diinginkan:</h3>
    </p>

</div>

<br><br>

<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">


            
                <h2>Perkembangan Jumlah Mahasiswa Program Magister Tahun 2007/2008 s/d 2011/2012</h2>

                <p><br>Menampilkan data umum perkembangan jumlah mahasiswa program magister tahun 2007/2008 s/d 2011/2012 <br> <br>Pascasarjana IPB Program Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=highcharts/indexdoktor">Mulai &raquo;</a></p>
            </div>
            
            <div style="text-align:center">
                <h2>Perkembangan Jumlah Mahasiswa Program Magister Secara Rinci Tahun 2011/2012</h2>

                <p><br>Menampilkan data rinci perkembangan jumlah mahasiswa program magister pada tahun 2011/2012 <br><br> Pascasarjana IPB Program Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=fakultasrincifordoktor/index">Mulai &raquo;</a></p>
            </div>
            
            
</div>
</div>
</div>