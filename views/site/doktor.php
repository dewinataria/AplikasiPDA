<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'SPs IPB Dalam Angka - Data Program Doktor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-doktor">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <h3>Silahkan pilih jenis rekap yang diinginkan:</h3>
    </p>

</div>

<br><br>

<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">


            
                <h2>Pelamar Program Doktor</h2>

                <p><br>Menampilkan data perkembangan jumlah pelamar program doktor berdasarkan tahun pendaftaran <br> <br>Pascasarjana IPB Program Doktor (S3).</p>

                <p><a class="btn btn-default" href="index.php?r=tahunmasukdoktor">Mulai &raquo;</a></p>
            </div>
            
            <div style="text-align:center">
                <h2>Mahasiswa Program Doktor</h2>

                <p><br>Menampilkan data perkembangan jumlah mahasiswa program doktor berdasarkan tahun masuk <br><br> Pascasarjana IPB Program Doktor (S3).</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Fsubdoktor">Mulai &raquo;</a></p>
            </div>
            
            
</div>
</div>
</div>



<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">
            
                <h2>Lulusan Program Doktor</h2>

                <p><br>Menampilkan data jumlah lulusan program doktor berdasarkan tahun lulus <br><br> Pascasarjana IPB Program Doktor (S3).</p>

                <p><a class="btn btn-default" href="index.php?r=putusstudi-doktor">Mulai &raquo;</a></p>
            </div>
            
            
             <div style="text-align:center">
                <h2>Putus Studi Program Doktor</h2>

                <p><br>Menampilkan data jumlah mahasiswa doktor yang putus studi berdasarkan tahun putus studi <br><br> Pascasarjana IPB Program Doktor (S3).</p>

                <p><a class="btn btn-default" href="index.php?r=putusstudi-doktor">Mulai &raquo;</a></p>
            </div>
</div>
</div>
</div>