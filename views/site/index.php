<?php

/* @var $this yii\web\View */

$this->title = 'Beranda - Pascasarjana Dalam Angka';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Selamat Datang!</h1>

        <p class="lead-lg-5">Aplikasi ini ditujukan untuk menampilkan <br>data-data dan statistik mahasiswa program Pascasarjana Institut Pertanian Bogor</p>

        
    </div>

    <div class="body-content">

        <div class="row" align="center">
            <div class="col-lg-6">
                <h2>Program Magister</h2>

                <p >Klik disini untuk menampilkan data dan statistik mahasiswa <br> Pascasarjana IPB Program Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Fmagister">Mulai &raquo;</a></p>
            </div>
            <div class="col-lg-6">
                <h2>Program Doktor</h2>

                <p>Klik disini untuk menampilkan data dan statistik mahasiswa <br> Pascasarjana IPB Program Doktor (S3).</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Fdoktor">Mulai &raquo;</a></p>
            </div>
            
        </div>

    </div>
</div>
