<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'SPs IPB Dalam Angka - Data Program Magister';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-magister">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <h3>Silakan pilih jenis rekap yang diinginkan:</h3>
    </p>

</div>

<br><br>

<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">


            
                <h2>Pelamar Program Magister</h2>

                <p><br>Menampilkan data perkembangan jumlah pelamar program magister berdasarkan tahun pendaftaran <br> <br>Pascasarjana IPB Program Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=tahunmasukmagister">Mulai &raquo;</a></p>
            </div>
            
            <div style="text-align:center">
                <h2>Mahasiswa Program Magister</h2>

                <p><br>Menampilkan data perkembangan jumlah mahasiswa program magister berdasarkan tahun masuk <br><br> Pascasarjana IPB Program Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=site%2Fsubmagister">Mulai &raquo;</a></p>
            </div>
            
            
</div>
</div>
</div>



<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12" style="text-align:center">
            
                <h2>Lulusan Program Magister</h2>

                <p><br>Menampilkan data jumlah lulusan program magister berdasarkan tahun lulus <br><br> Pascasarjana IPB Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=putusstudi-magister">Mulai &raquo;</a></p>
            </div>
            
            
             <div style="text-align:center">
                <h2>Putus Studi Program Magister</h2>

                <p><br>Menampilkan data jumlah mahasiswa magister yang putus studi berdasarkan tahun putus studi <br><br> Pascasarjana IPB Magister (S2).</p>

                <p><a class="btn btn-default" href="index.php?r=putusstudi-magister">Mulai &raquo;</a></p>
            </div>
</div>
</div>
</div>