<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Ipkreal */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Ipkreal',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipkreals'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="ipkreal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
