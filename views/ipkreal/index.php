<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\IpkrealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ipkreals');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipkreal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Ipkreal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'NIM',
            'IPK',
            'Fakultas',
            'Departemen',
            'KodeMayor',
            // 'TahunMasuk',
            // 'StatusAkademik',
            // 'TahunAkademikID',
            // 'Semester',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
