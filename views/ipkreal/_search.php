<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IpkrealSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipkreal-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'IPK') ?>

    <?= $form->field($model, 'Fakultas') ?>

    <?= $form->field($model, 'Departemen') ?>

    <?= $form->field($model, 'KodeMayor') ?>

    <?php // echo $form->field($model, 'TahunMasuk') ?>

    <?php // echo $form->field($model, 'StatusAkademik') ?>

    <?php // echo $form->field($model, 'TahunAkademikID') ?>

    <?php // echo $form->field($model, 'Semester') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
