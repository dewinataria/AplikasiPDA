<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdreftahunakademik */

$this->title = Yii::t('app', 'Create Akdreftahunakademik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdreftahunakademiks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdreftahunakademik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
