<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akdmstmayor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdmstmayor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'StrukturOrganisasiID')->textInput() ?>

    <?= $form->field($model, 'DepartemenID')->textInput() ?>

    <?= $form->field($model, 'StrataID')->textInput() ?>

    <?= $form->field($model, 'Kode')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'KodePDPT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NamaEn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Inisial')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Kaprodi')->textInput() ?>

    <?= $form->field($model, 'TanggalDibentuk')->textInput() ?>

    <?= $form->field($model, 'NomorSKDibentuk')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MayorSarjanaBKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'MayorPascasarjanaBKey')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TanggalDitutup')->textInput() ?>

    <?= $form->field($model, 'NomorSKDitutup')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
