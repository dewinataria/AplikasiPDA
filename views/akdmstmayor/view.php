<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Akdmstmayor */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdmstmayors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdmstmayor-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'StrukturOrganisasiID',
            'DepartemenID',
            'StrataID',
            'Kode',
            'KodePDPT',
            'Nama',
            'NamaEn',
            'Inisial',
            'Kaprodi',
            'TanggalDibentuk',
            'NomorSKDibentuk',
            'MayorSarjanaBKey',
            'MayorPascasarjanaBKey',
            'TanggalDitutup',
            'NomorSKDitutup',
        ],
    ]) ?>

</div>
