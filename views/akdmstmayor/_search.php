<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkdmstmayorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdmstmayor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'StrukturOrganisasiID') ?>

    <?= $form->field($model, 'DepartemenID') ?>

    <?= $form->field($model, 'StrataID') ?>

    <?= $form->field($model, 'Kode') ?>

    <?php // echo $form->field($model, 'KodePDPT') ?>

    <?php // echo $form->field($model, 'Nama') ?>

    <?php // echo $form->field($model, 'NamaEn') ?>

    <?php // echo $form->field($model, 'Inisial') ?>

    <?php // echo $form->field($model, 'Kaprodi') ?>

    <?php // echo $form->field($model, 'TanggalDibentuk') ?>

    <?php // echo $form->field($model, 'NomorSKDibentuk') ?>

    <?php // echo $form->field($model, 'MayorSarjanaBKey') ?>

    <?php // echo $form->field($model, 'MayorPascasarjanaBKey') ?>

    <?php // echo $form->field($model, 'TanggalDitutup') ?>

    <?php // echo $form->field($model, 'NomorSKDitutup') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
