<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdmstmayor */

$this->title = Yii::t('app', 'Create Akdmstmayor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdmstmayors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdmstmayor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
