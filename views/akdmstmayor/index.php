<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AkdmstmayorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Akdmstmayors');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdmstmayor-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Akdmstmayor'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'StrukturOrganisasiID',
            'DepartemenID',
            'StrataID',
            'Kode',
            // 'KodePDPT',
            // 'Nama',
            // 'NamaEn',
            // 'Inisial',
            // 'Kaprodi',
            // 'TanggalDibentuk',
            // 'NomorSKDibentuk',
            // 'MayorSarjanaBKey',
            // 'MayorPascasarjanaBKey',
            // 'TanggalDitutup',
            // 'NomorSKDitutup',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
