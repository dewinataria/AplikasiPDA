<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usia2011 */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Usia2011',
]) . $model->Mayor;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usia2011s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->Mayor, 'url' => ['view', 'id' => $model->Mayor]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="usia2011-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
