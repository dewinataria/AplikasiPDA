<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Usia2011Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usia2011s');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usia2011-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Usia2011'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Mayor',
            'lessthan25',
            'btwn25upto29',
            'btwn30upto34',
            'btwn35upto39',
            // 'btwn40upto44',
            // 'btwn45upto49',
            // 'morethaneqs50',
            // 'NotComplete',
            // 'Total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
