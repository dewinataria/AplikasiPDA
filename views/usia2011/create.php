<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usia2011 */

$this->title = Yii::t('app', 'Create Usia2011');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usia2011s'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usia2011-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
