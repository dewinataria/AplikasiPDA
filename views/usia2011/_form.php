<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usia2011 */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usia2011-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'Mayor')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lessthan25')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btwn25upto29')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btwn30upto34')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btwn35upto39')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btwn40upto44')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btwn45upto49')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'morethaneqs50')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NotComplete')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Total')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
