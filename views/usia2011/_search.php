<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usia2011Search */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usia2011-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Mayor') ?>

    <?= $form->field($model, 'lessthan25') ?>

    <?= $form->field($model, 'btwn25upto29') ?>

    <?= $form->field($model, 'btwn30upto34') ?>

    <?= $form->field($model, 'btwn35upto39') ?>

    <?php // echo $form->field($model, 'btwn40upto44') ?>

    <?php // echo $form->field($model, 'btwn45upto49') ?>

    <?php // echo $form->field($model, 'morethaneqs50') ?>

    <?php // echo $form->field($model, 'NotComplete') ?>

    <?php // echo $form->field($model, 'Total') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
