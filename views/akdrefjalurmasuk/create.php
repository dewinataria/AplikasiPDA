<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdrefjalurmasuk */

$this->title = Yii::t('app', 'Create Akdrefjalurmasuk');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdrefjalurmasuks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdrefjalurmasuk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
