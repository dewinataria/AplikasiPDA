<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdrefstatuskelanjutanstudi */

$this->title = Yii::t('app', 'Create Akdrefstatuskelanjutanstudi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdrefstatuskelanjutanstudis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdrefstatuskelanjutanstudi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
