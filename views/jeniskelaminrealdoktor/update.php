<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jeniskelaminrealdoktor */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Jeniskelaminrealdoktor',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jeniskelaminrealdoktors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="jeniskelaminrealdoktor-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
