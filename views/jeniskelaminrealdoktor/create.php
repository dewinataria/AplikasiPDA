<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jeniskelaminrealdoktor */

$this->title = Yii::t('app', 'Create Jeniskelaminrealdoktor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jeniskelaminrealdoktors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jeniskelaminrealdoktor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
