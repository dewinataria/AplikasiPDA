<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkdhisakreditasipsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdhisakreditasips-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'MayorID') ?>

    <?= $form->field($model, 'NilaiAkreditasi') ?>

    <?= $form->field($model, 'PredikatAkreditasi') ?>

    <?= $form->field($model, 'TMTAkreditasi') ?>

    <?php // echo $form->field($model, 'TSTAkreditasi') ?>

    <?php // echo $form->field($model, 'LembagaPengakreditasiID') ?>

    <?php // echo $form->field($model, 'LingkupAkreditasiID') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
