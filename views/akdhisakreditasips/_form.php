<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akdhisakreditasips */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdhisakreditasips-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'MayorID')->textInput() ?>

    <?= $form->field($model, 'NilaiAkreditasi')->textInput() ?>

    <?= $form->field($model, 'PredikatAkreditasi')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TMTAkreditasi')->textInput() ?>

    <?= $form->field($model, 'TSTAkreditasi')->textInput() ?>

    <?= $form->field($model, 'LembagaPengakreditasiID')->textInput() ?>

    <?= $form->field($model, 'LingkupAkreditasiID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
