<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AkdhisakreditasipsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Akdhisakreditasips');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdhisakreditasips-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Akdhisakreditasips'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'ID',
            'MayorID',
            'NilaiAkreditasi',
            'PredikatAkreditasi',
            'TMTAkreditasi',
            // 'TSTAkreditasi',
            // 'LembagaPengakreditasiID',
            // 'LingkupAkreditasiID',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
