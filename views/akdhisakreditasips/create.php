<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdhisakreditasips */

$this->title = Yii::t('app', 'Create Akdhisakreditasips');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdhisakreditasips'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdhisakreditasips-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
