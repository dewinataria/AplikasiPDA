<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\S2view */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'S2view',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'S2views'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="s2view-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
