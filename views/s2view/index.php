<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\S2viewSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'S2 - Program Studi');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="s2view-index">

   
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create S2view'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'Fakultas',
            'Departemen',
            'KodeMayor',
            'NIM',
            'TahunMasuk',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
