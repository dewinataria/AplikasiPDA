<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\S2viewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="s2view-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'Fakultas') ?>

    <?= $form->field($model, 'Departemen') ?>

    <?= $form->field($model, 'KodeMayor') ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'TahunMasuk') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
