<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\S3view */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'S3view',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'S3views'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="s3view-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
