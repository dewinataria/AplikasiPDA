<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\S3view */

$this->title = $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'S3views'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="s3view-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->NIM], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->NIM], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'Fakultas',
            'Departemen',
            'KodeMayor',
            'NIM',
            'TahunMasuk',
        ],
    ]) ?>

</div>
