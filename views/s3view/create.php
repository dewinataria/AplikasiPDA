<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\S3view */

$this->title = Yii::t('app', 'Create S3view');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'S3views'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="s3view-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
