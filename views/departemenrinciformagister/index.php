<?php
    use app\assets\HighchartsAsset;
    
    
    HighchartsAsset::register($this);
    $this->title = 'Highcharts Test';
    ?>

<!--data usia fakultas 2011 -->

<div class="container">
<div class="row">
<div class="x_panel">
<div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
   $(function () {
    $('#my-chart').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Magister Berdasarkan Usia Tahun2011',
            x: -20 //center
        },
        
        xAxis: {
            categories: $deptusia
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
            {   name: '<25',
              
                data: $lessthan25,

            },
            {   name: '25-29',
                
                data: $btween25to29,

            },

            {   name: '30-34',
                
                data: $btween30to34,

            },

            {   name: '35-39',
                
                data: $btween35to39,

            },

            {   name: '40-44',
                
                data: $btween40to44,

            },

            {   name: '45-49',
                
                data: $btween45to49,

            },

            {   name: '>=50',
                
                data: $morethaneqs50,

            },

            {   name: 'NotComplete',
                
                data: $NotComplete,

            }],

        
    })
});
")?>

</div>
</div> 
</div>
</div>

<!-- data ipk fakultas 2011 -->

<div class="container">
<div class="row">
<div class="x_panel">
<div id="my-chart1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
   $(function () {
    $('#my-chart1').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Magister Berdasarkan IPK Tahun 2011',
            x: -20 //center
        },
        
        xAxis: {
            categories: $deptipk
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
            {   name: '<2.5',
              
                data: $lessthanipk,

            },
            {   name: '2.50-2.74',
                
                data: $btweenipk1,

            },

            {   name: '2.75-2.99',
                
                data: $btweenipk2,

            },

            {   name: '3.00-3.24',
                
                data: $btweenipk3,

            },

            {   name: '3.25-3.49',
                
                data: $btweenipk4,

            },

            {   name: '3.50-4.00',
                
                data: $btweenipk5,

            },

            {   name: 'Tidak Bisa Dikonversi',
                
                data: $tidakbisadikonversi,

            },

            {   name: 'Tidak Lengkap',
                
                data: $TidakLengkap,

            }],

        
    })
});
")?>

</div>
</div> 
</div>
</div>

<!-- data ipk fakultas 2010 -->

<div class="container">
<div class="row">
<div class="x_panel">
<div id="my-chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
   $(function () {
    $('#my-chart3').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Magister Berdasarkan IPK Tahun 2010',
            x: -20 //center
        },
        
        xAxis: {
            categories: $deptipk2010
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [
            {   name: '<2.5',
              
                data: $lessthanipk2010,

            },
            {   name: '2.50-2.74',
                
                data: $btweenipk1a,

            },

            {   name: '2.75-2.99',
                
                data: $btweenipk2a,

            },

            {   name: '3.00-3.24',
                
                data: $btweenipk3a,

            },

            {   name: '3.25-3.49',
                
                data: $btweenipk4a,

            },

            {   name: '3.50-4.00',
                
                data: $btweenipk5a,

            },

            {   name: 'Tidak Bisa Dikonversi',
                
                data: $tidakbisadikonversi2010,

            },

            {   name: 'Tidak Lengkap',
                
                data: $TidakLengkap2010,

            }],

        
    })
});
")?>

</div>
</div> 
</div>
</div>