<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdrefstrata */

$this->title = Yii::t('app', 'Create Akdrefstrata');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdrefstratas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdrefstrata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
