<?php
    use app\assets\HighchartsAsset;
    
    
    HighchartsAsset::register($this);
    $this->title = 'Highcharts Test';
    ?>
    
    
    <div class="container">
          <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel">
                      <div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    
    
//jenis kelamin magister    
    <?php $this->registerJs("
    $(function () {
        $('#my-chart').highcharts({
            title: {
                text: 'Jenis Kelamin Magister',
                x: -20 //center
            },
    
            xAxis: {
                categories: $year
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Laki-laki',
                data: $male
            }, {
                name: 'Perempuan',
                data: $female
            },
            {
                name: 'Lain-lain',
                data: $other
            }]
        });
    });
    ")?>
    </div>
    </div>


          <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel">
                      <div id="my-chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    

//jenis kelamin doktor    
    
    <?php $this->registerJs("
    $(function () {
        $('#my-chart2').highcharts({
            title: {
                text: 'Jenis Kelamin Doktor',
                x: -20 //center
            },
    
            xAxis: {
                categories: $yeardoktor
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: 'Laki-laki',
                data: $maledoktor
            }, {
                name: 'Perempuan',
                data: $femaledoktor
            },
            {
                name: 'Lain-lain',
                data: $otherdoktor
            }]
        });
    });
    ")?>
    </div>
    </div>

//usia

 <div class="container">
          <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="x_panel">
                      <div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    <?php $this->registerJs("
    $(function () {
        $('#my-chart3').highcharts({
            title: {
                text: 'Usia',
                x: -20 //center
            },
    
            xAxis: {
                categories: $yearusia
            },
            yAxis: {
                title: {
                    text: 'Total'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                valueSuffix: ''
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle',
                borderWidth: 0
            },
            series: [{
                name: '<25',
                data: $usia2
            }, {
                name: '25-29',
                data: $usia3
            },
            {
                name: '30-34',
                data: $usia4
            },
            {
                name: '35-39',
                data: $usia5
            },
            {
                name: '40-44',
                data: $usia6
            },
            {
                name: '45-49',
                data: $usia7
            },
            {
                name: '>=50',
                data: $usia8
            },
            {
                name: 'Tidak Lengkap',
                data: $usia9
            }       
            ]
        });
    });
    ")?>
    </div>
    </div>