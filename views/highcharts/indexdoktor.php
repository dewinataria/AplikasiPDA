<?php
    use app\assets\HighchartsAsset;
    
    
    HighchartsAsset::register($this);
    $this->title = 'Highcharts Test';
    ?>
    
    
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    
    
<!-- jenis kelamin doktor -->    
<?php $this->registerJs("
$(function () {
    $('#my-chart').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Mahasiswa Doktor Berdasarkan Jenis Kelamin'
    },
    xAxis: {
        categories: $yearjkdoktor
    },
    yAxis: 
    {
        title: 
        {
           text: 'Jumlah'
        }
    },
    credits:
    {
        enabled: false
    },
    series: [{
            name: 'Laki-Laki',
            data: $maledoktor
        }, {
            name: 'Perempuan',
            data: $femaledoktor
        },
        {
            name: 'Lainnya',
            data: $otherdoktor
        }]
});
});
")?>
</div>
</div>

<!-- usia -->
               

<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="my-chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
   $(function () {
    $('#my-chart2').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Mahasiswa Doktor Berdasarkan Usia'
    },
    xAxis: {
        categories: $yearusiadoktor
    },
    yAxis: 
    {
        title: 
        {
           text: 'Jumlah'
        }
    },
    credits:
    {
        enabled: false
    },
            series: [{
                name: '<25',
                data: $usiadoktor1
            }, {
                name: '25-29',
                data: $usiadoktor2
            },
            {
                name: '30-34',
                data: $usiadoktor3
            },
            {
                name: '35-39',
                data: $usiadoktor4
            },
            {
                name: '40-44',
                data: $usiadoktor5
            },
            {
                name: '45-49',
                data: $usiadoktor6
            },
            {
                name: '>=50',
                data: $usiadoktor7
            },
            {
                name: 'Tidak Lengkap',
                data: $usiadoktor8
            }       
            ]
        });
    });
    ")?>
    </div>
    </div>

<!-- ipk -->
<div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
    <div id="my-chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart3').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Doktor Berdasarkan IPK',
            x: -20 //center
        },
        
        xAxis: {
            categories: $yearipkdoktor
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '<2.50',
            data: $ipkdoktor1
        }, {
            name: '2.50-2.74',
            data: $ipkdoktor2
        },
        {
            name: '2.75-2.99',
            data: $ipkdoktor3
        },
        {
            name: '3.00-3.24',
            data: $ipkdoktor4
        },
        {
            name: '3.25-3.49',
            data: $ipkdoktor5
        },
        {
            name: '3.50-4.00',
            data: $ipkdoktor6
        },
        {
            name: 'tidakbisadiconvert',
            data: $ipkdoktor7
        }]
    });
});
")?>
</div>
</div>

<!-- durasi studi -->
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="my-chart4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart4').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Doktor Berdasarkan Lama Studi',
            x: -20 //center
        },
        
        xAxis: {
            categories: $yeardurasistudidoktor
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '<2',
            data: $durasistudidoktor1
        }, {
            name: '2.0-2.4',
            data: $durasistudidoktor2
        },
        {
            name: '2.5-2.9',
            data: $durasistudidoktor3
        },
        {
            name: '3.0-3.4',
            data: $durasistudidoktor4
        },
        {
            name: '3.5-3.9',
            data: $durasistudidoktor5
        },
        {
            name: '>=4.0',
            data: $durasistudidoktor6
        },
        {
            name: 'TidakDiketahui',
            data: $durasistudidoktor7
        }]
    });
});
")?>
</div>
</div>

<!-- Status Studi -->

<div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
    <div id="my-chart5" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart5').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Doktor Berdasarkan Status Studi',
            x: -20 //center
        },
        
        xAxis: {
            categories: $yearstatusstudidoktor
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Aktif',
            data: $statusstudidoktor1
        }, {
            name: 'Cuti',
            data: $statusstudidoktor2
        },
        {
            name: 'Lulus',
            data: $statusstudidoktor3
        },
        {
            name: 'Mengundurkan Diri',
            data: $statusstudidoktor4
        },
        {
            name: 'Drop Out',
            data: $statusstudidoktor5
        },
        {
            name: 'Tanpa Keterangan',
            data: $statusstudidoktor6
        },
        {
            name: 'Meninggal Dunia',
            data: $statusstudidoktor7
        },
        {
            name: 'Non Aktif',
            data: $statusstudidoktor8
        },
        {
            name: 'Pindah Mayor',
            data: $statusstudidoktor9
        }]
    });
});
")?>
</div>
</div>