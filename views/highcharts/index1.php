<?php
use app\assets\HighchartsAsset;

HighchartsAsset::register($this);
$this->title = 'Highcharts Test';
?>


<div class="container">
      <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                


<?php $this->registerJs("
$(function () {
    $('#my-chart').highcharts({
        title: {
            text: 'Jenis Kelamin',
            x: -20 //center
        },
        
        xAxis: {
            categories: $tahun
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Laki-laki',
            data: $data_l
        }, {
            name: 'Perempuan',
            data: $data_p
        }]
    });
});
")?>
</div>
</div>


<!-- selanjutnya -->
      <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div id="my-chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart2').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jenis Kelamin Doktor'
    },
    xAxis: {
        categories: $tahun1
    },
    credits: {
        enabled: false
    },
    series: [{
            name: 'Laki-laki',
            data: $data_l1
        }, {
            name: 'Perempuan',
            data: $data_p1
        }]
});
});
")?>

</div>
</div> 

<div class="container">
      <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div id="my-chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php $this->registerJs("
$(function () {
    $('#my-chart').highcharts({
        title: {
            text: 'Usia',
            x: -20 //center
        },
        
        xAxis: {
            categories: $tahun2
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Usia',
            data: $data_l2
        }]
    });
});
")?>
</div>
</div>



