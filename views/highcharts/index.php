<?php
    use app\assets\HighchartsAsset;
    
    
    HighchartsAsset::register($this);
    $this->title = 'Highcharts Test';
    ?>
    
    
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
                    
    
<!-- jenis kelamin magister -->    
    <?php $this->registerJs("
$(function () {
    $('#my-chart').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Mahasiswa Magister Berdasarkan Jenis Kelamin'
    },
    xAxis: {
        categories: $year
    },
    yAxis: 
    {
        title: 
        {
           text: 'Jumlah'
        }
    },
    credits:
    {
        enabled: false
    },
    series: [{
            name: 'Laki-Laki',
            data: $male
        }, {
            name: 'Perempuan',
            data: $female
        },
        {
            name: 'Lainnya',
            data: $other
        }]
});
});
")?>
</div>
</div>

<!-- usia -->
               

<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="my-chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
   $(function () {
    $('#my-chart2').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jumlah Mahasiswa Magister Berdasarkan Usia'
    },
    xAxis: {
        categories: $yearusia
    },
    yAxis: 
    {
        title: 
        {
           text: 'Jumlah'
        }
    },
    credits:
    {
        enabled: false
    },
            series: [{
                name: '<25',
                data: $usia1
            }, {
                name: '25-29',
                data: $usia2
            },
            {
                name: '30-34',
                data: $usia3
            },
            {
                name: '35-39',
                data: $usia4
            },
            {
                name: '40-44',
                data: $usia5
            },
            {
                name: '45-49',
                data: $usia6
            },
            {
                name: '>=50',
                data: $usia7
            },
            {
                name: 'Tidak Lengkap',
                data: $usia8
            }       
            ]
        });
    });
    ")?>
    </div>
    </div>

<!-- ipk -->
<div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
    <div id="my-chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart3').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Magister Berdasarkan IPK',
            x: -20 //center
        },
        
        xAxis: {
            categories: $yearipk
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '<2.50',
            data: $ipk1
        }, {
            name: '2.50-2.74',
            data: $ipk2
        },
        {
            name: '2.75-2.99',
            data: $ipk3
        },
        {
            name: '3.00-3.24',
            data: $ipk4
        },
        {
            name: '3.25-3.49',
            data: $ipk5
        },
        {
            name: '3.50-4.00',
            data: $ipk6
        },
        {
            name: 'tidakbisadiconvert',
            data: $ipk7
        }]
    });
});
")?>
</div>
</div>

<!-- durasi studi -->
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="my-chart4" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart4').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Magister Berdasarkan Lama Studi',
            x: -20 //center
        },
        
        xAxis: {
            categories: $yeardurasistudi
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '<2',
            data: $durasistudi1
        }, {
            name: '2.0-2.4',
            data: $durasistudi2
        },
        {
            name: '2.5-2.9',
            data: $durasistudi3
        },
        {
            name: '3.0-3.4',
            data: $durasistudi4
        },
        {
            name: '3.5-3.9',
            data: $durasistudi5
        },
        {
            name: '>=4.0',
            data: $durasistudi6
        },
        {
            name: 'TidakDiketahui',
            data: $durasistudi7
        }]
    });
});
")?>
</div>
</div>

<!-- Status Studi -->

<div class="container">
    <div class="row">
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="x_panel">
    <div id="my-chart5" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart5').highcharts({
        title: {
            text: 'Jumlah Mahasiswa Magister Berdasarkan Status Studi',
            x: -20 //center
        },
        
        xAxis: {
            categories: $yearstatusstudi
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Aktif',
            data: $statusstudi1
        }, {
            name: 'Cuti',
            data: $statusstudi2
        },
        {
            name: 'Lulus',
            data: $statusstudi3
        },
        {
            name: 'Mengundurkan Diri',
            data: $statusstudi4
        },
        {
            name: 'Drop Out',
            data: $statusstudi5
        },
        {
            name: 'Tanpa Keterangan',
            data: $statusstudi6
        },
        {
            name: 'Meninggal Dunia',
            data: $statusstudi7
        },
        {
            name: 'Non Aktif',
            data: $statusstudi8
        },
        {
            name: 'Pindah Mayor',
            data: $statusstudi9
        }]
    });
});
")?>
</div>
</div>