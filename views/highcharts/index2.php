<?php
use app\assets\HighchartsAsset;

HighchartsAsset::register($this);
$this->title = 'Highcharts Test';
?>


<div class="container">
      <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div id="my-chart" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
 

<?php $this->registerJs("
$(function () {
    $('#my-chart').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jenis Kelamin Mahasiswa Magister'
    },
    
    xAxis: {
        categories: [
            '2011/2012',
            '2012/2013',
            '2013/2014',
            '2014/2015',
            '2015/2016
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
   
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Perempuan',
        data: [579, 825, 1184, 970, 796]

    }, {
        name: 'Laki-laki',
        data: [581, 786, 888, 729, 682]

    }]
});
});
")?>

</div>
</div> 

<!-- selanjutnya -->
<div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div id="my-chart2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

<?php $this->registerJs("
$(function () {
    $('#my-chart2').highcharts({
        chart: {
        type: 'column'
    },
    title: {
        text: 'Jenis Kelamin Mahasiswa Doktor'
    },
    
    xAxis: {
        categories: [
            '2011/2012',
            '2012/2013',
            '2013/2014',
            '2014/2015',
            '2015/2016'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Jumlah'
        }
    },
   
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [{
        name: 'Perempuan',
        data: [579, 825, 1184, 970, 796]

    }, {
        name: 'Laki-laki',
        data: [581, 786, 888, 729, 682]

    }]
});
});
")?>

</div>
</div> 

<!-- selanjutnya -->
    <div class="container">
      <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div id="my-chart3" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<?php $this->registerJs("
$(function () {
    $('#my-chart').highcharts({
        title: {
            text: 'IPK S1',
            x: -20 //center
        },
        
        xAxis: {
            categories: [
            '2011/2012',
            '2012/2013',
            '2013/2014',
            '2014/2015',
            '2015/2016
        ],
        crosshair: true
        },
        yAxis: {
            title: {
                text: 'Jumlah'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: ''
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
       series: [{
        name: 'Perempuan',
        data: [579, 825, 1184, 970, 796]

    }, {
        name: 'Laki-laki',
        data: [581, 786, 888, 729, 682]

    }]
    });
});
")?>
</div>
</div>