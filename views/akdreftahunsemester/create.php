<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdreftahunsemester */

$this->title = Yii::t('app', 'Create Akdreftahunsemester');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdreftahunsemesters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdreftahunsemester-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
