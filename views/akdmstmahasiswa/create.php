<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdmstmahasiswa */

$this->title = Yii::t('app', 'Create Akdmstmahasiswa');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdmstmahasiswas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdmstmahasiswa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
