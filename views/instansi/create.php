<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Instansi */

$this->title = Yii::t('app', 'Create Instansi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Instansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instansi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
