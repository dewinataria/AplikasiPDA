<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Jenisinstansi */

$this->title = Yii::t('app', 'Create Jenisinstansi');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jenisinstansis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jenisinstansi-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
