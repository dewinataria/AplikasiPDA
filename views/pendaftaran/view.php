<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Pendaftaran */

$this->title = $model->noPendaftaran;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pendaftarans'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendaftaran-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->noPendaftaran], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->noPendaftaran], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'noPendaftaran',
            'OrangID',
            'RencanaPembiayaanID',
            'ManajemenJalurMasukID',
            'PaketPendaftaranID',
            'setujuSyarat',
            'verifikasiPMB',
            'waktuBuat',
            'waktuubah',
        ],
    ]) ?>

</div>
