<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PendaftaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pendaftaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'noPendaftaran') ?>

    <?= $form->field($model, 'OrangID') ?>

    <?= $form->field($model, 'RencanaPembiayaanID') ?>

    <?= $form->field($model, 'ManajemenJalurMasukID') ?>

    <?= $form->field($model, 'PaketPendaftaranID') ?>

    <?php // echo $form->field($model, 'setujuSyarat') ?>

    <?php // echo $form->field($model, 'verifikasiPMB') ?>

    <?php // echo $form->field($model, 'waktuBuat') ?>

    <?php // echo $form->field($model, 'waktuubah') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
