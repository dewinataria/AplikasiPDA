<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pendaftaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pendaftaran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'noPendaftaran')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OrangID')->textInput() ?>

    <?= $form->field($model, 'RencanaPembiayaanID')->textInput() ?>

    <?= $form->field($model, 'ManajemenJalurMasukID')->textInput() ?>

    <?= $form->field($model, 'PaketPendaftaranID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'setujuSyarat')->textInput() ?>

    <?= $form->field($model, 'verifikasiPMB')->textInput() ?>

    <?= $form->field($model, 'waktuBuat')->textInput() ?>

    <?= $form->field($model, 'waktuubah')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
