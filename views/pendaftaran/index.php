<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PendaftaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pendaftarans');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pendaftaran-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Pendaftaran'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'noPendaftaran',
            'OrangID',
            'RencanaPembiayaanID',
            'ManajemenJalurMasukID',
            'PaketPendaftaranID',
            // 'setujuSyarat',
            // 'verifikasiPMB',
            // 'waktuBuat',
            // 'waktuubah',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
