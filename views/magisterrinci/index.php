<?php
    use app\assets\HighchartsAsset;
    
    
    HighchartsAsset::register($this);
    $this->title = 'Highcharts Test';
    ?>


<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="x_panel">
<div id="containers" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
 <?php $this->registerJs("

$(function () {

    // Create the chart
    $('#containers').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Basic drilldown'
        },
        
        xAxis: [{
            id: 0,
            categories: $faculty

        }
        ],
        legend: {
            enabled: false
        },

        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                }
            }
        },

        series: [
            {
                name: 'lessthan25',
              
                data: $,

            },
            {
                name: 'btween25to29',
                
                data: $,

            }],

        
    })
});
")?>

</div>
</div> 
</div>
</div>