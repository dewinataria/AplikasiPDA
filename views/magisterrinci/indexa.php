<?php
    use app\assets\HighchartsAsset;
    
    
    HighchartsAsset::register($this);
    $this->title = 'Highcharts Test';
    ?>


<div id="container1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

 <?php $this->registerJs("
    Highcharts.setOptions({
    lang: {
        drillUpText: '◁ Back to {series.name}'
    }
});

var options = {

    chart: {
        height: 300
    },
    
    title: {
        text: 'Highcharts Drilldown Plugin'
    },

    xAxis: {
        categories: true
    },
    
    drilldown: {
        series: [{
            id: 'fruits',
            name: 'lessthan25',
            data: [
                {
                name: 'A1', 
                y: 2,
                drilldown: 'toyota'
            },
            {
                name: 'A2', 
                y: 8,
                drilldown: 'toyota1'
            }
            ]
        }, {
            id: 'cars',
            name: 'lessthan25',
            data: [{
                name: 'B1', 
                y: 10,
                drilldown: 'toyota2'
            },
            {
                name: 'B2', 
                y: 12,
                drilldown: 'toyota3'
            }]
        },
        {
            id: 'fruits1',
            name: 'btween25to29',
            data: [{
                name: 'A1', 
                y: 5,
                drilldown: 'toyota4'
            },
            {
                name: 'A2', 
                y: 10,
                drilldown: 'toyota5'
            }]
        }, 
        {
            id: 'cars1',
            name: 'btween25to29',
            data: [{
                name: 'B1', 
                y: 15,
                drilldown: 'toyota6'
            },
            {
                name: 'B2', 
                y: 19,
                drilldown: 'toyota7'
            }]
        },
        {
            id: 'toyota',
            name: 'lessthan25',
            data: [
               {
                name: 'communication', 
                y: 7
            },
            {
                name: 'economics', 
                y: 6
            }
            ]
        },

        {
            id: 'toyota1',
            name: 'lessthan25',
            data: [
               {
                name: 'management', 
                y: 4
            },
            {
                name: 'socialhumility', 
                y: 5
            }
            ]
        },
        {
            id: 'toyota2',
            name: 'lessthan25',
            data: [
               {
                name: 'computerdesign', 
                y: 5
            },
            {
                name: 'computerengineering', 
                y: 3
            }
            ]
        },
        {
            id: 'toyota3',
            name: 'lessthan25',
            data: [
               {
                name: 'computerscience', 
                y: 1
            },
            {
                name: 'informationsystem', 
                y: 1
            }
            ]
        },
        {
            id: 'toyota4',
            name: 'btween25to29',
            data: [
               {
                name: 'communication', 
                y: 12
            },
            {
                name: 'economics', 
                y: 10
            }
            ]
        },
        {
            id: 'toyota5',
            name: 'btween25to29',
            data: [
               {
                name: 'management', 
                y: 5
            },
            {
                name: 'socialhumility', 
                y: 7
            }
            ]
        },
         {
            id: 'toyota6',
            name: 'btween25to29',
            data: [
               {
                name: 'computerdesign', 
                y: 5
            },
            {
                name: 'computerengineering', 
                y: 5
            }
            ]
        },
        {
            id: 'toyota7',
            name: 'btween25to29',
            data: [
               {
                name: 'computerscience', 
                y: 3
            },
            {
                name: 'informationsystem', 
                y: 2
            }
            ]
        }]
    },
    
    legend: {
        enabled: false
    },
    
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true
            },
            shadow: false
        },
        pie: {
            size: '80%'
        }
    },
    
    series: [{
        name: 'lessthan25',
        colorByPoint: true,
        data: [{
            name: 'A',
            y: 10,
            drilldown: 'fruits'
        }, {
            name: 'B',
            y: 22,
            drilldown: 'cars'
        }]
    },

    {
        name: 'btween25to29',
        colorByPoint: true,
        data: [{
            name: 'A',
            y: 15,
            drilldown: 'fruits1'
        }, {
            name: 'B',
            y: 34,
            drilldown: 'cars1'
        }]
    }]
};

// Column chart
options.chart.renderTo = 'container1';
options.chart.type = 'column';
var chart1 = new Highcharts.Chart(options);




")?>
</div>
</div>
