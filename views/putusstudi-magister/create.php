<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PutusstudiMagister */

$this->title = Yii::t('app', 'Create Putusstudi Magister');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Putusstudi Magisters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="putusstudi-magister-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
