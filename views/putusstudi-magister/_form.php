<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PutusstudiMagister */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="putusstudi-magister-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'StatusAkademik')->textInput() ?>

    <?= $form->field($model, 'JenisKelaminID')->textInput() ?>

    <?= $form->field($model, 'TahunMasuk')->textInput() ?>

    <?= $form->field($model, 'fakultas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NIM')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
