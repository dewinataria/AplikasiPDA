<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PutusstudiMagister */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Putusstudi Magister',
]) . $model->NIM;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Putusstudi Magisters'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->NIM, 'url' => ['view', 'id' => $model->NIM]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="putusstudi-magister-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
