<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PutusstudiMagisterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Putus Studi dan Status Studi S2');
$this->params['breadcrumbs'][] = $this->title;
?><div class="col-lg-3">
    <p><h3>Status Akademik:</h3> <br>
    1- Aktif <br>
    2 - Cuti <br>
    3 - Lulus <br>
    4 - Mengundurkan Diri <br>
    5 - Drop Out <br>
    6 - Tanpa Keterangan <br>
    7 - Meninggal Dunia <br>
    8- Nonaktif <br>
    9 - Pindah Mayor
    <br><br> 
    </div>
    <br>
<div class="putusstudi-magister-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Putusstudi Magister'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'StatusAkademik',
            //'JenisKelaminID',
            'TahunMasuk',
            'fakultas',
            'NIM',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
