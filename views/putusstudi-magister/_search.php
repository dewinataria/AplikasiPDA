<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PutusstudiMagisterSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="putusstudi-magister-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'StatusAkademik') ?>

    <?= $form->field($model, 'JenisKelaminID') ?>

    <?= $form->field($model, 'TahunMasuk') ?>

    <?= $form->field($model, 'fakultas') ?>

    <?= $form->field($model, 'NIM') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
