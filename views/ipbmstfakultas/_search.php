<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\IpbmstfakultasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ipbmstfakultas-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Kode') ?>

    <?= $form->field($model, 'Dekan') ?>

    <?= $form->field($model, 'TanggalDibentuk') ?>

    <?= $form->field($model, 'FakultasBKey') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
