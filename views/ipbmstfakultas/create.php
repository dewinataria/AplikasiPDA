<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ipbmstfakultas */

$this->title = Yii::t('app', 'Create Ipbmstfakultas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipbmstfakultas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipbmstfakultas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
