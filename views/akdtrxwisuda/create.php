<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdtrxwisuda */

$this->title = Yii::t('app', 'Create Akdtrxwisuda');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdtrxwisudas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdtrxwisuda-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
