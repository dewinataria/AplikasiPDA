<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AkdtrxwisudaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdtrxwisuda-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'MahasiswaID') ?>

    <?= $form->field($model, 'NIM') ?>

    <?= $form->field($model, 'NomorIjazah') ?>

    <?= $form->field($model, 'TanggalLulus') ?>

    <?php // echo $form->field($model, 'TanggalIjazah') ?>

    <?php // echo $form->field($model, 'TanggalDaftar') ?>

    <?php // echo $form->field($model, 'GelarID') ?>

    <?php // echo $form->field($model, 'SyaratBuktiLaporan') ?>

    <?php // echo $form->field($model, 'SyaratBuktiLaporanPath') ?>

    <?php // echo $form->field($model, 'SyaratBebasPustaka') ?>

    <?php // echo $form->field($model, 'SyaratBebasPustakaPath') ?>

    <?php // echo $form->field($model, 'SyaratLembarPengesahan') ?>

    <?php // echo $form->field($model, 'SyaratLembarPengesahanPath') ?>

    <?php // echo $form->field($model, 'SyaratPasFoto') ?>

    <?php // echo $form->field($model, 'SyaratPasFotoPath') ?>

    <?php // echo $form->field($model, 'SyaratBuktiBayar') ?>

    <?php // echo $form->field($model, 'SyaratBuktiBayarPath') ?>

    <?php // echo $form->field($model, 'SyaratIjazahSMA') ?>

    <?php // echo $form->field($model, 'SyaratIjazahSMAPath') ?>

    <?php // echo $form->field($model, 'HadirWisuda') ?>

    <?php // echo $form->field($model, 'UlangWisuda') ?>

    <?php // echo $form->field($model, 'LamaStudi') ?>

    <?php // echo $form->field($model, 'JumlahTransferSKS') ?>

    <?php // echo $form->field($model, 'TahapWisudaID') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
