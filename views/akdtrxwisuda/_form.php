<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Akdtrxwisuda */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="akdtrxwisuda-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID')->textInput() ?>

    <?= $form->field($model, 'MahasiswaID')->textInput() ?>

    <?= $form->field($model, 'NIM')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NomorIjazah')->textInput() ?>

    <?= $form->field($model, 'TanggalLulus')->textInput() ?>

    <?= $form->field($model, 'TanggalIjazah')->textInput() ?>

    <?= $form->field($model, 'TanggalDaftar')->textInput() ?>

    <?= $form->field($model, 'GelarID')->textInput() ?>

    <?= $form->field($model, 'SyaratBuktiLaporan')->textInput() ?>

    <?= $form->field($model, 'SyaratBuktiLaporanPath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SyaratBebasPustaka')->textInput() ?>

    <?= $form->field($model, 'SyaratBebasPustakaPath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SyaratLembarPengesahan')->textInput() ?>

    <?= $form->field($model, 'SyaratLembarPengesahanPath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SyaratPasFoto')->textInput() ?>

    <?= $form->field($model, 'SyaratPasFotoPath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SyaratBuktiBayar')->textInput() ?>

    <?= $form->field($model, 'SyaratBuktiBayarPath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'SyaratIjazahSMA')->textInput() ?>

    <?= $form->field($model, 'SyaratIjazahSMAPath')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'HadirWisuda')->textInput() ?>

    <?= $form->field($model, 'UlangWisuda')->textInput() ?>

    <?= $form->field($model, 'LamaStudi')->textInput() ?>

    <?= $form->field($model, 'JumlahTransferSKS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TahapWisudaID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
