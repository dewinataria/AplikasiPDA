<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usia */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Usia',
]) . $model->TahunMasuk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->TahunMasuk, 'url' => ['view', 'id' => $model->TahunMasuk]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="usia-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
