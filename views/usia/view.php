<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Usia */

$this->title = $model->TahunMasuk;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usias'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usia-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->TahunMasuk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->TahunMasuk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'TahunMasuk',
            'lessthan25',
            'btween25upto29',
            'btween30upto34',
            'btween35upto39',
            'btween40upto44',
            'btween45upto49',
            'morethanequals50',
            'TidakLengkap',
            'Total',
        ],
    ]) ?>

</div>
