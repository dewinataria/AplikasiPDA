<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usia */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usia-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'TahunMasuk')->textInput() ?>

    <?= $form->field($model, 'lessthan25')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btween25upto29')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btween30upto34')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btween35upto39')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btween40upto44')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'btween45upto49')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'morethanequals50')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'TidakLengkap')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Total')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
