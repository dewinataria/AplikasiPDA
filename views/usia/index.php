<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usia-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Usia'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'TahunMasuk',
            'lessthan25',
            'btween25upto29',
            'btween30upto34',
            'btween35upto39',
            // 'btween40upto44',
            // 'btween45upto49',
            // 'morethanequals50',
            // 'TidakLengkap',
            // 'Total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
