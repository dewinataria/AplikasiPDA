<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UsiaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usia-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'TahunMasuk') ?>

    <?= $form->field($model, 'lessthan25') ?>

    <?= $form->field($model, 'btween25upto29') ?>

    <?= $form->field($model, 'btween30upto34') ?>

    <?= $form->field($model, 'btween35upto39') ?>

    <?php // echo $form->field($model, 'btween40upto44') ?>

    <?php // echo $form->field($model, 'btween45upto49') ?>

    <?php // echo $form->field($model, 'morethanequals50') ?>

    <?php // echo $form->field($model, 'TidakLengkap') ?>

    <?php // echo $form->field($model, 'Total') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
