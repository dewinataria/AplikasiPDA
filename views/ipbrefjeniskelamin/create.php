<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Ipbrefjeniskelamin */

$this->title = Yii::t('app', 'Create Ipbrefjeniskelamin');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ipbrefjeniskelamins'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ipbrefjeniskelamin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
