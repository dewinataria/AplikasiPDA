<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Namafakultas */

$this->title = Yii::t('app', 'Create Namafakultas');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Namafakultas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="namafakultas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
