<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Akdrefstatusakademik */

$this->title = Yii::t('app', 'Create Akdrefstatusakademik');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Akdrefstatusakademiks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="akdrefstatusakademik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
