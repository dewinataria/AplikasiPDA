<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pekerjaanhasinstitusi */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Pekerjaanhasinstitusi',
]) . $model->pekerjaan_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pekerjaanhasinstitusis'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pekerjaan_id, 'url' => ['view', 'pekerjaan_id' => $model->pekerjaan_id, 'institusi_ID' => $model->institusi_ID]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="pekerjaanhasinstitusi-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
