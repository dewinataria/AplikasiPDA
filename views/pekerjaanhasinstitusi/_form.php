<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Pekerjaanhasinstitusi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pekerjaanhasinstitusi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'pekerjaan_id')->textInput() ?>

    <?= $form->field($model, 'institusi_ID')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
